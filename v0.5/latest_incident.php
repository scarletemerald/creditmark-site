<?php if ($login) { 
$incidents = getIncident("", "", 5); 
?>
          <table class="table data">
        	<thead>
            	<tr>
                    <td>Company</td>
                    <td width="150">Invoice No.</td>
                    <td width="60">Status</td>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($incidents as $incident) {
				$cust = getCustomer($incident['cust_id'], -1);
				$cust = array_pop($cust);
				
				$status = getIncidentStatus($incident['status']);
				 ?>
            	<tr>
                    <td><?=$cust['company_name'];?></td>
                    <td><?=$incident['invoice_no'];?></td>
                    <td><?=$status;?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="push-right"><a href="incident_manage.php">See All</a></div>
<?php
}
else {
	header("Location: index.php");
}
?>