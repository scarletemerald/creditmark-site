<?php include("header.php"); ?>
<?php 
if (!$login && !empty($adapter) && !empty($user_data)) { // Social Login
	
	$profileType = $adapter->id;
	$identifier = $user_data->identifier;
	$time = date("Y-m-d H:i:s");
	
	$link = dbConnect();
	$sSQL = "SELECT * FROM user_social WHERE identifier = '".mysqli_real_escape_string($link, $identifier)."' AND profileType = '".mysqli_real_escape_string($link, $profileType)."'";
	$aRs = mysqli_query($link, $sSQL);
	if (mysqli_num_rows($aRs) > 0) { // Already registered
		$data = mysqli_fetch_assoc($aRs);
		$user_id = $data['user_id'];
		$sSQL = "SELECT * FROM user WHERE user_id = '".mysqli_real_escape_string($link, $user_id)."'";
		$aRs = mysqli_query($link, $sSQL);
		if (mysqli_num_rows($aRs) > 0) {
			$data = mysqli_fetch_assoc($aRs);
			
			$_SESSION['AP_login'] = "true";
			$_SESSION['AP_uid'] = $data['user_id'];
			$_SESSION['AP_ut'] = $data['user_type'];
			$_SESSION['AP_eml'] = $data['user_email'];
			echo '<script>window.location.reload();</script>';
		}
		else {
			echo '<div class="container">An unknown error has occurred. Please <a href="mailto:service@creditmark.org">contact us</a> for assistance.</div>';
		}
	}
	else { // Not registered yet
	
		$sSQL = "SELECT * FROM user WHERE user_email = '".mysqli_real_escape_string($link, $user_data->email)."'";
		$aRs = mysqli_query($link, $sSQL);
		if (mysqli_num_rows($aRs) > 0) {
			echo '<div class="container">Your Social Network Email Address is already a registered user with CreditMark. Please <a href="login.php">login</a> with your email instead.</div>';
		}
		else {
			$password = generateSerial(10);
			
			// Store in User Table
			$sSQL2 = "INSERT INTO user (user_name, user_type, user_email, user_password, user_phone, user_photo, registered_time, last_modified_time)
	VALUES
				('".mysqli_real_escape_string($link, $user_data->displayName)."', 
				'1', 
				'".mysqli_real_escape_string($link, $user_data->email)."', 
				'".mysqli_real_escape_string($link, $password)."', 
				'".mysqli_real_escape_string($link, $user_data->phone)."', 
				'".mysqli_real_escape_string($link, $user_data->photoURL)."', 
				'".$time."', '".$time."')";
			$aRs2 = mysqli_query($link, $sSQL2);
			if ($aRs2) {
				$user_id = mysqli_insert_id($link);
				$dob = $user_data->birthYear."-".$user_data->birthMonth."-".$user_data->birthDay;
				
				// Store in User Social Table
				$sSQL3 = "INSERT INTO user_social (identifier, profileType, user_id, profileURL, photoURL, displayName, firstName, lastName, gender, dob, email, phone, address, country, region, city, zip, created_time, last_modified_time)
	VALUES
					('".mysqli_real_escape_string($link, $identifier)."', 
					'".mysqli_real_escape_string($link, $profileType)."', 
					'".mysqli_real_escape_string($link, $user_id)."', 
					'".mysqli_real_escape_string($link, $user_data->profileURL)."', 
					'".mysqli_real_escape_string($link, $user_data->photoURL)."', 
					'".mysqli_real_escape_string($link, $user_data->displayName)."', 
					'".mysqli_real_escape_string($link, $user_data->firstName)."', 
					'".mysqli_real_escape_string($link, $user_data->lastName)."', 
					'".mysqli_real_escape_string($link, $user_data->gender)."', 
					'".mysqli_real_escape_string($link, $dob)."', 
					'".mysqli_real_escape_string($link, $user_data->email)."', 
					'".mysqli_real_escape_string($link, $user_data->phone)."', 
					'".mysqli_real_escape_string($link, $user_data->address)."', 
					'".mysqli_real_escape_string($link, $user_data->country)."', 
					'".mysqli_real_escape_string($link, $user_data->region)."', 
					'".mysqli_real_escape_string($link, $user_data->city)."',  
					'".mysqli_real_escape_string($link, $user_data->zip)."', 
					'".$time."', '".$time."')";
				
				$aRs3 = mysqli_query($link, $sSQL3);
				
				$_SESSION['AP_login'] = "true";
				$_SESSION['AP_uid'] = $user_id;
				$_SESSION['AP_ut'] = 1;
				$_SESSION['AP_eml'] = $user_data->email;
				$error = '<script>window.location="account.php?edit=1";</script>';
			}
		}
	}
}
else if ($login) { 
$user = getUserAccount(); 
//if ($user['user_company'] == "") $_GET['edit'] = 1;

$companys = array();
$grid = 6;
if ($_SESSION['AP_ut'] == 2) {
	$companys = getCustomer("", -1, "", "", " AND user_id = '".$_SESSION['AP_uid']."'");
	$grid = 12;
}
else {
	/*
	if (checkOustandingInvoices()) {
		echo '<script>';
		echo '$(function() {';
		echo 'if (confirm("You have outstanding UNPAID invoice(s) that is/are due. Proceed to update them accordingly?")) {';
		echo 'window.location = "incident_manage.php";';
		echo '}';
		echo '});';
		echo '</script>';
	}
	*/
}
?>
  <div class="container">
    <div class="row">
        <div class="grid_<?=$grid; ?>">
          <div class="header2">
            <h2><strong>My</strong> Account</h2>
          </div>
          <div class="post2">
            <div class="content">
              <table class="table">
                <tbody>
                  <tr>
                    <td rowspan="3" width="150" style="width: 150px; text-align: center; vertical-align: middle;" class="profilePhoto"><?php
					
                                if (is_file('upload/photo/'.$user['user_photo'])) {
                                ?>
                      <img src="upload/photo/<?=$user['user_photo'];?>" style="max-width: 150px; width: 150px; height: auto;" />
                      <?php	
                                }
								else if (preg_match("/^((http|ftp|https):\/\/)([a-z0-9]((([a-z0-9-])+)\.)+)(([a-z0-9])+)/i", $user['user_photo'])) { // Is URL
					?>
                    <img src="<?=$user['user_photo'];?>" style="max-width: 150px; width: 150px; height: auto;" />
                    <?php
								}
                                else {
                                ?>
                      <img src="upload/photo/profile.jpg" style="max-width: 150px; width: 150px; height: auto;" />
                      <?php
                                } 
                                ?></td>
                    <td class="strong"><strong>
                      <?=$user['user_name'];?>
                      </strong></td>
                  </tr>
                  <tr>
                    <td>
                    <?php if ($_SESSION['AP_ut'] == 2) { echo $user['user_job_title'].'<br />'; } ?>
					<?=$user['user_email'];?>
                      <br />
                      <?=$user['user_phone'];?></td>
                  </tr>
                  <tr>
                    <td><a class="btn-small" href="#" onclick="openModal('account_edit.php');">[edit]</a></td>
                  </tr>
                  <tr>
                    <td colspan="2"><hr /></td>
                  </tr>
                  <?php if ($_SESSION['AP_ut'] == 1) { ?>
                  <tr>
                    <td><strong>Company Name</strong></td>
                    <td><?=$user['user_company'];?></td>
                  </tr>
                  <?php if ($user['user_paypal'] != "") { ?>
                  <tr>
                    <td><strong>Paypal Account</strong></td>
                    <td><?=$user['user_paypal'];?></td>
                  </tr>
                  <?php } ?>
                  <?php if ($user['user_bank_branch'] != "") { ?>
                  <tr>
                    <td><strong>Bank Branch Code</strong></td>
                    <td><?=$user['user_bank_branch'];?></td>
                  </tr>
                  <?php } ?>
                  <?php if ($user['user_bank_acc'] != "") { ?>
                  <tr>
                    <td><strong>Bank Account No.</strong></td>
                    <td><?=$user['user_bank_acc'];?></td>
                  </tr>
                  <?php } ?>
                  <?php if ($user['user_bank_address'] != "") { ?>
                  <tr>
                    <td><strong>Bank Address</strong></td>
                    <td><?=$user['user_bank_address'];?></td>
                  </tr>
                  <?php } ?>
                  <?php if ($user['user_bank_swift'] != "") { ?>
                  <tr>
                    <td><strong>Bank Swift Code</strong></td>
                    <td><?=$user['user_bank_swift'];?></td>
                  </tr>
                  <?php } ?>
                  <?php if ($user['user_bank_iban'] != "") { ?>
                  <tr>
                    <td><strong>Bank IBAN</strong></td>
                    <td><?=$user['user_bank_iban'];?></td>
                  </tr>
                  <?php } ?>
                  <?php } else if ($_SESSION['AP_ut'] == 2) { 
				 ?>
                 <tr>
                    <td><strong>Claimed Company</strong></td>
                    <td>
                  <?php
				  		if (sizeof($companys) > 0) {
							foreach ($companys as $company) {
				 ?>
                  <?='<a href="#coy'.md5($company['cust_id']).'">'.$company['company_name'].'</a>; ';?>
                  <?php }
				  } else { ?>
                  <a href="search_company_main.php">Click here to search for your customer(s)</a>
                  <?php } ?>
                  </td>
                  </tr>
                  <?php 
				   } ?>
                </tbody>
              </table>
              <div class="modal fade response-message">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title">Editing My Account</h4>
                    </div>
                    <div class="modal-body">
                      <iframe id="modalcontent" src="" width="100%" height="500px"></iframe>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php if ($_SESSION['AP_ut'] == 1) { ?>
        <div class="grid_6">
          <div class="header2">
            <h2><strong>Search</strong> Customer(s)</h2>
          </div>
          <div class="post2">
            <div class="content" style="padding: 10px;">
            <div class="sselect"style="width: 90%;" >
                            <select name="coycty" id="coycty" class="select" onChange="updateSearch($(this).val());">
                            <option value="SGP" selected>Singapore</option>
                    	<option value="AUS">Australia</option>
                            </select>
                        </div>
        	<input name="coy" id="coy" type="text" style="width: 90%;" placeholder="Enter your corporate customer's full name" class="coysearch" /><a href="javascript:void(0);" onClick="<?php if ($login) { ?>openModal('search_company.php?query='+encodeURIComponent($('#coy').val())+'&cty='+encodeURIComponent($('#coycty').val()));<?php } else { ?>alert('Please login to view search results.');<?php } ?>" class="btn-default" style="width: 100%;">Search</a>
        	</div>
           </div>
           <div class="header2">
            <h2><strong>Your</strong> review(s)</h2>
          </div>
          <div class="post2">
            <div class="content">
              <?php include('latest_review.php'); ?>
            </div>
          </div>
        </div>
        <?php } else if ($_SESSION['AP_ut'] == 2) { ?>
        	
        <div class="grid_12">
          <div class="header2">
            <h2><strong>Search</strong> Customer(s)</h2>
          </div>
          <div class="post2">
            <div class="content" style="padding: 10px;">
            <div class="sselect" >
                            <select name="coycty" id="coycty" class="select" onChange="updateSearch($(this).val());">
                            <option value="SGP" selected>Singapore</option>
                    	<option value="AUS">Australia</option>
                            </select>
                        </div>
        	<input name="coy" id="coy" type="text" style="width: 57%; margin: 0px;" placeholder="Enter your corporate customer's full name" class="coysearch" /><a href="javascript:void(0);" onClick="<?php if ($login) { ?>openModal('search_company.php?query='+encodeURIComponent($('#coy').val())+'&cty='+encodeURIComponent($('#coycty').val()));<?php } else { ?>alert('Please login to view search results.');<?php } ?>" class="btn-default" style="display: inline-block;">Search</a>
        	</div>
           </div>
          <?php 
		  foreach ($companys as $company) {
			  $_REQUEST['cid'] = base64_encode($company['cust_id']);
		  ?>
          <div class="header2">
            <h2><a name="coy<?=md5($company['cust_id']);?>"></a><strong>Company</strong> Page</h2>
          </div>
          <div class="post2">
            <div class="content">
			  	  <?php include('company_page.php'); ?>
            </div>
        </div>
		  <?php
		  }
		  ?>
          </div>
        <?php } ?>
    </div>
  </div>
  
	<link href="css/autocomplete.css" media="all" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.autocomplete.js"></script>
    <script>
	var options, a;
	jQuery(function(){
		options = { serviceUrl:'getCustomers.php', showNoSuggestionNotice:true, minChars:3, lookupLimit:10, deferRequestBy: 500, orientation: 'auto', params: { 'cty': 'SGP' },
		onSearchStart: function (query) {
			$('.autocomplete-suggestions').html('<div class="autocomplete-suggestion" data-index="0">Please wait while we search our extensive database. This might take a few moments.</div>').show();
		},
		onSelect: function (suggestion) {
			$.post('track.php', { 't': 'SEARCH', 'i': suggestion.id });
		}};
		a = $('input.coysearch').autocomplete(options);
	});
	function updateSearch(val) {
		options['params'] = { 'cty': val };
		console.log(options);
		$('input.coysearch').autocomplete('setOptions', options);
	}
	</script>
  <?php /*if ($_GET['edit'] == 1) { ?>
  <script>
  $(document).ready(function() {
  	openModal('account_edit.php');
  });
  </script>
  <?php }*/ ?>
  <?php include_once("footer.php"); ?>
<?php
}
else {
	header("Location: index.php");
}
?>