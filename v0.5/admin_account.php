<?php include("header.php"); ?>
<?php 
if ($login && $_SESSION['AP_ut'] == 9999) { 
$user = getAdminAccount(); 
if ($user['user_company'] == "") $_GET['edit'] = 1;

$companys = array();
$grid = 12;
?>
  <div class="container">
    <div class="row">
        <div class="grid_<?=$grid; ?>">
          <div class="header2">
            <h2><strong>My</strong> Account</h2>
          </div>
          <div class="post2">
            <div class="content">
              <table class="table">
                <tbody>
                  <tr>
                    <td rowspan="3" width="150" style="width: 150px; text-align: center; vertical-align: middle;" class="profilePhoto"><?php
					
                                if (is_file('upload/photo/'.$user['user_photo'])) {
                                ?>
                      <img src="upload/photo/<?=$user['user_photo'];?>" style="max-width: 150px; width: 150px; height: auto;" />
                      <?php	
                                }
								else if (preg_match("/^((http|ftp|https):\/\/)([a-z0-9]((([a-z0-9-])+)\.)+)(([a-z0-9])+)/i", $user['user_photo'])) { // Is URL
					?>
                    <img src="<?=$user['user_photo'];?>" style="max-width: 150px; width: 150px; height: auto;" />
                    <?php
								}
                                else {
                                ?>
                      <img src="upload/photo/profile.jpg" style="max-width: 150px; width: 150px; height: auto;" />
                      <?php
                                } 
                                ?></td>
                    <td class="strong"><strong>
                      <?=$user['user_name'];?>
                      </strong></td>
                  </tr>
                  <tr>
                    <td>
                    <?php if ($_SESSION['AP_ut'] == 2) { echo $user['user_job_title'].'<br />'; } ?>
					<?=$user['user_email'];?>
                      <br />
                      <?=$user['user_phone'];?></td>
                  </tr>
                  <tr>
                    <td><a class="btn-small" href="#" onclick="$('.modal').modal();">[edit]</a></td>
                  </tr>
                </tbody>
              </table>
              <div class="modal fade response-message">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title">Editing My Account</h4>
                    </div>
                    <div class="modal-body">
                      <iframe src="account_edit.php" width="100%" height="500px"></iframe>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
  <?php include_once("footer.php"); ?>
<?php
}
else {
	header("Location: index.php");
}
?>