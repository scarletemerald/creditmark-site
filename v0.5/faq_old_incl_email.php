<?php include_once('header.php'); ?>
<div class="container">
        <div class="row">
            <div class="grid_12">
                <div class="header2">
                    <h2><strong>FAQ</strong></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="grid_12">
            	<div class="post4">
                    <class="label">What is CreditMark?<br>
                    <p>CreditMark helps suppliers get an idea of how long a prospective customer is likely to take to pay their invoices, based on the experiences of other suppliers.<br>
                    CreditMark also provides customers the opportunity to respond with details of their procurement policy.</p>
				</div>
                <div class="post4">
                    <a href="#" class="label">Who should use CreditMark?</a>
                    <p>CreditMark is used by suppliers large and small, including sole traders, freelancers - anyone who wants to know the likely time frame for getting paid <u><i>before</i></u> deciding whether to supply a product or service to a customer.</p>
				</div>
                <div class="post4">
                    <a href="#" class="label">How does CreditMark work?</a>
                    <p>CreditMark users give their customers a rating and share their past payment track record.<br>
                    By aggregating this data anonymously, suppliers get an idea when the customer is likely to settle their invoices.</p>
				</div>
                <div class="post4">
                    <a href="#" class="label">What information does CreditMark collect?</a>
                    <p>By contributing information about a customer's payment track record, you help other suppliers decide whether to provide products and services to them.<br>
                    To rate a customer, you will be prompted for details about a particular invoice you issued to them, namely the date of issue, the due date, and the date payment was received (if any).<br>
                    We <i>do not</i> need to know the amount of the invoice, the identity of your contact person at the company, and so on.<br>
                    You can provide the payment track record of as many customers and as many invoices as you wish. The more you contribute, the more you help the CreditMark community globally.<br>
                    If you opt in to use our email reminder service, you will be asked for additional information about your customer, including contact name and email address, and additional invoice details. See below for further details.</p>
				</div>
                <div class="post4">
                    <a href="#" class="label">How does CreditMark use the information I submit?</a>
                    <p>Customer payment track record information which suppliers provide is aggregated anonymously to derive an average time the customer takes to pay their invoices.<br>
                    Please see our <a href="privacy.php">privacy policy</a> for further details.</p>
				</div>
                <div class="post4">
                    <a href="#" class="label">What if no one has provided a review of a customer I searched for?</a>
                    <p>We list top search results on our home page. The customers searched for most often, and for which no payment track record has been provided, appear higher on the list.<br>
                    We encourage users who have supplied products or services to that customer to provide their payment track record, which helps other suppliers understand how long they took to pay.</p>
				</div>
                <div class="post4">
                    <a href="#" class="label">How accurate is the data?</a>
                    <p>The data is only as accurate as users provide, which is why it is important for suppliers to contribute payment track record data, and ensure it is entered accurately.<br>
                    We do not check the accuracy, and make no warranty that the payment track record data is correct.<br>
                    For this reason, we also allow customers to claim their profile and clarify their procurement processes.<br>
                    If in doubt, suppliers should have an open and friendly conversation with the customer about payment terms.<br>
                <div class="post4">
                    <a href="#" class="label">Are details of unpaid invoices included in the ratings?</a>                    
                    No, an open invoice which has not been settled does not contribute to the aggregated ratings.<br>
                    Payment track record data is only included once you have marked the invoice as paid.</p>
				</div>
                <div class="post4">
                    <a href="#" class="label">How can I rate a customer who has left my invoices open indefinitely?</a>
                    <p>Even though your invoice is not included in the aggregated rating until it is settled, you can still award 1-5 stars to the customer for their payment track record.</p>
				</div>
                <div class="post4">
                    <a href="#" class="label">How much does CreditMark cost?</a>
                    <p>CreditMark is free during beta testing.</p>
				</div>
           	<div class="post4">
                    <a href="#" class="label">Can CreditMark help me chase payments?</a>
                    <p>One of CreditMark's value-added resources is an automated email reminder that reminds your customers to settle your invoices. It saves you time and effort, and avoids miscommunication about the timing of payment before it arises.</p>
				</div>
                <div class="post4">
                    <a href="#" class="label">How does the email reminder work?</a>
                    <p>CreditMark asks you for a few details about your invoice, and then automatically messages your customers, courteously and professionally, at strategically important points in time. A calendar entry also helps remind your customer when your invoice is due.</p>
				</div>
                <div class="post4">
                    <a href="#" class="label">When should I use the email reminder service?</a>
                    <p>Ideally, you should activate the CreditMark emails as soon as you send your invoice to your customer. The first mailing goes out about half-way through your credit term (eg, on Day 15 if you have given your customer 30 days to pay, or Day 30 if you have 60 day credit terms). This ensures the customer is reminded before the due date, and any questions or comments the customer has can be discussed early in the process.</p>
				</div>
                <div class="post4">
                    <a href="#" class="label">What will my customers receive?</a>
                    <p>Your customers will receive courteous and professionally-written emails at strategic points during and after your credit term, which reminds them to settle your invoice. A calendar entry is also sent for the due date.</p>
				</div>
                <div class="post4">
                    <a href="#" class="label">What feedback can I get from my customers?</a>
                    <p>Each email contains a link your recipients can click, in order to interact with you, for example, if there was a query regarding the amount, or the legal name to who you made your invoice out to.</p>
				</div>
                <div class="post4">
                    <a href="#" class="label">Can I pause the campaign?</a>
                    <p>Yes, any time. Plus you receive an email alert one day before the email is sent out, so that you can still pause the campaign if the customer has paid in the meantime.</p>
				</div>
                <div class="post4">
                    <a href="#" class="label">What if the customer still doesn't pay?</a>
                    <p>Be sure to keep your CreditMark profile updated and provide the relevant review of the customer.<br>
                    Browse the Resources page for other tips for avoiding and for collecting bad debts.</p>
				</div>
                <div class="post4">
                    <a href="#" class="label">When should I activate the email campaign?</a>
                    <p>Ideally, as soon as you issue the invoice, but you can start the campaign at any time.</p>
                </div>
            </div>
        </div>
    </div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-27668603-3', 'auto');
  ga('send', 'pageview');

</script>
<?php include_once('footer.php'); ?>