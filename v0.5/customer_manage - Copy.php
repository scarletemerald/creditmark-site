<?php include("header.php"); ?>
<?php if ($login) { 
$custs = getCustomer(); 
$avgval = array();
$minval = 0;
$maxval = 0;
?>
<script src="js/Chart.js-master/Chart.min.js" type="text/javascript"></script>
<div class="container">
    <div class="row">
        <div class="grid_12">
          <div class="header2">
            <div class="push-right"><button class="btn btn-medium" onClick="openModal('customer_edit.php');">add new customer</button></div>
            <h2><strong>Manage</strong> Customer(s)</h2>
          </div>
          <table class="table data">
        	<thead>
            	<tr>
                    <td>Company</td>
                    <td width="200">Days until due date<br />(your invoices)</td>
                    <td width="200">Average time to pay<br />(All CreditMark customers)</td>
                </tr>
            </thead>
            <tbody>
            <?php if (sizeof($custs) > 0) {
			foreach ($custs as $cust) { 
				$incidents = getIncident("", "", "", $cust['cust_id']);
			?>
            	<tr>
                    <td><a href="#" onclick="openModal('customer_edit.php?cid=<?=$cust['cust_id'];?>');"><?=$cust['company_name'];?></a></td>
                    <td><?php
					$paidInvoices = 0;
					$daystook = 0;
					foreach ($incidents as $inc) {
						if ($inc['payment_received'] != "") {
							$daystook += floor((strtotime($inc['payment_received']) - strtotime($inc['invoice_date']))/24/60/60);
							$paidInvoices++;
						}
					}
					echo $avgval[$cust['company_name']][0] = number_format(round($daystook/$paidInvoices, 2), 2);
					 ?>
                    </td>
                    <td><?php 
					$oCusts = getCustomer("", -1, "", "", " AND company_name RLIKE '".mysql_real_escape_string($cust['company_name'])."'");
					
					if (empty($oCusts)) {
						echo $avgval[$cust['company_name']][0];
					}
					else {
						$paidInvoices = 0;
						$daystook = 0;
						foreach ($oCusts as $ocust) {
							$oIncs = getIncident("", -1, "", $ocust['cust_id']);
							foreach ($oIncs as $oinc) {
								if ($oinc['payment_received'] != "") {
									$daystook += floor((strtotime($oinc['payment_received']) - strtotime($oinc['invoice_date']))/24/60/60);
									$paidInvoices++;
								}
							}
						}
						echo $avgval[$cust['company_name']][1] = number_format(round($daystook/$paidInvoices, 2), 2);
					}
					?></td>
                </tr>
            <?php }
			} else { ?>
            	<tr>
                	<td colspan="3">You have no customer yet. Please click above to add one now.</td>
                </tr>
            <?php } ?>
            </tbody>
          </table>
          <br />
          <div style="width: 100%">
                <canvas id="canvas" height="450" width="940"></canvas>
            </div>
          <br />
          <table class="table data">
        	<thead>
            	<tr>
                    <td>Company</td>
                    <td width="150">No of Contact(s)</td>
                    <td width="150">No of Case(s)</td>
                    <td width="150">Avg Time of Payment</td>
                    <td width="80">Actions</td>
                </tr>
            </thead>
            <tbody>
            <?php if (sizeof($custs) > 0) {
			foreach ($custs as $cust) { 
				$incidents = getIncident("", "", "", $cust['cust_id']);
			?>
            	<tr>
                    <td><a href="#" onclick="openModal('customer_edit.php?cid=<?=$cust['cust_id'];?>');"><?=$cust['company_name'];?></a></td>
                    <td><?='<a href="#" onclick="openModal(\'customer_edit.php?cid='.$cust['cust_id'].'\');">'.sizeof($cust['contact']).'</a>';?></td>
                    <td><?php if (sizeof($incidents) <= 0) echo '<a href="incident_manage.php?add=1&cid='.$cust['cust_id'].'">Add New</a>'; else echo '<a href="incident_manage.php?cid='.$cust['cust_id'].'">'.sizeof($incidents).'</a>';?></td>
                    <td><?php
					$paidInvoices = 0;
					$daystook = 0;
					foreach ($incidents as $inc) {
						if ($inc['payment_received'] != "") {
							$daystook += floor((strtotime($inc['payment_received']) - strtotime($inc['invoice_date']))/24/60/60);
							$paidInvoices++;
						}
					}
					echo round($daystook/$paidInvoices, 2);
					 ?>
                    </td>
                    <td><a href="#" onclick="openModal('customer_edit.php?cid=<?=$cust['cust_id'];?>');"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x" title="edit"></i><i class="fa fa-pencil fa-stack-1x fa-inverse" title="edit"></i></span></a>
                    <?php if (sizeof($incidents) > 0) { ?><a href="#" onclick="deleteThis('<?=$cust['cust_id'];?>');"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x" title="Pause All Cases"></i><i class="fa fa-pause fa-stack-1x fa-inverse" title="Pause All Cases"></i></span></a><?php } ?></td>
                </tr>
            <?php }
			} else { ?>
            	<tr>
                	<td colspan="4">You have no customer yet. Please click above to add one now.</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="modal fade response-message">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title">Customer</h4>
                    </div>
                    <div class="modal-body">
                      <iframe id="modalcontent" src="" width="100%" height="500px"></iframe>
                    </div>
                  </div>
                </div>
              </div>
      </div>
    </div>
</div>
<script>
function openModal(url) {
	$('#modalcontent').attr('src', url);
	$('.modal').modal();
}
function deleteThis(id) {
	if (confirm("Are you sure you wish to paused all Cases for this Customer?")) {
		$.post('bat/customer_del.php', { 'id': id }, function(d) {
			alert(d);
			window.location.reload();
		});
	}
}
$(document).ready(function() {
	
<?php if ($_GET['add'] == 1) { ?>
	openModal('customer_edit.php');
<?php } ?>
<?php if (!empty($avgval)) { ?>
	var barChartData = {
		labels : [<?php
		$cnt = 0;
		foreach ($avgval as $coy=>$val) { 
			if ($cnt > 0) echo ', ';
			echo '"'.$coy.'"';
			$cnt++;
		}
		?>],
		datasets : [
			{
				label	: "Days until due date (your invoices)",
				fillColor : "rgba(220,220,220,0.5)",
				strokeColor : "rgba(220,220,220,0.8)",
				highlightFill: "rgba(220,220,220,0.75)",
				highlightStroke: "rgba(220,220,220,1)",
				data : [<?php
				$cnt = 0;
				foreach ($avgval as $coy=>$val) { 
					if ($cnt > 0) echo ', ';
					echo $val[0];
					$cnt++;
				}?>]
			},
			{
				label	: "Average time to pay (All CreditMark customers)",
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : [<?php
				$cnt = 0;
				foreach ($avgval as $coy=>$val) { 
					if ($cnt > 0) echo ', ';
					echo $val[1];
					$cnt++;
				}?>]
			}
		]
	}
	
	var options = {
		legendTemplate : "<ul class=\"Days until due date (your invoices)-legend\"><li><div style=\"background-color:rgba(220,220,220,0.5); display: inline-block; width: 20px; height: 20px; margin-right: 10px; vertical-align: middle;\"></div>Days until due date (your invoices)</li><li><div style=\"background-color:rgba(151,187,205,0.5); display: inline-block; width: 20px; height: 20px; margin-right: 10px; vertical-align: middle;\"></div>Average time to pay (All CreditMark customers)</li></ul>"
	}
	// Get context with jQuery - using jQuery's .get() method.
	var ctx = $("#canvas").get(0).getContext("2d");
	// This will get the first returned node in the jQuery collection.
	var myNewChart = new Chart(ctx).Bar(barChartData, options).generateLegend();
	$('#canvas').parent('div').append(myNewChart);
<?php } ?>
});
</script>

<?php include_once("footer.php"); ?>
<?php
}
else {
	header("Location: index.php");
}
?>