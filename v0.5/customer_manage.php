<?php if ($login) { 
$contacts = getCustomerContact("", $_SESSION['AP_uid']);
$custs = array();
foreach ($contacts as $contact) {
	$custs[$contact['cust_id']] = getCustomer($contact['cust_id']);
}
?>
<div class="container">
    <div class="row">
        <div class="grid_12">
          <div class="header2">
            <div class="push-right"><button class="btn btn-medium" onClick="openModal('customer_edit.php');">add new contact</button></div>
            <h2><strong>Manage</strong> Customer(s)</h2>
          </div>
          <table class="table data">
        	<thead>
            	<tr>
                    <td>Company</td>
                    <td width="150">No of Contact(s)</td>
                    <td width="150">No of Case(s)</td>
                    <td width="150">Avg Time of Payment</td>
                    <td width="80">Actions</td>
                </tr>
            </thead>
            <tbody>
            <?php if (sizeof($custs) > 0) {
			foreach ($custs as $cust) {
				$cust = array_pop($cust);
				$incidents = getIncident("", "", "", $cust['cust_id']);
			?>
            	<tr>
                    <td><a href="#" onclick="openModal('customer_edit.php?cid=<?=$cust['cust_id'];?>');"><?=$cust['company_name'];?></a></td>
                    <td><?='<a href="#" onclick="openModal(\'customer_edit.php?cid='.$cust['cust_id'].'\');">'.sizeof($cust['contact']).'</a>';?></td>
                    <td><?php if (sizeof($incidents) <= 0) echo '<a href="incident_manage.php?add=1&cid='.$cust['cust_id'].'">Add New</a>'; else echo '<a href="incident_manage.php?cid='.$cust['cust_id'].'">'.sizeof($incidents).'</a>';?></td>
                    <td><?php
					$paidInvoices = 0;
					$daystook = 0;
					foreach ($incidents as $inc) {
						if ($inc['payment_received'] != "") {
							$daystook += floor((strtotime($inc['payment_received']) - strtotime($inc['invoice_date']))/24/60/60);
							$paidInvoices++;
						}
					}
					echo round($daystook/$paidInvoices, 2);
					 ?>
                    </td>
                    <td><a href="#" onclick="openModal('customer_edit.php?cid=<?=$cust['cust_id'];?>');"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x" title="edit"></i><i class="fa fa-pencil fa-stack-1x fa-inverse" title="edit"></i></span></a>
                    <?php if (sizeof($incidents) > 0) { ?><a href="#" onclick="deleteThis('<?=$cust['cust_id'];?>');"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x" title="Pause All Cases"></i><i class="fa fa-pause fa-stack-1x fa-inverse" title="Pause All Cases"></i></span></a><?php } ?></td>
                </tr>
            <?php }
			} else { ?>
            	<tr>
                	<td colspan="5">You have no customer yet. Please click above to add one now.</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        </div>
    </div>
</div>
<script>
function deleteThis(id) {
	if (confirm("Are you sure you wish to paused all Cases for this Customer?")) {
		$.post('bat/customer_del.php', { 'id': id }, function(d) {
			alert(d);
			window.location.reload();
		});
	}
}
$(document).ready(function() {
<?php if ($_GET['add'] == 1) { ?>
	openModal('customer_edit.php');
<?php } ?>
});
</script>
<?php
}
else {
	header("Location: index.php");
}
?>