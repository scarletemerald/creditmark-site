<?php include_once('header.php'); 

unset($_SESSION["AP_login"]);
unset($_SESSION["AP_uid"]);
unset($_SESSION["AP_ut"]);
unset($_SESSION["AP_eml"]);
setcookie('registered_user@creditmark.org', '', time()-3600);
?>
<style>
.idpico {
	cursor: pointer;
}
</style>
<link href="css/autocomplete.css" media="all" rel="stylesheet" type="text/css" />  
<link rel="stylesheet" href="js/raty/jquery.raty.css">
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<script src="js/raty/jquery.raty.js"></script>
<script>
$(document).ready(function() {
	$('.idpico').click(function() {
		var idp = $( this ).attr( "idp" );
		switch( idp ){
			case "facebook": case "linkedin" : 
				start_auth( "?provider=" + idp );
				break;
			default: alert( "u no fun" );
		}
	});
	
	options = { serviceUrl:'getCustomers.php', showNoSuggestionNotice:true, minChars:3, lookupLimit:10,
		onSelect: function (suggestion) {
			currentcoy = $('input.coysearch').val();
			var uen = suggestion.uen;
			var url = suggestion.url;
			
			if (uen != "") {
				$('input#rateBizRegNo').val(uen);
				$('.rateBizRegNo ._placeholder').addClass('hidden');
			}
			else
				$('.rateBizRegNo ._placeholder').removeClass('hidden');
			
			if (url != "") {
				$('input#rateCompanyURL').val(url);
				$('.rateCompanyURL ._placeholder').addClass('hidden');
			}
			else
				$('.rateCompanyURL ._placeholder').removeClass('hidden');
		}};
	a = $('input.coysearch').autocomplete(options);
	
	$('div.rating').raty({
		'half'			: true,
		'starHalf'		: 'js/raty/images/star-half.png',
		'starOff'		: 'js/raty/images/star-off.png',
		'starOn'		: 'js/raty/images/star-on.png',
		'scoreName'		: 'rateCompanyRatingVal',
		'hints'			: ['I am in a payment dispute with this customer', "This customer usually pays late/doesn't respond to enquiries", 'This customer usually pays late/responds to enquiries', 'This customer sometimes pays late/responds to enquiries', 'This customer always pays on time and is a pleasure to deal with']
	});
	
	$('input[name=rateCompanyRatingVal]').attr("data-constraints", "@Required");
});
function start_auth( params ){
	var start_url = 'bat/login_social.php' + params + "&return_to=<?php echo urlencode(WL_PATH."account.php"); ?>" + "&_ts=" + (new Date()).getTime();
	window.open(
		start_url, 
		"hybridauth_social_sing_on", 
		"location=0,status=0,scrollbars=0,width=800,height=500"
	);  
}
function showRegForm(val) {
	$('form.reg').hide();
	if (val == "Supplier") {
		$('#register-form').show();
	}
	else if (val == "Buyer") {
		$('#registerbuyer-form').show();
	}
	else {
		$('#register-choose').show();
	}
}

function calcDueDate(val) {
	var invDate = ($('#rateInvoiceDate').val()).split('-');
	invDate = invDate[2] + "-" + invDate[1] + "-" + invDate[0];
	var t1 = (new Date(invDate)).getTime();
	if (isNaN(t1)) {
	}
	else {
		var t2 = val*24*60*60*1000;
		var dueDate = new Date(t1+t2);
		dueDate = ((dueDate.getDate()<10)?"0"+dueDate.getDate():dueDate.getDate()) + "-" + ((dueDate.getMonth()+1<10)?"0"+(dueDate.getMonth()+1):dueDate.getMonth()+1) + "-" + dueDate.getFullYear();
		$('#rateInvoiceDueDate').val(dueDate);
		$('.rateInvoiceDueDate ._placeholder').addClass('hidden');
	
	}
}
function calcDueDay() {
	var invDate = ($('#rateInvoiceDate').val()).split('-');
	invDate = invDate[2] + "-" + invDate[1] + "-" + invDate[0];
	var t1 = (new Date(invDate)).getTime();
	if (isNaN(t1)) {
	}
	else {
		var dueDate = ($('#rateInvoiceDueDate').val()).split('-');
		dueDate = dueDate[2] + "-" + dueDate[1] + "-" + dueDate[0];
		var t2 = (new Date(dueDate)).getTime();
		
		var dueDays = Math.floor((t2-t1)/1000/3600/24);
		$('#rateInvoiceDue').val(dueDays).css('color', 'white');
	}
}
function showCertifyTxt(val) {
	if (val == "") {
		$('#certifytxt').html("I certify that the information provided above is correct, and I will update it once payment has been received. I confirm that the purpose of my review is purely to share my genuine experiences with the customer's payment processes in good faith. I have not been offered any incentive or payment by the customer to write this review or to alter a previous review.");
	}
	else {
		$('#certifytxt').html("I certify that the information provided above is correct. I confirm that the purpose of my review is purely to share my genuine experiences with the customer's payment processes in good faith. I have not been offered any incentive or payment by the customer to write this review or to alter a previous review.");
	}
}
</script>
<div class="container">
	<div class="row">
        <div class="grid_4">
            <form id="login-form" class="contact-form">
                <div class="contact-form-loader"></div>
                <div class="header">
                    <h3>Login</h3>
                </div>
                <fieldset>
                    <div class="row">
                        <div class="grid_4">
                            <label class="loginEmail">
                                <input type="text" name="loginEmail" placeholder="E-mail:" data-constraints="@Required @Email" />
                                <span class="empty-message">*This field is required.</span>
                                <span class="error-message">*This doesn't look right. Please check that this is a valid email.</span>
                            </label>
                            <label class="loginPassword">
                                <input type="password" name="loginPassword" placeholder="Password:" value="" data-constraints="@Required" />
                                <span class="empty-message">*This field is required.</span>
                            </label>
<!--                             
                            <label class="usePitchMark" style="height: 30px; color: white;">
                            <img float="right" src="images/pitchmark.png" title="PitchMark" width="50" />
                            	<input type="checkbox" name="usePitchMark" value="on" />
                            	Tick if you are logging in with your PitchMark ID
                            </label> -->
                        </div>
                    </div>
                    <div class="contact-form-buttons">
                        <a href="#" data-type="submit" class="btn-default">Login Now</a>
                    </div>
                </fieldset>
                <div class="modal fade response-message">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Login</h4>
                            </div>
                            <div class="modal-body">
                                Sorry, your login details didn't match our records. Please try again.
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            
            
                    <div class="row">
                    	<div class="grid_4" style="text-align: center;">
                            <br />
                            Register/Login With
                            <br />
                        	<img class="idpico" idp="linkedin" title="LinkedIn" src="images/linkedin.png" width="50" /> <img class="idpico" idp="facebook" src="images/facebook.png" title="Facebook" width="50" />
                            <br />
                            <br />
                            <a href="forgetpassword.php"><em>Forget Password?</em></a>
                        </div>
                    </div>
        </div>
        <div class="grid_8">
        	
        	<form id="register-choose" class="contact-form reg" enctype="multipart/form-data">
                <div class="contact-form-loader"></div>
                <div class="header">
                    <h3>Register</h3>
                </div>
                <fieldset>
                    <div class="row">
                        <div class="grid_8">
                            <p style="padding-top: 0px;">Please select whether you are a:</p>
                            
                            <label class="usertype-select" style="color: #fff;">
                                <input type="radio" name="usertype-select" id="usertype-select1" onClick="showRegForm(this.value);" value="Supplier" /> Supplier (You send Invoices to your customers)
                                <br />
                                <input type="radio" name="usertype-select" id="usertype-select2" onClick="showRegForm(this.value);" value="Buyer" /> Buyer (You receive Invoices from suppliers)
                            </label>
                            <p>If you wish to sign up as both a Supplier and a Buyer, please register separately.</p>
                            <p>Once you have registered as a Supplier, you will be invited immediately to add details about a recent invoice you have sent to a customer.</p>
                            <p>You will be prompted <u>only</u> for<br>1. The date of your invoice<br>2. Your credit terms (eg, 30 days), and<br>3. The date on which you actually received payment.</p>
                            <p>That's it!</p>
                            <p>We will <u>not</u> ask you for any other details (items, amounts, names and addresses, etc).</p>
                            <p>Remember, the more invoice details you submit, the more often you can search CreditMark.</p>
                            <p>Support the CreditMark community and help bring about real change in companies' payment policies by submitting details about as many invoices as possible.</p>
                        <br />
                        <br />
                        <br />
                        </div>
                    </div>
                </fieldset>
                <div class="modal fade response-message">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Register</h4>
                            </div>
                            <div class="modal-body">
                                For some reason your registration was unsuccessful. Sorry about that. Please try again. If the problem persists, please contact us.
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        
            <form id="register-form" class="contact-form reg" style="display: none;" enctype="multipart/form-data">
                <div class="contact-form-loader"></div>
                <div class="header">
                    <h3>Register</h3>
                </div>
                <fieldset>
                    <div class="row">
                        <div class="grid_8">
                            <p style="padding-top: 0px;">You have chosen to register as a SUPPLIER. Tell us about yourself.</p>
                            
                            <label class="accountUserName">
                                <a class="hastip" title="Your first and last name"><input type="text" name="accountUserName" placeholder="Your Name*" data-constraints="@Required" value="" /></a>
                                <span class="empty-message">*This field is required.</span>
                            </label>
                            
                            <label class="accountEmail">
                                <input type="text" title="This will be your login ID" name="accountEmail" placeholder="Your E-mail*" data-constraints="@Required @Email" value="" />
                                <span class="empty-message">*This field is required.</span>
                                <span class="error-message">*This doesn't look right. Please check that this is a valid email.</span>
                            </label>
                            <label class="accountPhone">
                                <a class="hastip" title="Enter numbers only - omit +-() symbols"><input type="tel" name="accountPhone" placeholder="Your Preferred Phone Number*" value=""
                                       data-constraints="@Required @JustNumbers" /></a>
                                <span class="empty-message">*This field is required.</span>
                                <span class="error-message">*Sorry, this doesn't match the format we require. Please only enter numbers.</span>
                            </label>
                            <label class="accountPassword">
                                <input type="password" name="accountPassword" placeholder="Password*" value="" data-constraints="@Required" />
                                <span class="empty-message">*This field is required.</span>
                            </label>
                            
                            <label class="accountPassword2">
                                <input type="password" name="accountPassword2" placeholder="Confirm Password*" value="" data-constraints="@Required @matchPassword" data-contraints-match="accountPassword" />
                                <span class="empty-message">*This field is required.</span>
                                <span class="error-message">*Sorry, the passwords do not match. Please check.</span>
                            </label>
                        </div>
                    </div>
                    <div class="contact-form-buttons">
                        <a href="#" data-type="submit" class="btn-default">Next</a>
                        <a href="#" onclick="showRegForm('');" class="btn-default">Cancel</a>
                    </div>
                </fieldset>
                <div class="modal fade response-message">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Register</h4>
                            </div>
                            <div class="modal-body">
                                For some reason your registration was unsuccessful. Sorry about that. Please try again. If the problem persists, please contact us.
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <?php /*
            <form id="register2-form" class="contact-form reg" style="display: none;" enctype="multipart/form-data">
                <div class="contact-form-loader"></div>
                <div class="header">
                    <h3>Register</h3>
                </div>
                <fieldset>
                    <div class="row">
                        <div class="grid_8">
                            <p style="padding-top: 0px;">The following details will be included in the outgoing CreditMark emails to your customers. Please ensure that they are correct.</p>
                            <p>Your Company &amp; Payment Details</p>
                            
                            <label class="accountCompanyName">
                                <input type="text" name="accountCompanyName" placeholder="Your customer Name*" data-constraints="@Required" value="" />
                                <span class="empty-message">*This field is required.</span>
                            </label>
                            
                            <label class="bankPayPal">
                            	<a class="hastip" title="<strong>Why do you need my PayPal details?</strong><br />If you provide it, we will display your PayPal email address in CreditMark emails,<br />in order to offer your customers a variety of payment avenues.<br />You can add this later by logging into your CreditMark account.<br /><br /><a href='http://www.paypal.com' target='_blank' style='color: white;'>Register for an Paypal account <u>here</u>.</a>"><input type="text" name="bankPayPal" class="@Email" placeholder="Your PayPal Account E-mail" value="" /></a>
                                <span class="error-message">*This is not a valid email.</span>
                            </label>
                            <label class="bankBranchCode">
                                <input type="text" name="bankBranchCode" placeholder="Your Bank Branch Code" value="" />
                            </label>
                            <label class="bankAccount">
                                <input type="text" name="bankAccount" placeholder="Your Bank Account Number" value="" />
                            </label>
                            <label class="bankAddress">
                                <input type="text" name="bankAddress" placeholder="Your Bank Address" value="" />
                            </label>
                            <label class="bankSwiftCode">
                                <input type="text" name="bankSwiftCode" placeholder="Your Bank Swift Code" value="" />
                            </label>
                            <label class="bankIBANCode">
                                <input type="text" name="bankIBANCode" placeholder="Your Bank IBAN Code" value="" />
                            </label>
                            
                            <a class="hastip" title="<strong>Preferred Dimension Ratio:</strong><br />120px (Width) by 150px (Height)<br><br><strong>Preferred File Type:</strong><br>.jpg, .png, .gif"><span class="btn btn-default btn-file">
                                Upload Profile Photo <input type="file" name="profilePhoto" placeholder="">
                            </span></a>
                        </div>
                    </div>
                    <div class="contact-form-buttons">
                        <a href="#" data-type="submit" class="btn-default">Next</a>
                    </div>
                </fieldset>
                <div class="modal fade response-message">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Register</h4>
                            </div>
                            <div class="modal-body">
                                Failed to Register. Please try again.
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            */ ?>
            <form id="register3-form" class="contact-form reg" style="display: none;" enctype="multipart/form-data">
                <div class="contact-form-loader"></div>
                <div class="header">
                    <h3>Rate a customer's payment processes</h3>
                </div>
                <fieldset>
                    <div class="row" style="margin-top: 0px; margin-bottom: 12px;">
                        <div class="grid_8">
                            <p style="padding-top: 0px;">Thank you for being part of the CreditMark community.</p>
                            <p>As a member, you play a critically important part in getting companies like yours paid faster.</p>
                            <p>Every time you share just a few details about your invoices to your customers below, you are bringing the community one step closer to better cash flow.</p>
                            <p>The information you provide is aggregated with our existing crowdsourced data. Your details will not be shared.</p>
                            <p>First, tell us a few details about your customer.</p>
                            <label class="rateCompanyName">
                                <input type="text" name="rateCompanyName" title="Full legal name, including 'LLP', 'Pty Ltd', 'Limited', etc" id="rateCompanyName" class="coysearch" placeholder="Your customer (company name)*" data-constraints="@Required" value="" />
                                <span class="empty-message">*Sorry, we can't proceed unless you tell us which company you are reviewing.</span>
                            </label>
                            <label class="rateBizRegNo">
                                <a class="hastip" title="Make absolutely certain that the customer you are reviewing and their business registration number match!"><input type="text" name="rateBizRegNo" id="rateBizRegNo" placeholder="Your customer's business registration number (eg, ACRA, ABN)*" data-constraints="@Required" value="" /></a>
                                <span class="empty-message">*We need to know your customer's corporate registration number for cross-referencing.</span>
                            </label>
                            <label class="rateCompanyURL" style="margin-bottom: 0px;">
                                <a class="hastip" title="Make absolutely certain that the customer you are reviewing and their web address match! Please include http://"><input type="text" name="rateCompanyURL" id="rateCompanyURL" placeholder="Company URL" data-constraints="@URL" value="" /></a>
                                <span class="error-message">*Sorry, the URL is in an incorrect format. It should look like this: http://www.company.com or http://prefix.company.com.</span>
                            </label>
                        </div>
                     </div>
                     <?php /*
                     <div class="row">
                     	<div class="grid_8">
                            <label class="rateContactName">
                                <input type="text" name="rateContactName" placeholder="Contact Person Name" value="" />
                            </label>
                            
                            <label class="rateContactEmail">
                                <input type="text" name="rateContactEmail" placeholder="Contact Person E-mail" data-constraints="@Email" value="" />
                                <span class="error-message">*This is not a valid email.</span>
                            </label>
                            
                            <hr style="width: 90%;" />
                        </div>
                     </div>
					 */ ?>
                     <div class="row" style="margin-bottom: 12px;">
                     	<div class="grid_8">
                            <p style="padding-top: 12px;">Describe this customer's payment process. Give them a rating out of five (hint: mouse-over the stars for descriptions): </p>
                            <label class="rateCompanyRating" style="height: auto;">
                            <div class="rating" id="rateCompanyRating"></div>
                            <span class="empty-message">*This field is required.</span>
                            </label>
                        	<p style="padding-top: 0px;">Are you a:</p>
                                
                                <label class="rateCompanyUserType" style="color: #fff; height: auto;">
                                    <input type="radio" name="rateCompanyUserType" id="rateCompanyUserType1" data-constraints="@JustChecked" data-contraints-match="rateCompanyUserType" value="Current supplier" /> Current supplier
                                    <br />
                                    <input type="radio" name="rateCompanyUserType" id="rateCompanyUserType3" data-constraints="@JustChecked" data-contraints-match="rateCompanyUserType" value="Former supplier" /> Former supplier
                                    <br /> 
                                    <input type="radio" name="rateCompanyUserType" id="rateCompanyUserType4" data-constraints="@JustChecked" data-contraints-match="rateCompanyUserType" value="Other" /> Other 								
                                    <span class="error-message">*This field is required.</span>
                                </label>
                         </div>
                    </div>
                    <div class="row" style="margin-bottom: 12px;">
                     	<div class="grid_8">
                            <p style="padding-top: 0px;">You submitted your invoice (tick as many as apply):</p>
                                
                                <label class="rateInvoiceSubmitted" style="color: #fff; height: auto;">
                                    <input type="checkbox" name="rateInvoiceSubmitted[]" id="rateInvoiceSubmitted1" data-constraints="@JustChecked" data-contraints-match="rateInvoiceSubmitted[]" value="Electronically, via the customer's procurement portal" /> Electronically, via the customer's procurement portal
                                    <br />
                                    <input type="checkbox" name="rateInvoiceSubmitted[]" id="rateInvoiceSubmitted2" data-constraints="@JustChecked" data-contraints-match="rateInvoiceSubmitted[]" value="Electronically, via regular email" /> Electronically, via regular email<br />
                                    <input type="checkbox" name="rateInvoiceSubmitted[]" id="rateInvoiceSubmitted3" data-constraints="@JustChecked" data-contraints-match="rateInvoiceSubmitted[]" value="Hard copy, delivered with the product/service" /> Hard copy, delivered with the product/service
                                    <br /> 
                                    <input type="checkbox" name="rateInvoiceSubmitted[]" id="rateInvoiceSubmitted4" data-constraints="@JustChecked" data-contraints-match="rateInvoiceSubmitted[]" value="Hard copy, snail mailed" /> Hard copy, snail mailed 								
                                    <span class="error-message">*This field is required.</span>
                                </label>
                         </div>
                    </div>
                    <div class="row" style="margin-bottom: 12px;">
                     	<div class="grid_8">
                                <label class="rateInvoiceConfirm" style="color: #fff; height: auto; line-height: 1.2">
                                    <a class="hastip" title="Do not check this box if your customer has pointed out errors in your invoice,<br />and/or you did not submit your invoice through their preferred channels."><input type="checkbox" name="rateInvoiceConfirm" id="rateInvoiceConfirm" value="1" />I confirm that I issued my invoice in accordance with the instructions of the customer.</a>
                                </label>
                         </div>
                    </div>
                    
                    <div class="row" style="margin-bottom: 12px;">
                     	<div class="grid_8">
                            <p style="padding-top: 0px;">The customer placed their order with me using (tick as many as apply):</p>
                                
                                <label class="ratePlaceOrder" style="color: #fff; height: auto;">
                                    <input type="checkbox" name="ratePlaceOrder[]" id="ratePlaceOrder1" data-constraints="@JustChecked" data-contraints-match="ratePlaceOrder[]" value="A purchase order (PO)" /> A purchase order (PO)
                                    <br />
                                    <input type="checkbox" name="ratePlaceOrder[]" id="ratePlaceOrder2" data-constraints="@JustChecked" data-contraints-match="ratePlaceOrder[]" value="A signed, hard copy quotation" /> A signed, hard copy quotation<br />
                                    <input type="checkbox" name="ratePlaceOrder[]" id="ratePlaceOrder3" data-constraints="@JustChecked" data-contraints-match="ratePlaceOrder[]" value="A digitally-signed quotation" /> A digitally-signed quotation
                                    <br /> 
                                    <input type="checkbox" name="ratePlaceOrder[]" id="ratePlaceOrder4" data-constraints="@JustChecked" data-contraints-match="ratePlaceOrder[]" value="A telephone/in-person conversation" /> A telephone/in-person conversation<br />
                                    <input type="checkbox" name="ratePlaceOrder[]" id="ratePlaceOrder5" data-constraints="@JustChecked" data-contraints-match="ratePlaceOrder[]" value="Other" /> Other
                                    <span class="error-message">*This field is required.</span>
                                </label>
                         </div>
                    </div>
                    <div class="row" style="margin-bottom: 12px;">
                     	<div class="grid_8">
                                <label class="rateInvoiceConfirm2" style="color: #fff; height: auto; line-height: 1.2">
                                    <a class="hastip" title="Do not check this box if your product/service<br />did not meet the specifications of your customer."><input type="checkbox" name="rateInvoiceConfirm2" id="rateInvoiceConfirm2" value="1" />I confirm that the products/services were delivered according to specifications.</a>
                                </label>
                                <hr style="width: 90%;" />
                         </div>
                    </div>
                    <div class="row" style="margin-bottom: 25px;">
                     	<div class="grid_8">
                        	<p style="padding-top: 0px;">Now, tell us about your most recent invoice to the customer.</p>
                        	<?php /*<label class="rateInvoiceNo">
                                <input type="text" name="rateInvoiceNo" placeholder="Invoice Number" value="" />
                            </label> */ ?>
                            <label class="rateInvoiceDate">
                                <a class="hastip" title="Date Format: DD-MM-YYYY"><input type="text" name="rateInvoiceDate" id="rateInvoiceDate" placeholder="The invoice is dated...*" data-constraints="@Required @JustDate" value="" /></a>
                                <span class="empty-message">*This field is required.</span>
                                <span class="error-message">*Date format as dd-mm-yyyy.</span>
                            </label>
                            <?php /*
                        	<label class="rateInvoiceCurrency">
                                <select name="rateInvoiceCurrency" id="rateInvoiceCurrency" class="select-menu">
                                    <option value="">Invoice Currency</option> 
                                    <?php foreach ($currencyArr as $cur) { ?>
                                    <option value="<?=$cur;?>"><?=$cur;?></option>
                                    <?php } ?>
                                </select>
                            </label>
                            
                            <label class="rateInvoiceAmount">
                                <input type="text" name="rateInvoiceAmount" placeholder="Invoice Amount" data-constraints="@JustDigits" value="<?=$incident['invoice_amount'];?>" />
                                <span class="error-message">*Please enter only numbers.</span>
                            </label>
							*/ ?>
                            
                            <label class="rateInvoiceDue">
                                <select name="rateInvoiceDue" id="rateInvoiceDue" class="select-menu" data-constraints="@Required" onchange="calcDueDate(this.value);">
                                    <option value="">*Your Invoice to the customer was due after...</option> 
                                    <optgroup label="Standard Days">
                                        <option value="14">14 Days</option>
                                        <option value="30">30 Days</option>
                                        <option value="60">60 Days</option>
                                        <option value="90">90 Days</option>
                                        <option value="120">120 Days</option>
                                    </optgroup>
                                    <optgroup label="Other Days">
                                    <?php for ($i=1; $i<=365; $i++) { 
                                        if (!in_array($i, array(14, 30, 60, 90, 120))) {
                                    ?>
                                    <option value="<?=$i;?>"><?=$i;?> Days</option>
                                    <?php }
                                    } ?>
                                    </optgroup>
                                </select>
                                <span class="empty-message">*This field is required.</span>
                            </label>
                            <label class="rateInvoiceDueDate">
                               <a class="hastip" title="Date Format: DD-MM-YYYY"> <input type="text" name="rateInvoiceDueDate" id="rateInvoiceDueDate" placeholder="Your Invoice to the customer was due after...*" data-constraints="@Required @JustDate" value="" onblur="calcDueDay()" /></a>
                                <span class="empty-message">*This field is required.</span>
                                <span class="error-message">*Date format as dd-mm-yyyy.</span>
                            </label>
                            
                            <?php /*
                            <label class="rateInvoiceInterest">
									<input type="text" name="rateInvoiceInterest" placeholder="Interest Due on Overdue Invoices (%)" data-constraints="@JustDigits" value="" />
									<span class="error-message">*Please enter only numbers.</span>
								</label>
								
								<label class="rateInvoiceInterestPeriod">
									<select name="rateInvoiceInterestPeriod" class="select-menu">
										<option value="">Interest Payable Time Period</option> 
										<option value="1">Per Day</option>
										<option value="7">Per Week</option>
										<option value="30">Per Month</option>
										<option value="365">Per Year</option>
									</select>
								</label>
                            */ ?>
                            <label class="ratePaymentDate" style="margin-bottom: 5px;">
                                <a class="hastip" title="IMPORTANT: if your invoice has not been paid yet, leave this blank.<br />But to ensure accurate results, please return to this review and complete this date once you have received payment.<br />Your review will be excluded from the aggregated results if you do not complete this review within 90 days.<br /><br />Date Format: DD-MM-YYYY"><input type="text" name="ratePaymentDate" placeholder="Actual date you received payment from your customer..." onblur="showCertifyTxt(this.value)" data-constraints="@JustDate" value="" /></a>
                                <span class="error-message">*Date format as dd-mm-yyyy.</span>
                            </label>
                            <br />
                            <hr style="width: 90%;" />
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 12px;">
                     	<div class="grid_8">
                                <label class="rateAgree" style="color: #fff; height: auto; line-height: 1.2">
                                    <input type="checkbox" name="rateAgree" id="rateAgree" value="1" data-constraints="@Required" /> <span id="certifytxt">I certify that the information provided above is correct, and I will update it once payment has been received. I confirm that the purpose of my review is purely to share my genuine experiences with the customer's payment processes in good faith. I have not been offered any incentive or payment by the customer to write this review or to alter a previous review.</span>*
                    <span class="empty-message" style="top: auto; bottom: -30px;">*This field is required.</span>
                                </label>
                         </div>
                    </div>

                    <div class="contact-form-buttons">
                        <a href="#" data-type="submit" class="btn-default">Submit</a>
                    </div>
                </fieldset>
                <div class="modal fade response-message">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Register</h4>
                            </div>
                            <div class="modal-body">
                                Sorry, your registration was unsuccessful. Please try again. If you are still unsuccessful, please contact us.
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            
            <form id="registerbuyer-form" class="contact-form reg" style="display: none;" enctype="multipart/form-data">
                <div class="contact-form-loader"></div>
                <div class="header">
                    <h3>Register</h3>
                </div>
                <fieldset>
                    <div class="row">
                        <div class="grid_8">
                            <p style="padding-top: 0px;">Welcome to CreditMark</p>
                            <p>
                            <p>The following details will be used to verify you as an employee of the company so that you can claim your company's CreditMark profile page. Please ensure that they are correct.</p>
                            <p>Your Account Details</p>
                            
                            <label class="buyerUserName">
                                <input type="text" name="buyerUserName" placeholder="Your Name*" data-constraints="@Required" value="" />
                                <span class="empty-message">*This field is required.</span>
                            </label>
                            <label class="buyerCompany">
                                <input type="text" name="buyerCompany" placeholder="Your Company Name*" data-constraints="@Required" value="" />
                                <span class="empty-message">*This field is required.</span>
                            </label>
                            <label class="buyerCompanyUEN">
                                <input type="text" name="buyerCompanyUEN" placeholder="Your Company Registration Number*" data-constraints="@Required" value="" />
                                <span class="empty-message">*This field is required.</span>
                            </label>
                            <label class="buyerCompanyWebsite">
                                <a class="hastip" title="Please include http://"><input type="text" name="buyerCompanyWebsite" placeholder="Your Company Website" data-constraints="@URL" value="" /></a>
                                <span class="error-message">*Sorry, the URL is in an incorrect format. It should look like this: http://www.company.com or http://prefix.company.com.</span>
                            </label>
                            <label class="buyerJob">
                                <input type="text" name="buyerJob" placeholder="Your Job Title*" data-constraints="@Required" value="" />
                                <span class="empty-message">*This field is required.</span>
                            </label>
                            <label class="buyerEmail">
                                <input type="text" name="buyerEmail" placeholder="Your Company E-mail*" data-constraints="@Required @Email" value="" />
                                <span class="empty-message">*This field is required.</span>
                                <span class="error-message">*This doesn't look right. Please check that this is a valid email.</span>
                            </label>
                            <label class="buyerPhone">
                                <a class="hastip" title="Enter numbers only - omit +-() symbols."><input type="tel" name="buyerPhone" placeholder="Your Company Phone Number*" value=""
                                       data-constraints="@Required @JustNumbers" /></a>
                                <span class="empty-message">*This field is required.</span>
                                <span class="error-message">*Sorry, this doesn't match the format we require. Please only enter numbers.</span>
                            </label>
                            <label class="buyerPassword">
                                <input type="password" name="buyerPassword" placeholder="Password*" value="" data-constraints="@Required" />
                                <span class="empty-message">*This field is required.</span>
                            </label>
                            
                            <label class="buyerPassword2">
                                <input type="password" name="buyerPassword2" placeholder="Confirm Password*" value="" data-constraints="@Required @matchPassword" data-contraints-match="buyerPassword" />
                                <span class="empty-message">*This field is required.</span>
                                <span class="error-message">*Passwords do not match.</span>
                            </label>
                            
                            <a class="hastip" title="<strong>Preferred Dimension Ratio:</strong><br />120px (Width) by 150px (Height)<br><br><strong>Preferred File Type:</strong><br>.jpg, .png, .gif"><span class="btn btn-default btn-file">
                                Upload your company logo <input type="file" name="buyerCompanyLogo" placeholder="">
                            </span></a>
                        </div>
                    </div>
                    <div class="contact-form-buttons">
                        <a href="#" data-type="submit" class="btn-default">Register</a>
                        <a href="#" onclick="showRegForm('');" class="btn-default">Cancel</a>
                    </div>
                </fieldset>
                <div class="modal fade response-message">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Register</h4>
                            </div>
                            <div class="modal-body">
                                Sorry, your registration was unsuccessful. Please try again. If you are still unsuccessful, please contact us.
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-27668603-3', 'auto');
  ga('send', 'pageview');

</script>
<?php include_once('footer.php'); ?>