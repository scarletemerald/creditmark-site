<?php include("header.php"); ?>
<?php if ($login && $_SESSION['AP_ut'] == 1) {
$reviews = getCustomerReviews("", $_SESSION['AP_uid']);
?>
<div class="container">
    <div class="row">
        <div class="grid_12">
          <div class="header2">
          	<div style="float: right"><button type="button" id="writereviewbtn" onClick="openModal('write_review.php');" class="btn btn-medium push-right">Write Review</button>
            	<button class="btn btn-medium push-right" onClick="openModal('incident_import.php');">import cases</button></div>
            <h2><strong>Manage</strong> Review(s)</h2>
          </div>
          <table class="table data">
        	<thead>
            	<tr>
                    <td>Customer</td>
                    <td>Invoice Date</td>
                    <td>Invoice Due</td>
                    <td>Invoice Paid</td>
                    <td>Star Rating</td>
                    <td>&nbsp;</td>
            </thead>
            <tbody>
            <?php 
			if (sizeof($reviews) > 0) {
			foreach ($reviews as $review) { 
				$user = getUserAccount($review['user_id']);
				$cust = getCustomer($review['cust_id'], -1);
				$incident = getIncident($review['incident_id'], $review['user_id']);
				$incident = array_pop($incident);
				$cust = array_pop($cust);
			?>
            	<tr>
                    <td><a href="#" onclick="openModal('company_page.php?cid=<?=base64_encode($cust['cust_id']);?>');"><?=$cust['company_name'];?></a></td>
                    <td><?=($incident['invoice_date'] != "0000-00-00" && $incident['invoice_date'] != "")?date("d M Y", strtotime($incident['invoice_date'])):"";?></td>
                    <td><?=($incident['invoice_due'] != "0000-00-00" && $incident['invoice_due'] != "")?date("d M Y", strtotime($incident['invoice_due'])):"";?></td>
                    <td><?=($incident['payment_received'] != "0000-00-00" && $incident['payment_received'] != "")?date("d M Y", strtotime($incident['payment_received'])):"";?></td>
                    <td><?=rating(($review['rating']>0)?$review['rating']:0);?></td>
                    <td><a href="#" onclick="openModal('write_review.php?cid=<?=base64_encode($cust['cust_id']);?>&rid=<?=base64_encode($review['rate_id']);?>');"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x" title="edit"></i><i class="fa fa-pencil fa-stack-1x fa-inverse" title="edit"></i></span></a></td>
                </tr>
            <?php }
			} else { ?>
            	<tr>
                	<td colspan="5">You have not provided a review yet. Click "Write a review" to add one now.</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="modal fade response-message">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title">Customer</h4>
                    </div>
                    <div class="modal-body">
                      <iframe id="modalcontent" src="" width="100%" height="500px"></iframe>
                    </div>
                  </div>
                </div>
              </div>
      </div>
    </div>
</div>
<script>
function updateClaim(id, stats) {
	var s = (stats==1)?"APPROVE":"REJECT";
	if (confirm("Are you sure you wish to "+s+" this claim?")) {
		$.post('bat/claimDB.php', { 'id': id, 'stats': stats }, function(d) {
			alert(d);
			window.location.reload();
		});
	}
}
<?php if ($_GET['add'] == 1) { ?>
$(document).ready(function() {
	openModal('write_review.php<?php if ($_GET['cid'] != "") echo '?cid='.$_GET['cid'];?>');
});
<?php } ?>
</script>

<?php include_once("footer.php"); ?>
<?php
}
else {
	header("Location: index.php");
}
?>