			<div class="row">
                <div class="grid_12">
                	<h2><strong>Most</strong> Requested Companies</h2>
                	<?php
					$coys = mostPopular(10);
					?>
                	<table>
                        <colgroup>
                            <col />
                            <col width="150" />
                            <col width="200" />
                        </colgroup>
                        <thead>
                        <tr>
                            <td>Company</td>
                            <td align="center">Rating</td>
                            <td>&nbsp;</td>
                        </tr>
                        </thead>
                        <?php 
						foreach($coys as $c) {
							$c = getCustomer($c, -1);
							$c = array_pop($c);
							
							if (!$login) { 
								$writeReviewAction = "<button type=\"button\" onClick=\"alert('You have to login as a Supplier to write a review.'); window.top.location='login.php';\" class=\"btn-smallest\">Write Review</button>";
								$claimCompanyAction = "<button type=\"button\" onClick=\"alert('You have to login as a Buyer to claim a company page.'); window.top.location='login.php';\" class=\"btn-smallest\">Claim Company</button>";
							}
							else if ($_SESSION['AP_ut'] == 1) { // Supplier
								$writeReviewAction = "<button type=\"button\" onClick=\"openModal('write_review.php?cid=".base64_encode($c['cust_id'])."');\" class=\"btn-smallest\">Write Review</button>";
								$claimCompanyAction = "";
							}
							else if ($_SESSION['AP_ut'] == 2) { // Buyer
								$writeReviewAction = "";
								$claimCompanyAction = "<button type=\"button\" onClick=\"claimPage('".base64_encode($c['cust_id'])."');\" class=\"btn-smallest\">Claim Company</button>";
							}
							if ($c['user_id'] > 0) {
								$claimCompanyAction = '<span class="claimed">*Claimed</span>';
							}
						?> 
                        <tr>
                            <td><a href="javascript:void(0);" onClick="<?php if ($login) { ?>openModal('company_page.php?cid=<?=base64_encode($c['cust_id']);?>');<?php } else { ?>alert('Please login to view company page.');<?php } ?>"><?=$c['company_name'];?></a></td>
                            <td align="center"><?=rating(calcRating($c['cust_id'])); ?></td>
                            <td><?=$writeReviewAction;?> <?=$claimCompanyAction;?></td>
                        </tr>
                        <?php } ?>
                    </table>
                  <br />
                  </div>
                </div>