<?php
include_once('function.php');
$data = base64_decode($_REQUEST['params']);
$data = unserialize($data);

$error = "";
$incident = getIncident($data['iid'], $data['uid'], "", $data['cid']);
$incident = array_pop($incident);
$user = getUserAccount($incident['user_id']);

if (is_array($incident) && $incident['incident_id'] == $data['iid'] && $incident['user_id'] == $data['uid'] && $incident['cust_id'] == $data['cid']) {
	$company = $incident['customer'];
	$contact = array_pop($company['contact']);
	
	$link = dbConnect();
	$time = date("Y-m-d H:i:s");
	$sSQL = "UPDATE `customer_incident` SET 
		`not_tally` = '1', 
		`status` = '2', 
		`last_modified_time` = '".$time."'
		WHERE `cust_id` = '".mysqli_real_escape_string($link, $data['cid'])."' AND 
		 `user_id` = '".mysqli_real_escape_string($link, $data['uid'])."' AND 
		`incident_id` = '".mysqli_real_escape_string($link, $incident['incident_id'])."'";
	mysqli_query($link, $sSQL);
	
	$subject = "[CreditMark.org] Case Not Tally!";
	$body = '<p style="font-family: Arial, sans-serif; font-size: 12px; color: #000;">Dear <strong>'.$user['user_name'].'</strong>,<br />
	<br />
	Your Customer <strong>'.$contact['contact_name'].' from '.$company['company_name'].'</strong> says your invoice <strong>'.$incident['invoice_no'].'</strong> does not tally with their records. Please contact your client.<br />
	<br />
	Thank you and best wishes<br />
	CreditMark.org</p>';
	
	if (sendEmail($user['contact_email'], $subject, $body)) {
		$msg = "An email has been sent to notify the company. They will contact you accordingly.";
	}
	else $msg = "Failed to notify the company. Please try again later.";
} else $msg = "This Invoice does not exist.";
?>
<?php include("header.php"); ?>
	<div class="container">
        <div class="row">
            <div class="grid_12">
              <div class="header2">
                <h2><strong><?=$user['user_company'];?></strong></h2>
              </div>
              <p><?=$msg;?></p>
         	</div>
          </div>
       </div>
<?php include("footer.php"); ?>