<?php 
require_once('function.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>CreditMark.org - Discover the payment track record of your prospects and customers</title>
    <meta charset="utf-8">
    <meta name = "format-detection" content = "telephone=no" />
    <link rel="icon" href="images/favicon16x16.png" type="image/x-icon">
    <link rel="stylesheet" href="css/font-awesome-4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/contact-form.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script>
    <script src="js/TMForm.js"></script>
    <script src="js/modal.js"></script>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
    <![endif]-->
    
    <link rel="shortcut icon" href="images/favicon16x16.png">
</head>
<body>
<script>
/**
 * detect IE
 * returns version of IE or false, if browser is not Internet Explorer
 */
function detectIE() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
       // IE 12 => return version number
       return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}
var iever = detectIE();
if (iever && iever > 0)  {
	$('body').addClass('ie');
}
</script>

<!--========================================================
                          HEADER
=========================================================-->
<header id="header" class="<?=$headerclass;?>">
    <div id="stuck_container">
        <div class="container">
            <div class="row">
                <div class="grid_4">
                    <h1><a href="index.php"><img src="images/CreditMark - logo.png" /></a></h1>
                </div>
                <div class="grid_8">
                    <nav>
                        <ul class="sf-menu">
                            <li<?php if (strpos($_SERVER['PHP_SELF'], 'index.php') !== false) echo ' class="current"'; ?>><a href="index.php">Home</a></li>
                            <?php if ($_SESSION['AP_login'] == true && $_SESSION['AP_uid'] != "") { ?>
                            <?php if ($_SESSION['AP_ut'] == 9999) { ?>
                            <li<?php if (strpos($_SERVER['PHP_SELF'], 'admin_account.php') !== false) echo ' class="current"'; ?>><a href="admin_account.php">My Account</a>
                            <?php } else { ?>
                            <li<?php if (strpos($_SERVER['PHP_SELF'], 'account.php') !== false) echo ' class="current"'; ?>><a href="account.php">My Account</a>
                            <?php } ?>
                            	<ul>
                                	<?php if ($_SESSION['AP_ut'] == 9999) { ?>
                                    <li<?php if (strpos($_SERVER['PHP_SELF'], 'user_manage.php') !== false) echo ' class="current"'; ?>><a href="user_manage.php">Users List</a></li>
                                    <li<?php if (strpos($_SERVER['PHP_SELF'], 'buyer_manage.php') !== false) echo ' class="current"'; ?>><a href="buyer_manage.php">Manage Pending Buyers</a></li>
                                    <li<?php if (strpos($_SERVER['PHP_SELF'], 'customer_manage_claims.php') !== false) echo ' class="current"'; ?>><a href="customer_manage_claims.php">Manage Buyer Claims</a></li>
                                    <?php } ?>
                            		<?php if ($_SESSION['AP_ut'] == 1) { ?>
                                    <li<?php if (strpos($_SERVER['PHP_SELF'], 'customer_manage_reviews.php') !== false) echo ' class="current"'; ?>><a href="customer_manage_reviews.php">Manage Reviews</a></li>
                                    <?php /*
                                    <li<?php if (strpos($_SERVER['PHP_SELF'], 'customer_manage.php') !== false) echo ' class="current"'; ?>><a href="customer_manage.php">Manage Customers</a></li> */ ?>
                                	<?php } ?>
                                    <li<?php if (strpos($_SERVER['PHP_SELF'], 'search_company_main.php') !== false) echo ' class="current"'; ?>><a href="search_company_main.php">Search Company</a></li>
                                    <li<?php if (strpos($_SERVER['PHP_SELF'], 'resources.php') !== false) echo ' class="current"'; ?>><a href="resources.php">Resources</a></li>
                                    
                            		<?php if ($_SESSION['AP_ut'] == 1) { ?>
                                    <?php /* <li<?php if (strpos($_SERVER['PHP_SELF'], 'incident_manage.php') !== false) echo ' class="current"'; ?>><a href="incident_manage.php">Manage Emails</a></li> */ ?>
                                	<?php } ?>
                                    <li<?php if (strpos($_SERVER['PHP_SELF'], 'logout.php') !== false) echo ' class="current"'; ?>><a href="logout.php">Logout</a></li>
                                </ul>
                            </li>
                            <?php } else { ?>
                            <li<?php if (strpos($_SERVER['PHP_SELF'], 'login.php') !== false) echo ' class="current"'; ?>><a href="login.php">Register / Login</a></li>
                            <?php } ?>
                            <li<?php if (strpos($_SERVER['PHP_SELF'], 'faq.php') !== false) echo ' class="current"'; ?>><a href="faq.php">FAQ</a></li>
                            <li<?php if (strpos($_SERVER['PHP_SELF'], 'contact_us.php') !== false) echo ' class="current"'; ?>><a href="contact_us.php">Contact Us</a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>

<!--========================================================
                          CONTENT
=========================================================-->
<section id="content"<?=$maincontentclass;?>>