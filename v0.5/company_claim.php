<?php
require_once('function.php');

if ($login && $_POST['cid'] != "" && $_SESSION['AP_ut'] == 2 && $_SESSION['AP_uid'] > 0) {
	$cid = base64_decode($_POST['cid']);
	$link = dbConnect();
	$sSQL = "SELECT * FROM customer_claim WHERE cust_id = '".mysqli_real_escape_string($link, $cid)."' AND user_id = '".$_SESSION['AP_uid']."'";
	$aRs = mysqli_query($link, $sSQL);
	if (mysqli_num_rows($aRs) > 0) {
		echo 'You have claimed the profile before. Please wait for our approval notification.';
	}
	else {
		$sSQL = "INSERT INTO customer_claim (cust_id, user_id, created_time) VALUES ('".mysqli_real_escape_string($link, $cid)."', '".$_SESSION['AP_uid']."', '".date("Y-m-d H:i:s")."')";
		if (mysqli_query($link, $sSQL)) {
			echo 'You have claimed this profile. We will notify you once this has been approved.';
		}
		else echo 'An unknown error has occurred. Please try again later.';
	}
	dbClose();
}
else echo 'You are not logged in as a Buyer. Please logout and re-login as a Buyer to claim the page.';
?>