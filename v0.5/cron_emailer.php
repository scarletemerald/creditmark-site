<?php
include_once('function.php');

$link = dbConnect();
$time = time();
$time = mktime(12,0,0,2,17,2015);

// 60 Days AFTER Due
echo $sSQL = "SELECT * FROM customer_incident WHERE status = 1 AND invoice_due = '".date("Y-m-d", $time - (60*24*60*60))."'";
$aRs = mysqli_query($link, $sSQL);
fclose($fp);
while ($incident = mysqli_fetch_assoc($aRs)) {
	$user = getUserAccount($incident['user_id']);
	$company = getCustomer($incident['cust_id'], $incident['user_id'], "", $incident['contact_id']);
	$company = array_pop($company);
	$contact = array_pop($company['contact']);
	
	$template = getEmailTemplate($incident['incident_id'], 1); /// Emailer 1
	
	$body1 = $template[1];
	$body1 = str_replace("[CustomerName1]", $contact['contact_name'], $body1);
	$body1 = str_replace("[UserCompany]", $user['user_company'], $body1);
	$body1 = str_replace("[InvoiceNumber]", $incident['invoice_no'], $body1);
	$body1 = str_replace("[InvoiceCurrency]", $incident['invoice_currency'], $body1);
	$body1 = str_replace("[InvoiceAmount]", number_format($incident['invoice_amount'], 2, ".", ","), $body1);
	$body1 = str_replace("[InvoiceDate]", date("d M Y", strtotime($incident['invoice_due'])), $body1);
	$body1 = str_replace("[InterestRate]", $incident['invoice_interest']."%", $body1);
	$body1 = str_replace("[InterestTime]", $incident['invoice_interest_period']." day(s)", $body1);
	if ($incident['invoice_interest_period'] <= 30) {
		$interest = $incident['invoice_amount']*($incident['invoice_interest']/100)*30/$incident['invoice_interest_period'];
	}
	else $interest = 0;
	$body1 = str_replace("[InvoiceInterestNow]", number_format($interest, 2, ".", ","), $body1);
	$body1 = str_replace("[InvoiceAmountNow]", number_format($incident['invoice_amount']+$interest, 2, ".", ","), $body1);
	$body1 = str_replace("[UsersEmailAddress]", $user['user_email'], $body1);
	$body1 = str_replace("[UserTelephone]", $user['user_phone'], $body1);
	$body1 = str_replace("[UserName]", $user['user_name'], $body1);
	
	$body1 .= $template[2];
	
	if ($user['user_paypal'] != "") {
		$body1 = str_replace("[PaypalLink]", '&bull; <a href="https://www.paypal.com/cgi-bin/webscr?business='.$user['user_paypal'].'&cmd=_xclick&currency_code='.$incident['invoice_currency'].'&amount='.number_format($incident['invoice_amount'], 2, ".", "").'&item_name='.$incident['invoice_no'].'" target="_blank">Make payment now by PayPal</a><br />', $body1);
	}
	else $body1 = str_replace("[PaypalLink]", "", $body1);
	$invoiceData = array();
	$invoiceData['uid'] = $incident['user_id'];
	$invoiceData['cid'] = $incident['cust_id'];
	$invoiceData['iid'] = $incident['incident_id'];
	$body1 = str_replace("[Link1]", WL_PATH."invoice_paid.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link2]", WL_PATH."invoice_nottally.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link3]", WL_PATH."invoice_wrongrecipient.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link4]", WL_PATH."login.php", $body1);
	echo $body1;
	echo sendEmail($user['contact_email'], $template[0], $body1);
}
echo '<br /><br />';

// 30 Days AFTER Due
echo $sSQL = "SELECT * FROM customer_incident WHERE status = 1 AND invoice_due = '".date("Y-m-d", $time - (30*24*60*60))."'";
$aRs = mysqli_query($link, $sSQL);
while ($incident = mysqli_fetch_assoc($aRs)) {
	$user = getUserAccount($incident['user_id']);
	$company = getCustomer($incident['cust_id'], $incident['user_id'], "", $incident['contact_id']);
	$company = array_pop($company);
	$contact = array_pop($company['contact']);
	
	$template = getEmailTemplate($incident['incident_id'], 2); /// Emailer 2
	
	$body1 = $template[1];
	$body1 = str_replace("[CustomerName1]", $contact['contact_name'], $body1);
	$body1 = str_replace("[UserCompany]", $user['user_company'], $body1);
	$body1 = str_replace("[InvoiceNumber]", $incident['invoice_no'], $body1);
	$body1 = str_replace("[InvoiceCurrency]", $incident['invoice_currency'], $body1);
	$body1 = str_replace("[InvoiceAmount]", number_format($incident['invoice_amount'], 2, ".", ","), $body1);
	$body1 = str_replace("[InvoiceDate]", date("d M Y", strtotime($incident['invoice_due'])), $body1);
	$body1 = str_replace("[InterestRate]", $incident['invoice_interest']."%", $body1);
	$body1 = str_replace("[InterestTime]", $incident['invoice_interest_period']." day(s)", $body1);
	if ($incident['invoice_interest_period'] <= 30) {
		$interest = $incident['invoice_amount']*($incident['invoice_interest']/100)*30/$incident['invoice_interest_period'];
	}
	else $interest = 0;
	$body1 = str_replace("[InvoiceInterestNow]", number_format($interest, 2, ".", ","), $body1);
	$body1 = str_replace("[InvoiceAmountNow]", number_format($incident['invoice_amount']+$interest, 2, ".", ","), $body1);
	$body1 = str_replace("[UsersEmailAddress]", $user['user_email'], $body1);
	$body1 = str_replace("[UserTelephone]", $user['user_phone'], $body1);
	$body1 = str_replace("[UserName]", $user['user_name'], $body1);
	
	$body1 .= $template[2];
	
	if ($user['user_paypal'] != "") {
		$body1 = str_replace("[PaypalLink]", '&bull; <a href="https://www.paypal.com/cgi-bin/webscr?business='.$user['user_paypal'].'&cmd=_xclick&currency_code='.$incident['invoice_currency'].'&amount='.number_format($incident['invoice_amount'], 2, ".", "").'&item_name='.$incident['invoice_no'].'" target="_blank">Make payment now by PayPal</a><br />', $body1);
	}
	else $body1 = str_replace("[PaypalLink]", "", $body1);
	$invoiceData = array();
	$invoiceData['uid'] = $incident['user_id'];
	$invoiceData['cid'] = $incident['cust_id'];
	$invoiceData['iid'] = $incident['incident_id'];
	$body1 = str_replace("[Link1]", WL_PATH."invoice_paid.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link2]", WL_PATH."invoice_nottally.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link3]", WL_PATH."invoice_wrongrecipient.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link4]", WL_PATH."login.php", $body1);
	echo $body1;
	echo sendEmail($user['contact_email'], $template[0], $body1);
}
echo '<br /><br />';

// 21 Days AFTER Due
echo $sSQL = "SELECT * FROM customer_incident WHERE status = 1 AND invoice_due = '".date("Y-m-d", $time - (21*24*60*60))."'";
$aRs = mysqli_query($link, $sSQL);
while ($incident = mysqli_fetch_assoc($aRs)) {
	$user = getUserAccount($incident['user_id']);
	$company = getCustomer($incident['cust_id'], $incident['user_id'], "", $incident['contact_id']);
	$company = array_pop($company);
	$contact = array_pop($company['contact']);
	
	$template = getEmailTemplate($incident['incident_id'], 3); /// Emailer 3
	
	$body1 = $template[1];
	$body1 = str_replace("[CustomerName1]", $contact['contact_name'], $body1);
	$body1 = str_replace("[UserCompany]", $user['user_company'], $body1);
	$body1 = str_replace("[InvoiceNumber]", $incident['invoice_no'], $body1);
	$body1 = str_replace("[InvoiceCurrency]", $incident['invoice_currency'], $body1);
	$body1 = str_replace("[InvoiceAmount]", number_format($incident['invoice_amount'], 2, ".", ","), $body1);
	$body1 = str_replace("[InvoiceDate]", date("d M Y", strtotime($incident['invoice_due'])), $body1);
	$body1 = str_replace("[UsersEmailAddress]", $user['user_email'], $body1);
	$body1 = str_replace("[UserTelephone]", $user['user_phone'], $body1);
	$body1 = str_replace("[UserName]", $user['user_name'], $body1);
	
	$body1 = $template[2];
	
	if ($user['user_paypal'] != "") {
		$body1 = str_replace("[PaypalLink]", '&bull; <a href="https://www.paypal.com/cgi-bin/webscr?business='.$user['user_paypal'].'&cmd=_xclick&currency_code='.$incident['invoice_currency'].'&amount='.number_format($incident['invoice_amount'], 2, ".", "").'&item_name='.$incident['invoice_no'].'" target="_blank">Make payment now by PayPal</a><br />', $body1);
	}
	else $body1 = str_replace("[PaypalLink]", "", $body1);
	$invoiceData = array();
	$invoiceData['uid'] = $incident['user_id'];
	$invoiceData['cid'] = $incident['cust_id'];
	$invoiceData['iid'] = $incident['incident_id'];
	$body1 = str_replace("[Link1]", WL_PATH."invoice_paid.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link2]", WL_PATH."invoice_nottally.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link3]", WL_PATH."invoice_wrongrecipient.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link4]", WL_PATH."login.php", $body1);
	echo $body1;
	echo sendEmail($user['contact_email'], $template[0], $body1);
}
echo '<br /><br />';

// 14 Days AFTER Due
echo $sSQL = "SELECT * FROM customer_incident WHERE status = 1 AND invoice_due = '".date("Y-m-d", $time - (14*24*60*60))."'";
$aRs = mysqli_query($link, $sSQL);
while ($incident = mysqli_fetch_assoc($aRs)) {
	$user = getUserAccount($incident['user_id']);
	$company = getCustomer($incident['cust_id'], $incident['user_id'], "", $incident['contact_id']);
	$company = array_pop($company);
	$contact = array_pop($company['contact']);
	
	$template = getEmailTemplate($incident['incident_id'], 4); /// Emailer 4
	
	$body1 = $template[1];
	$body1 = str_replace("[CustomerName1]", $contact['contact_name'], $body1);
	$body1 = str_replace("[UserCompany]", $user['user_company'], $body1);
	$body1 = str_replace("[InvoiceNumber]", $incident['invoice_no'], $body1);
	$body1 = str_replace("[InvoiceCurrency]", $incident['invoice_currency'], $body1);
	$body1 = str_replace("[InvoiceAmount]", number_format($incident['invoice_amount'], 2, ".", ","), $body1);
	$body1 = str_replace("[InvoiceDate]", date("d M Y", strtotime($incident['invoice_due'])), $body1);
	$body1 = str_replace("[UsersEmailAddress]", $user['user_email'], $body1);
	$body1 = str_replace("[UserTelephone]", $user['user_phone'], $body1);
	$body1 = str_replace("[UserName]", $user['user_name'], $body1);
	
	$body = $template[2];
	
	if ($user['user_paypal'] != "") {
		$body1 = str_replace("[PaypalLink]", '&bull; <a href="https://www.paypal.com/cgi-bin/webscr?business='.$user['user_paypal'].'&cmd=_xclick&currency_code='.$incident['invoice_currency'].'&amount='.number_format($incident['invoice_amount'], 2, ".", "").'&item_name='.$incident['invoice_no'].'" target="_blank">Make payment now by PayPal</a><br />', $body1);
	}
	else $body1 = str_replace("[PaypalLink]", "", $body1);
	$invoiceData = array();
	$invoiceData['uid'] = $incident['user_id'];
	$invoiceData['cid'] = $incident['cust_id'];
	$invoiceData['iid'] = $incident['incident_id'];
	$body1 = str_replace("[Link1]", WL_PATH."invoice_paid.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link2]", WL_PATH."invoice_nottally.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link3]", WL_PATH."invoice_wrongrecipient.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link4]", WL_PATH."login.php", $body1);
	echo $body1;
	echo sendEmail($user['contact_email'], $template[0], $body1);
}
echo '<br /><br />';

// 7 Days AFTER Due
echo $sSQL = "SELECT * FROM customer_incident WHERE status = 1 AND invoice_due = '".date("Y-m-d", $time - (7*24*60*60))."'";
$aRs = mysqli_query($link, $sSQL);
while ($incident = mysqli_fetch_assoc($aRs)) {
	$user = getUserAccount($incident['user_id']);
	$company = getCustomer($incident['cust_id'], $incident['user_id'], "", $incident['contact_id']);
	$company = array_pop($company);
	$contact = array_pop($company['contact']);
	
	$template = getEmailTemplate($incident['incident_id'], 5); /// Emailer 5
	
	$body1 = $template[1];
	$body1 = str_replace("[CustomerName1]", $contact['contact_name'], $body1);
	$body1 = str_replace("[UserCompany]", $user['user_company'], $body1);
	$body1 = str_replace("[InvoiceNumber]", $incident['invoice_no'], $body1);
	$body1 = str_replace("[InvoiceCurrency]", $incident['invoice_currency'], $body1);
	$body1 = str_replace("[InvoiceAmount]", number_format($incident['invoice_amount'], 2, ".", ","), $body1);
	$body1 = str_replace("[InvoiceDate]", date("d M Y", strtotime($incident['invoice_due'])), $body1);
	$body1 = str_replace("[UsersEmailAddress]", $user['user_email'], $body1);
	$body1 = str_replace("[UserTelephone]", $user['user_phone'], $body1);
	$body1 = str_replace("[UserName]", $user['user_name'], $body1);
	
	$body1 .= $template[2];
	
	if ($user['user_paypal'] != "") {
		$body1 = str_replace("[PaypalLink]", '&bull; <a href="https://www.paypal.com/cgi-bin/webscr?business='.$user['user_paypal'].'&cmd=_xclick&currency_code='.$incident['invoice_currency'].'&amount='.number_format($incident['invoice_amount'], 2, ".", "").'&item_name='.$incident['invoice_no'].'" target="_blank">Make payment now by PayPal</a><br />', $body1);
	}
	else $body1 = str_replace("[PaypalLink]", "", $body1);
	$invoiceData = array();
	$invoiceData['uid'] = $incident['user_id'];
	$invoiceData['cid'] = $incident['cust_id'];
	$invoiceData['iid'] = $incident['incident_id'];
	$body1 = str_replace("[Link1]", WL_PATH."invoice_paid.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link2]", WL_PATH."invoice_nottally.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link3]", WL_PATH."invoice_wrongrecipient.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link4]", WL_PATH."login.php", $body1);
	echo $body1;
	echo sendEmail($user['contact_email'], $template[0], $body1);
}
echo '<br /><br />';

// 3 Days AFTER Due
echo $sSQL = "SELECT * FROM customer_incident WHERE status = 1 AND invoice_due = '".date("Y-m-d", $time - (3*24*60*60))."'";
$aRs = mysqli_query($link, $sSQL);
while ($incident = mysqli_fetch_assoc($aRs)) {
	$user = getUserAccount($incident['user_id']);
	$company = getCustomer($incident['cust_id'], $incident['user_id'], "", $incident['contact_id']);
	$company = array_pop($company);
	$contact = array_pop($company['contact']);
	
	$template = getEmailTemplate($incident['incident_id'], 6); /// Emailer 6
	
	$body1 = $template[1];
	$body1 = str_replace("[CustomerName1]", $contact['contact_name'], $body1);
	$body1 = str_replace("[UserCompany]", $user['user_company'], $body1);
	$body1 = str_replace("[InvoiceNumber]", $incident['invoice_no'], $body1);
	$body1 = str_replace("[InvoiceCurrency]", $incident['invoice_currency'], $body1);
	$body1 = str_replace("[InvoiceAmount]", number_format($incident['invoice_amount'], 2, ".", ","), $body1);
	$body1 = str_replace("[InvoiceDate]", date("d M Y", strtotime($incident['invoice_due'])), $body1);
	$body1 = str_replace("[UsersEmailAddress]", $user['user_email'], $body1);
	$body1 = str_replace("[UserTelephone]", $user['user_phone'], $body1);
	$body1 = str_replace("[UserName]", $user['user_name'], $body1);
	
	$body1 .= $template[2];
	
	if ($user['user_paypal'] != "") {
		$body1 = str_replace("[PaypalLink]", '&bull; <a href="https://www.paypal.com/cgi-bin/webscr?business='.$user['user_paypal'].'&cmd=_xclick&currency_code='.$incident['invoice_currency'].'&amount='.number_format($incident['invoice_amount'], 2, ".", "").'&item_name='.$incident['invoice_no'].'" target="_blank">Make payment now by PayPal</a><br />', $body1);
	}
	else $body1 = str_replace("[PaypalLink]", "", $body1);
	$invoiceData = array();
	$invoiceData['uid'] = $incident['user_id'];
	$invoiceData['cid'] = $incident['cust_id'];
	$invoiceData['iid'] = $incident['incident_id'];
	$body1 = str_replace("[Link1]", WL_PATH."invoice_paid.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link2]", WL_PATH."invoice_nottally.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link3]", WL_PATH."invoice_wrongrecipient.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link4]", WL_PATH."login.php", $body1);
	echo $body1;
	echo sendEmail($user['contact_email'], $template[0], $body1);
}
echo '<br /><br />';

// Due Date
echo $sSQL = "SELECT * FROM customer_incident WHERE status = 1 AND invoice_due = '".date("Y-m-d", $time)."'";
$aRs = mysqli_query($link, $sSQL);
while ($incident = mysqli_fetch_assoc($aRs)) {
	$user = getUserAccount($incident['user_id']);
	$company = getCustomer($incident['cust_id'], $incident['user_id'], "", $incident['contact_id']);
	$company = array_pop($company);
	$contact = array_pop($company['contact']);
	
	$template = getEmailTemplate($incident['incident_id'], 7); /// Emailer 7
	
	$body1 = $template[1];
	$body1 = str_replace("[CustomerName1]", $contact['contact_name'], $body1);
	$body1 = str_replace("[UserCompany]", $user['user_company'], $body1);
	$body1 = str_replace("[InvoiceNumber]", $incident['invoice_no'], $body1);
	$body1 = str_replace("[InvoiceCurrency]", $incident['invoice_currency'], $body1);
	$body1 = str_replace("[InvoiceAmount]", number_format($incident['invoice_amount'], 2, ".", ","), $body1);
	$body1 = str_replace("[InvoiceDate]", date("d M Y", strtotime($incident['invoice_due'])), $body1);
	$body1 = str_replace("[UsersEmailAddress]", $user['user_email'], $body1);
	$body1 = str_replace("[UserTelephone]", $user['user_phone'], $body1);
	$body1 = str_replace("[UserName]", $user['user_name'], $body1);
	
	$body .= $template[2];
	
	if ($user['user_paypal'] != "") {
		$body1 = str_replace("[PaypalLink]", '&bull; <a href="https://www.paypal.com/cgi-bin/webscr?business='.$user['user_paypal'].'&cmd=_xclick&currency_code='.$incident['invoice_currency'].'&amount='.number_format($incident['invoice_amount'], 2, ".", "").'&item_name='.$incident['invoice_no'].'" target="_blank">Make payment now by PayPal</a><br />', $body1);
	}
	else $body1 = str_replace("[PaypalLink]", "", $body1);
	$invoiceData = array();
	$invoiceData['uid'] = $incident['user_id'];
	$invoiceData['cid'] = $incident['cust_id'];
	$invoiceData['iid'] = $incident['incident_id'];
	$body1 = str_replace("[Link1]", WL_PATH."invoice_paid.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link2]", WL_PATH."invoice_nottally.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link3]", WL_PATH."invoice_wrongrecipient.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link4]", WL_PATH."login.php", $body1);
	echo $body1;
	echo sendEmail($user['contact_email'], $template[0], $body1);
}
echo '<br /><br />';

// 3 Days BEFORE Due
echo $sSQL = "SELECT * FROM customer_incident WHERE status = 1 AND invoice_due = '".date("Y-m-d", $time + (3*24*60*60))."'";
$aRs = mysqli_query($link, $sSQL);
while ($incident = mysqli_fetch_assoc($aRs)) {
	$user = getUserAccount($incident['user_id']);
	$company = getCustomer($incident['cust_id'], $incident['user_id'], "", $incident['contact_id']);
	$company = array_pop($company);
	$contact = array_pop($company['contact']);
	
	$template = getEmailTemplate($incident['incident_id'], 8); /// Emailer 8
	
	$body1 = $template[1];
	$body1 = str_replace("[CustomerName1]", $contact['contact_name'], $body1);
	$body1 = str_replace("[UserCompany]", $user['user_company'], $body1);
	$body1 = str_replace("[InvoiceNumber]", $incident['invoice_no'], $body1);
	$body1 = str_replace("[InvoiceCurrency]", $incident['invoice_currency'], $body1);
	$body1 = str_replace("[InvoiceAmount]", number_format($incident['invoice_amount'], 2, ".", ","), $body1);
	$body1 = str_replace("[InvoiceDate]", date("d M Y", strtotime($incident['invoice_due'])), $body1);
	$body1 = str_replace("[UsersEmailAddress]", $user['user_email'], $body1);
	$body1 = str_replace("[UserTelephone]", $user['user_phone'], $body1);
	$body1 = str_replace("[UserName]", $user['user_name'], $body1);
	
	$body1 .= $template[2];
	
	if ($user['user_paypal'] != "") {
		$body1 = str_replace("[PaypalLink]", '&bull; <a href="https://www.paypal.com/cgi-bin/webscr?business='.$user['user_paypal'].'&cmd=_xclick&currency_code='.$incident['invoice_currency'].'&amount='.number_format($incident['invoice_amount'], 2, ".", "").'&item_name='.$incident['invoice_no'].'" target="_blank">Make payment now by PayPal</a><br />', $body1);
	}
	else $body1 = str_replace("[PaypalLink]", "", $body1);
	$invoiceData = array();
	$invoiceData['uid'] = $incident['user_id'];
	$invoiceData['cid'] = $incident['cust_id'];
	$invoiceData['iid'] = $incident['incident_id'];
	$body1 = str_replace("[Link1]", WL_PATH."invoice_paid.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link2]", WL_PATH."invoice_nottally.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link3]", WL_PATH."invoice_wrongrecipient.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link4]", WL_PATH."login.php", $body1);
	echo $body1;
	echo sendEmail($user['contact_email'], $template[0], $body1);
}
echo '<br /><br />';

// Half Way BEFORE Due (i.e. (Due Date - Invoice Date) / 2 ) and IFF > 3 Days BEFORE DUE)
echo $sSQL = "SELECT *, DATE_ADD(invoice_date, INTERVAL half_days DAY) AS half_date FROM customer_incident t1 LEFT JOIN (SELECT incident_id, (DATEDIFF(invoice_due, invoice_date)/2) AS half_days FROM customer_incident) t2 ON t1.incident_id = t2.incident_id WHERE status = 1 AND invoice_due > '".date("Y-m-d", $time + (3*24*60*60))."' HAVING half_date = '".date("Y-m-d", $time)."'";
$aRs = mysqli_query($link, $sSQL);
while ($incident = mysqli_fetch_assoc($aRs)) {
	$user = getUserAccount($incident['user_id']);
	$company = getCustomer($incident['cust_id'], $incident['user_id'], "", $incident['contact_id']);
	$company = array_pop($company);
	$contact = array_pop($company['contact']);
	
	$template = getEmailTemplate($incident['incident_id'], 9); /// Emailer 9
	
	$body1 = $template[1];
	$body1 = str_replace("[CustomerName1]", $contact['contact_name'], $body1);
	$body1 = str_replace("[UserCompany]", $user['user_company'], $body1);
	$body1 = str_replace("[InvoiceNumber]", $incident['invoice_no'], $body1);
	$body1 = str_replace("[InvoiceCurrency]", $incident['invoice_currency'], $body1);
	$body1 = str_replace("[InvoiceAmount]", number_format($incident['invoice_amount'], 2, ".", ","), $body1);
	$body1 = str_replace("[InvoiceDate]", date("d M Y", strtotime($incident['invoice_due'])), $body1);
	$body1 = str_replace("[UsersEmailAddress]", $user['user_email'], $body1);
	$body1 = str_replace("[UserTelephone]", $user['user_phone'], $body1);
	$body1 = str_replace("[UserName]", $user['user_name'], $body1);
	
	$body1 .= $template[2];
	
	if ($user['user_paypal'] != "") {
		$body1 = str_replace("[PaypalLink]", '&bull; <a href="https://www.paypal.com/cgi-bin/webscr?business='.$user['user_paypal'].'&cmd=_xclick&currency_code='.$incident['invoice_currency'].'&amount='.number_format($incident['invoice_amount'], 2, ".", "").'&item_name='.$incident['invoice_no'].'" target="_blank">Make payment now by PayPal</a><br />', $body1);
	}
	else $body1 = str_replace("[PaypalLink]", "", $body1);
	$invoiceData = array();
	$invoiceData['uid'] = $incident['user_id'];
	$invoiceData['cid'] = $incident['cust_id'];
	$invoiceData['iid'] = $incident['incident_id'];
	$body1 = str_replace("[Link1]", WL_PATH."invoice_paid.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link2]", WL_PATH."invoice_nottally.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link3]", WL_PATH."invoice_wrongrecipient.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link4]", WL_PATH."login.php", $body1);
	echo $body1;
	echo sendEmail($user['contact_email'], $template[0], $body1);
}
echo '<br /><br />';

// Invoice Date (IFF > 4 Days BEFORE DUE and Created Today)
echo $sSQL = "SELECT * FROM customer_incident WHERE status = 1 AND invoice_due > '".date("Y-m-d", $time + (3*24*60*60))."' 
	AND created_time >= '".date("Y-m-d", $time)." 00:00:00' AND created_time <= '".date("Y-m-d", $time)." 23:59:59'";
$aRs = mysqli_query($link, $sSQL);
while ($incident = mysqli_fetch_assoc($aRs)) {
	$user = getUserAccount($incident['user_id']);
	$company = getCustomer($incident['cust_id'], $incident['user_id'], "", $incident['contact_id']);
	$company = array_pop($company);
	$contact = array_pop($company['contact']);
	
	$template = getEmailTemplate($incident['incident_id'], 10); /// Emailer 10
	
	$body1 = $template[1];
	$body1 = str_replace("[CustomerName1]", $contact['contact_name'], $body1);
	$body1 = str_replace("[UserCompany]", $user['user_company'], $body1);
	$body1 = str_replace("[InvoiceNumber]", $incident['invoice_no'], $body1);
	$body1 = str_replace("[InvoiceCurrency]", $incident['invoice_currency'], $body1);
	$body1 = str_replace("[InvoiceAmount]", number_format($incident['invoice_amount'], 2, ".", ","), $body1);
	$body1 = str_replace("[InvoiceDate]", date("d M Y", strtotime($incident['invoice_due'])), $body1);
	$body1 = str_replace("[UsersEmailAddress]", $user['user_email'], $body1);
	$body1 = str_replace("[UserTelephone]", $user['user_phone'], $body1);
	$body1 = str_replace("[UserName]", $user['user_name'], $body1);
	
	$body1 .= $template[2];
	
	if ($user['user_paypal'] != "") {
		$body1 = str_replace("[PaypalLink]", '&bull; <a href="https://www.paypal.com/cgi-bin/webscr?business='.$user['user_paypal'].'&cmd=_xclick&currency_code='.$incident['invoice_currency'].'&amount='.number_format($incident['invoice_amount'], 2, ".", "").'&item_name='.$incident['invoice_no'].'" target="_blank">Make payment now by PayPal</a><br />', $body1);
	}
	else $body1 = str_replace("[PaypalLink]", "", $body1);
	$invoiceData = array();
	$invoiceData['uid'] = $incident['user_id'];
	$invoiceData['cid'] = $incident['cust_id'];
	$invoiceData['iid'] = $incident['incident_id'];
	$body1 = str_replace("[Link1]", WL_PATH."invoice_paid.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link2]", WL_PATH."invoice_nottally.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link3]", WL_PATH."invoice_wrongrecipient.php?params=".base64_encode(serialize($invoiceData)), $body1);
	$body1 = str_replace("[Link4]", WL_PATH."login.php", $body1);
	echo $body1;
	echo sendEmail($user['contact_email'], $template[0], $body1);
}
echo '<br /><br />';
?>