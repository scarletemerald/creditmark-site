<?php include("header.php"); ?>
<?php if ($login && $_SESSION['AP_ut'] == 9999) {
$users = getUsersAccount(); 
?>
<div class="container">
    <div class="row">
        <div class="grid_12">
          <div class="header2">
            <h2><strong>User(s)</strong> List</h2>
          </div>
          <table class="table data">
        	<thead>
            	<tr>
                    <td width="20">No</td>
                    <td>Name</td>
                    <td>Email</td>
                    <td>Phone</td>
                    <td>Type</td>
                </tr>
            </thead>
            <tbody>
            <?php if (sizeof($users) > 0) {
			$x = 1;
			foreach ($users as $user) { 
			?>
            	<tr>
                    <td><?=$x;?></td>
                    <td><?=$user['user_name'];?></td>
                    <td><?=$user['user_email'];?></td>
                    <td><?=$user['user_phone'];?></td>
                    <td><?=($user['user_type']==1)?"Supplier":"Buyer";?></td>
                </tr>
            <?php 
				$x++;
			}
			} else { ?>
            	<tr>
                	<td colspan="4">You don't seem to have a user account yet.</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="modal fade response-message">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title">User</h4>
                    </div>
                    <div class="modal-body">
                      <iframe id="modalcontent" src="" width="100%" height="500px"></iframe>
                    </div>
                  </div>
                </div>
              </div>
      </div>
    </div>
</div>

<?php include_once("footer.php"); ?>
<?php
}
else {
	header("Location: index.php");
}
?>