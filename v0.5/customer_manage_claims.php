<?php include("header.php"); ?>
<?php if ($login && $_SESSION['AP_ut'] == 9999) {
$claims = getCustomerClaims("", "", "", " AND approved = 0"); // Get All Non Approved
?>
<div class="container">
    <div class="row">
        <div class="grid_12">
          <div class="header2">
            <h2><strong>Manage</strong> Buyer Claim(s)</h2>
          </div>
          <table class="table data">
        	<thead>
            	<tr>
                    <td>Company</td>
                    <td width="500">Claimed By</td>
                    <td width="80">Actions</td>
                </tr>
            </thead>
            <tbody>
            <?php if (sizeof($claims) > 0) {
			foreach ($claims as $claim) { 
				$user = getUserAccount($claim['user_id']);
				$cust = getCustomer($claim['cust_id'], -1);
				$cust = array_pop($cust);
			?>
            	<tr>
                    <td><a href="#" onclick="openModal('company_page.php?cid=<?=base64_encode($cust['cust_id']);?>');"><?=$cust['company_name'];?></a></td>
                    <td><strong><?=$user['user_name'];?></strong><br />
                    <?=$user['user_email'];?><br />
                    <?=$user['user_phone'];?><br />
                    <?=$user['user_job_title'];?><br />
                    <?=$user['user_company'];?><br />
                    <?=$user['user_company_url'];?></td>
                    <td><a href="#" onclick="updateClaim('<?=base64_encode($claim['claim_id']);?>', 1);"><i class="fa fa-check-circle-o fa-2x" title="Approve"></i></a>
                    <a href="#" onclick="updateClaim('<?=base64_encode($claim['claim_id']);?>', -1);"><i class="fa fa-times-circle-o fa-2x" title="Reject"></i></a></td>
                </tr>
            <?php }
			} else { ?>
            	<tr>
                	<td colspan="4">You have no claims yet.</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="modal fade response-message">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title">Customer</h4>
                    </div>
                    <div class="modal-body">
                      <iframe id="modalcontent" src="" width="100%" height="500px"></iframe>
                    </div>
                  </div>
                </div>
              </div>
      </div>
    </div>
</div>
<script>
function updateClaim(id, stats) {
	var s = (stats==1)?"APPROVE":"REJECT";
	if (confirm("Are you sure you wish to "+s+" this claim?")) {
		$.post('bat/claimDB.php', { 'id': id, 'stats': stats }, function(d) {
			alert(d);
			window.location.reload();
		});
	}
}
</script>

<?php include_once("footer.php"); ?>
<?php
}
else {
	header("Location: index.php");
}
?>