<?php 
require_once("../function.php");

$error = false;

// Clear session/cookie
unset($_SESSION["AP_login"]);
unset($_SESSION["AP_uid"]);
unset($_SESSION["AP_eml"]);
setcookie('registered_user@creditmark.org', '', time()-3600);
$_SESSION = array();
$_COOKIES = array();

if ($_POST['loginEmail'] != "" && $_POST['loginPassword'] != "") {
	$link = dbConnect();
	
	if ($_POST['usePitchMark'] == "on") {
		$linkPM = dbConnectPM();
		$sSQL = "SELECT * FROM client WHERE 
			email = '".mysqli_real_escape_string($link, $_POST['loginEmail'])."' AND 
			password = '".mysqli_real_escape_string($link, $_POST['loginPassword'])."'";
		$aRs = mysqli_query($linkPM, $sSQL);
		if (mysqli_num_rows($aRs) > 0) { // Pitchmark Login Successful
			$pmdata = mysqli_fetch_assoc($aRs);
			$identifier = $pmdata['client_id'];
			$profileType = "PitchMark";
			$sSQL = "SELECT * FROM user_social WHERE identifier = '".mysqli_real_escape_string($link, $identifier)."' AND profileType = '".mysqli_real_escape_string($link, $profileType)."'";
			$aRs = mysqli_query($link, $sSQL);
			if (mysqli_num_rows($aRs) > 0) { // Already registered
				$data = mysqli_fetch_assoc($aRs);
				$user_id = $data['user_id'];
				$sSQL = "SELECT * FROM user WHERE active = 1 AND user_id = '".mysqli_real_escape_string($link, $user_id)."'";
				$aRs = mysqli_query($link, $sSQL);
				if (mysqli_num_rows($aRs) > 0) {
					$data = mysqli_fetch_assoc($aRs);
					
					$_SESSION['AP_login'] = "true";
					$_SESSION['AP_uid'] = $data['user_id'];
					$_SESSION['AP_ut'] = $data['user_type'];
					$_SESSION['AP_eml'] = $data['user_email'];
					$error = 'Login Successfully!<script>window.location="account.php";</script>';
				}
				else {
					echo 'An unknown error has occurred. Please contact <a href="mailto:service@creditmark.org">service@creditmark.org</a> for assistance.';
				}
			}
			else { // Not registered yet
			
				$sSQL = "SELECT * FROM user WHERE user_email = '".mysqli_real_escape_string($link, $pmdata['email'])."'";
				$aRs = mysqli_query($link, $sSQL);
				if (mysqli_num_rows($aRs) > 0) {
					echo 'Your Email Address is already a registered user with CreditMark. Please <a href="login.php">login</a> with your email instead.</div>';
				}
				else {
					
					$photo = '';
					if ($pmdata['photo'] != "") $photo = "https://pitchmark.org/client/upload/photo/".$pmdata['photo'];
					// Store in User Table
					$sSQL2 = "INSERT INTO user (user_name, user_type, user_email, user_password, user_phone, user_company, user_photo, registered_time, last_modified_time)
			VALUES
						('".mysqli_real_escape_string($link, $pmdata['client_name'])."',
						'1', 
						'".mysqli_real_escape_string($link, $pmdata['email'])."', 
						'".mysqli_real_escape_string($link, $pmdata['password'])."', 
						'".mysqli_real_escape_string($link, $pmdata['mobile'])."', 
						'".mysqli_real_escape_string($link, $pmdata['company_name'])."', 
						'".mysqli_real_escape_string($link, $photo)."', 
						'".$time."', '".$time."')";
					$aRs2 = mysqli_query($link, $sSQL2);
					if ($aRs2) {
						$user_id = mysqli_insert_id($link);
						
						// Store in User Social Table
						$sSQL3 = "INSERT INTO user_social (identifier, profileType, user_id, profileURL, photoURL, displayName, firstName, dob, email, phone, address, country, region, city, zip, created_time, last_modified_time)
			VALUES
							('".mysqli_real_escape_string($link, $identifier)."', 
							'".mysqli_real_escape_string($link, $profileType)."', 
							'".mysqli_real_escape_string($link, $user_id)."', 
							'".mysqli_real_escape_string($link, '')."', 
							'".mysqli_real_escape_string($link, "https://pitchmark.org/client/upload/photo/".$pmdata['photo'])."', 
							'".mysqli_real_escape_string($link, $pmdata['client_name'])."', 
							'".mysqli_real_escape_string($link, $pmdata['client_name'])."', 
							'".mysqli_real_escape_string($link, $pmdata['dob'])."', 
							'".mysqli_real_escape_string($link, $pmdata['email'])."', 
							'".mysqli_real_escape_string($link, $pmdata['mobile'])."', 
							'".mysqli_real_escape_string($link, $pmdata['addr1'].(($pmdata['addr2']!="")?", ".$pmdata['addr2']:""))."', 
							'".mysqli_real_escape_string($link, $pmdata['country'])."', 
							'".mysqli_real_escape_string($link, $pmdata['province'])."', 
							'".mysqli_real_escape_string($link, $pmdata['city'])."',  
							'".mysqli_real_escape_string($link, $pmdata['postcode'])."', 
							'".$time."', '".$time."')";
						
						$aRs3 = mysqli_query($link, $sSQL3);
						
						$_SESSION['AP_login'] = "true";
						$_SESSION['AP_uid'] = $user_id;
						$_SESSION['AP_ut'] = 1;
						$_SESSION['AP_eml'] = $pmdata['email'];
						
						$error = 'Login Successfully!<script>window.location="account.php?edit=1";</script>';
					}
				}
			}
		}
		else {
			$error = "Login Failed! Please try again.";
		}
		dbClose($linkPM);
	}
	else {
		$sSQL = "SELECT * FROM user WHERE active = 1 AND 
			user_email = '".mysqli_real_escape_string($link, $_POST['loginEmail'])."' AND 
			user_password = '".mysqli_real_escape_string($link, $_POST['loginPassword'])."'";
		
		$aRs = mysqli_query($link, $sSQL);
		
		if (mysqli_num_rows($aRs) > 0) { // Login successfully
			$data = mysqli_fetch_assoc($aRs);
			$_SESSION['AP_login'] = "true";
			$_SESSION['AP_uid'] = $data['user_id'];
			$_SESSION['AP_ut'] = $data['user_type'];
			$_SESSION['AP_eml'] = $data['user_email'];
			
			$pg = 'account.php';
			
			$sSQL = "UPDATE user SET last_login = '".date('Y-m-d H:i:s')."' WHERE user_id = '".$data['user_id']."'";
			mysqli_query($link, $sSQL);
			
			if ($_POST['staylogin'] == 1) {
				$time = time() + (3600 * 24 * 90); // 90 days
				setcookie('registered_user@creditmark.org', serialize($_SESSION), $time);
			}
		}
		else $error = "Login Failed! Please try again.";
	}
	dbClose($link);
}
else $error = "Login Failed! Please try again.";

if (!$error) echo 'Login Successfully!<script>window.location="'.$pg.'";</script>';
else echo $error;
?>