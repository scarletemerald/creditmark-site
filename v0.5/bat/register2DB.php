<?php 
require_once("../function.php");

$error = false;

if ($_SESSION['AP_login'] && $_SESSION['AP_uid'] != "" && $_REQUEST['accountCompanyName'] != "") {
	$link = dbConnect();
	
	$time = date("Y-m-d H:i:s");
	$photo = '';
	if (isset($_FILES['profilePhoto'])) {
		require_once("../file.php");
		$photo = fileProcess($_FILES['profilePhoto'], "../upload/photo/");
		if (!is_file("../upload/photo/".$photo)) $photo = "";
	}
	
	$sSQL = "UPDATE `user` SET 
		`user_company` = '".mysqli_real_escape_string($link, $_REQUEST['accountCompanyName'])."', 
		`user_paypal` = '".mysqli_real_escape_string($link, $_REQUEST['bankPayPal'])."', 
		`user_bank_branch` = '".mysqli_real_escape_string($link, $_REQUEST['bankBranchCode'])."', 
		`user_bank_acc` = '".mysqli_real_escape_string($link, $_REQUEST['bankAccount'])."', 
		`user_bank_address` = '".mysqli_real_escape_string($link, $_REQUEST['bankAddress'])."', 
		`user_bank_swift` = '".mysqli_real_escape_string($link, $_REQUEST['bankSwiftCode'])."', 
		`user_bank_iban` = '".mysqli_real_escape_string($link, $_REQUEST['bankIBANCode'])."', 
		`user_photo` = '".mysqli_real_escape_string($link, $photo)."', 
		`last_modified_time` = '".$time."'
		WHERE user_id = '".mysqli_real_escape_string($link, $_SESSION['AP_uid'])."'";
		
	$aRs = mysqli_query($link, $sSQL);
	if (!$aRs) $error = true;
	dbClose($link);
}
else $error = true;
/*
if (!$error) echo '<script>if (confirm("You have registered successfully! Do you wish to Setup your first Customer now?")) { window.location="customer_manage.php?add=1";
}
else {
window.location="account.php";
}</script>';
else {
	if (strlen($error) > 1) echo $error;
	else echo "Registration Failed! Please update your details later.<script>window.location='account.php';</script>";
}*/
if (!$error) echo '<script>$("#register2-form").hide();$("#register3-form").show();setTimeout(\'$(".modal").modal("hide");$("body").removeClass("modal-open");$(".modal-backdrop").remove();\',3000 );</script>';
else {
	if (strlen($error) > 1) echo $error;
	else echo "Registration Failed! Please try again.";
}
?>