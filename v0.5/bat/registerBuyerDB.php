<?php 
require_once("../function.php");

$error = false;

if ($_REQUEST['buyerUserName'] != "" && $_REQUEST['buyerCompany'] != "" && $_REQUEST['buyerEmail'] != "" && $_REQUEST['buyerPassword'] != "") {
	$link = dbConnect();
	
	$time = date("Y-m-d H:i:s");
	
	$sSQL = "SELECT * FROM user WHERE user_email = '".mysqli_real_escape_string($link, $_REQUEST['buyerEmail'])."'";
	$aRs = mysqli_query($link, $sSQL);
	if (mysqli_num_rows($aRs) > 0) {
		$error = "Your account email already exists. Please login instead.";
	}
	
	if (!$error) {
		
		$photo = '';
		if (isset($_FILES['buyerCompanyLogo'])) {
			require_once("../file.php");
			$photo = fileProcess($_FILES['buyerCompanyLogo'], "../upload/photo/");
			if (!is_file("../upload/photo/".$photo)) $photo = "";
		}
		
		$sSQL = "INSERT INTO `user` (
			`user_name`, 
			`user_type`,
			`user_email`, 
			`user_password`, 
			`user_phone`,
			`user_company`,
			`user_company_url`,
			`user_job_title`,
			`user_photo`,
			`active`,
			`registered_time`, 
			`last_modified_time` 
		) VALUES (
			'".mysqli_real_escape_string($link, $_REQUEST['buyerUserName'])."', 
			'2', 
			'".mysqli_real_escape_string($link, $_REQUEST['buyerEmail'])."', 
			'".mysqli_real_escape_string($link, $_REQUEST['buyerPassword'])."', 
			'".mysqli_real_escape_string($link, $_REQUEST['buyerPhone'])."', 
			'".mysqli_real_escape_string($link, $_REQUEST['buyerCompany'])."', 
			'".mysqli_real_escape_string($link, $_REQUEST['buyerCompanyWebsite'])."', 
			'".mysqli_real_escape_string($link, $_REQUEST['buyerJob'])."', 
			'".mysqli_real_escape_string($link, $photo)."', 
			'0', 
			'".$time."', 
			'".$time."'
		)";
		
		$aRs = mysqli_query($link, $sSQL);
		if ($aRs) {
			
			$user_id = mysql_insert_id($link);
			
			$sSQL = "SELECT * FROM customer WHERE company_name = '".mysqli_real_escape_string($link, trim($_REQUEST['buyerCompany']))."'";
			$aRs = mysqli_query($link, $sSQL);
			if (mysqli_num_rows($aRs) > 0) { // Company Name Exists
			}
			else { // Create New Company
				$sSQL = "INSERT INTO customer (user_id, company_name, company_uen, company_url, company_logo, created_time, last_modified_time) VALUES ('".$user_id."', '".mysqli_real_escape_string($link, trim($_REQUEST['buyerCompany']))."', '".mysqli_real_escape_string($link, trim($_REQUEST['buyerCompanyUEN']))."', '".mysqli_real_escape_string($link, trim($_REQUEST['buyerCompanyWebsite']))."', 
			'".mysqli_real_escape_string($link, $photo)."', '".$time."', '".$time."')";
				if (mysqli_query($link, $sSQL)) {
					$cust_id = mysqli_insert_id($link);
				}
				else $error .= "Failed to insert Row #".$row."!<br />";
			}
		}
		else $error = true;
	}
	dbClose($link);
}
else $error = true;

if (!$error) echo "<script>alert('You have registered successfully! We will let you know soon once your account is verified and approved.'); window.location='index.php';</script>";
else {
	if (strlen($error) > 1) echo $error;
	else echo "<script>alert('Registration Failed! Please update your details later.');window.location='index.php';</script>";
}
?>