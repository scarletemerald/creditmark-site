<?php 
require_once("../function.php");
require_once("../file.php");

$error = false;

if ($_REQUEST['companyName'] != "" && $_REQUEST['contactName_1'] != "" && $_REQUEST['contactEmail_1'] != "") {
	$link = dbConnect();
	$time = date("Y-m-d H:i:s");
	
	if ($_REQUEST['companyID'] == "") {
		$sSQL = "SELECT * FROM customer WHERE company_name = '".mysqli_real_escape_string($link, $_REQUEST['companyName'])."'";
		$aRs = mysqli_query($link, $sSQL);
		if (mysqli_num_rows($aRs) > 0) {
			$d = mysqli_fetch_assoc($aRs);
			$_REQUEST['companyID'] = $d['cust_id'];
		}
	}
	$newCoy = false;
	$coyID = $_REQUEST['companyID'];
	if ($coyID == "") {
		/*$sSQL = "INSERT INTO `customer` (`company_name`, `company_uen`, `company_url`, `created_time`, `last_modified_time`) 
			VALUES 
			('".mysqli_real_escape_string($link, strtoupper($_REQUEST['companyName']))."', '".mysqli_real_escape_string($link, strtoupper($_REQUEST['companyUEN']))."', '".mysqli_real_escape_string($link, strtolower($_REQUEST['companyURL']))."', '".$time."', '".$time."')";
		mysqli_query($link, $sSQL);
		$coyID = mysqli_insert_id($link);*/
		$error = 'You have to select a company from the suggested list. Please try again.';
	}
	
	if ($coyID != "") {
		$sSQL = "UPDATE `customer_contact` SET active = 0 WHERE cust_id = '".mysqli_real_escape_string($link, $coyID)."' AND user_id = '".$_SESSION['AP_uid']."'";
		mysqli_query($link, $sSQL);
		
		for ($i=1; $i<=5; $i++) {
			if ($_REQUEST['contactName_'.$i] != "" && $_REQUEST['contactEmail_'.$i] != "") {
				$sSQL = "INSERT INTO `customer_contact` (`user_id`, `cust_id`, `contact_name`, `contact_email`, `created_time`, `last_modified_time`) 
					VALUES 
					('".mysqli_real_escape_string($link, $_SESSION['AP_uid'])."', '".mysqli_real_escape_string($link, $coyID)."',
					'".mysqli_real_escape_string($link, $_REQUEST['contactName_'.$i])."', '".mysqli_real_escape_string($link, $_REQUEST['contactEmail_'.$i])."',
					'".$time."', '".$time."')";
				mysqli_query($link, $sSQL);
				$newCoy = true;
			}
		}
	}
	
	dbClose($link);
}
else $error = true;

if (!$error) {
	if ($newCoy) {
		echo '<script>if (confirm("You have created your Customer Contact successfully! Do you wish to setup a Case for this Customer Contact now?")) { window.top.location="incident_manage.php?add=1&cid='.$coyID.'";
	}
	else {
	window.top.location="incident_manage.php";
	}</script>';
	}
	else {
		echo '<script>window.top.location="incident_manage.php";</script>';
	}
}
else {
	if (strlen($error) > 1) echo $error;
	else echo "Update Failed! Please try again.";
}
?>