<?php 
require_once("../function.php");

$error = false;

if ($_REQUEST['id'] != "") {
	$link = dbConnect();
	$time = date("Y-m-d H:i:s");
	
	$sSQL = "SELECT * FROM customer WHERE cust_id = '".mysqli_real_escape_string($link, $_REQUEST['id'])."' AND user_id = '".$_SESSION['AP_uid']."'";
	$aRs = mysqli_query($link, $sSQL);
	if (mysqli_num_rows($aRs) <= 0) {
		$error = 'Customer does not exist.';
	}
	else {
		$sSQL = "UPDATE customer_incident SET status = 2 WHERE cust_id = '".mysqli_real_escape_string($link, $_REQUEST['id'])."' AND user_id = '".$_SESSION['AP_uid']."' AND status = 1";
		if (!mysqli_query($link, $sSQL)) $error = true;
	}
	dbClose($link);
}
else $error = true;

if (!$error) echo 'You have updated the Cases for this Customer successfully!';
else {
	if (strlen($error) > 1) echo $error;
	else echo "Update Failed! Please try again.";
}
?>