<?php 
require_once("../function.php");

$error = false;

if ($_REQUEST['id'] != "") {
	$link = dbConnect();
	$time = date("Y-m-d H:i:s");
	
	$sSQL = "SELECT * FROM customer_incident WHERE incident_id = '".mysqli_real_escape_string($link, $_REQUEST['id'])."' AND user_id = '".$_SESSION['AP_uid']."'";
	$aRs = mysqli_query($link, $sSQL);
	if (mysqli_num_rows($aRs) <= 0) {
		$error = 'Case does not exist.';
	}
	else {
		
		if ($_REQUEST['prdate'] == "") {
			$sSQL = "UPDATE customer_incident SET status = '".mysqli_real_escape_string($link, $_REQUEST['status'])."' WHERE incident_id = '".mysqli_real_escape_string($link, $_REQUEST['id'])."' AND user_id = '".$_SESSION['AP_uid']."'";
		}
		else {
			$paymentDate = ($_REQUEST['prdate'] != "")?date("Y-m-d", strtotime($_REQUEST['prdate'])):"";
			$sSQL = "UPDATE customer_incident SET payment_received = '".mysqli_real_escape_string($link, $paymentDate)."', status = '3' WHERE incident_id = '".mysqli_real_escape_string($link, $_REQUEST['id'])."' AND user_id = '".$_SESSION['AP_uid']."'";
		}
		if (!mysqli_query($link, $sSQL)) $error = true;
		
	}
	dbClose($link);
}
else $error = true;

if (!$error) echo 'You have updated the Case successfully!';
else {
	if (strlen($error) > 1) echo $error;
	else echo "Update Failed! Please try again.";
}
?>