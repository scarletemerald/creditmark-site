<?php 
ini_set("auto_detect_line_endings", true);
require_once("../function.php");
require_once("../file.php");

$error = false;

$time = date("Y-m-d H:i:s");

$maxsize = 5242880;
$filetype = array('csv');

if (!$_FILES['importFile']['error'] && $_FILES['importFile']['tmp_name'] != "") {
	$file_size = @filesize($_FILES['importFile']["tmp_name"]);
	if (!$file_size || $file_size > $maxsize) {
		$error = "File exceeds the maximum allowed size.";
	}
	else {
		$impfile = fileProcess($_FILES['importFile'], "../upload/csv/", array('csv'));
		if (is_file("../upload/csv/".$impfile)) {
			$row=1;
			$handle = fopen("../upload/csv/".$impfile, "r");
			
			$link = dbConnect();
			$user_id = $_SESSION['AP_uid'];
			
			while (($data = fgetcsv($handle, 10000, ",")) !== FALSE) {
				// 1st row is header
				if ($row > 1) {
					// Get data - data format:
					// Company Name, Contact Person Name, Contact Person Email, 
					//Invoice No, Invoice Date (dd-mm-yyyy), Invoice Due Date (dd-mm-yyyy), 
					//Invoice Currency (e.g. USD, SGD), Invoice Amount, 
					//Invoice Interest Rate (%), Invoice Interest Period (1/7/30/365 Days)
					
					// Check Contact Email Exists
					$sSQL = "SELECT * FROM customer_contact WHERE contact_email = '".mysqli_real_escape_string($link, trim($data[2]))."' AND user_id = '".mysqli_real_escape_string($link, $user_id)."'";
					$aRs = mysqli_query($link, $sSQL);
					$cust_id = '';
					$contact_id = '';
					if (mysqli_num_rows($aRs) > 0) { // Contact Exists
						$contact = mysqli_fetch_assoc($aRs);
						$cust_id = $contact['cust_id'];
						$contact_id = $contact['contact_id'];
					}
					else {
						// Check Company Exists
						$sSQL = "SELECT * FROM customer WHERE company_name = '".mysqli_real_escape_string($link, trim($data[0]))."'";
						$aRs = mysqli_query($link, $sSQL);
						if (mysqli_num_rows($aRs) > 0) { // Company Name Exists
							$company = mysqli_fetch_assoc($aRs);
							$cust_id = $company['cust_id'];
						}
						else { // Create New Company
							$sSQL = "INSERT INTO customer (company_name, created_time, last_modified_time) VALUES ('".mysqli_real_escape_string($link, trim($data[0]))."', '".$time."', '".$time."')";
							if (mysqli_query($link, $sSQL)) {
								$cust_id = mysqli_insert_id($link);
							}
							else $error .= "Failed to insert Row #".$row."!<br />";
						}
						
						// Create New Contact
						if ($cust_id != "") {
							$sSQL = "INSERT INTO customer_contact (user_id, cust_id, contact_name, contact_email, created_time, last_modified_time) 
								VALUES (
								'".mysqli_real_escape_string($link, $user_id)."', 
								'".mysqli_real_escape_string($link, $cust_id)."', 
								'".mysqli_real_escape_string($link, trim($data[1]))."', 
								'".mysqli_real_escape_string($link, trim($data[2]))."', 
								'".$time."', '".$time."');";
							if (mysqli_query($link, $sSQL)) {
								$contact_id = mysqli_insert_id($link);
							}
							else $error .= "Failed to insert Row #".$row."!<br />";
						}
					}
					
					if ($cust_id != "" && $contact_id != "") {
						// Store Case
						$sSQL = "INSERT INTO customer_incident (user_id, cust_id, contact_id, 
							invoice_no, invoice_date, invoice_due, 
							invoice_currency, invoice_amount, 
							invoice_interest, invoice_interest_period, 
							created_time, last_modified_time) 
							VALUES (
							'".mysqli_real_escape_string($link, $user_id)."', 
							'".mysqli_real_escape_string($link, $cust_id)."', 
							'".mysqli_real_escape_string($link, $contact_id)."', 
							'".mysqli_real_escape_string($link, trim($data[3]))."', 
							'".mysqli_real_escape_string($link, date("Y-m-d", strtotime(trim($data[4]))))."', 
							'".mysqli_real_escape_string($link, date("Y-m-d", strtotime(trim($data[5]))))."', 
							'".mysqli_real_escape_string($link, trim($data[6]))."', 
							'".mysqli_real_escape_string($link, trim($data[7]))."', 
							'".mysqli_real_escape_string($link, trim($data[8]))."', 
							'".mysqli_real_escape_string($link, trim($data[9]))."', 
							'".$time."', '".$time."');";
						if (!mysqli_query($link, $sSQL)) {
							$error .= "Failed to insert Row #".$row."!<br />";
						}

					}
				}
				$row++;
			}
		}
		else $error = "Error uploading file. Please try again.";
	}
}
else $error = "Error uploading file. Please try again.";

if (!$error) echo '<script>alert("Your file is imported."); window.parent.location.reload();</script>';
else echo $error;
?>