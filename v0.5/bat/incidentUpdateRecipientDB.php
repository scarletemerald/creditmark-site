<?php 
require_once("../function.php");

$error = false;

//print_r($_REQUEST);
$data = base64_decode($_REQUEST['params']);
$data = unserialize($data);
//print_r($data);

if ($data['iid'] != "" && $data['uid'] != "" && $data['cid'] != "" && $_REQUEST['contactName_1'] != "" && $_REQUEST['contactEmail_1'] != "") {
	$link = dbConnect();
	$time = date("Y-m-d H:i:s");
	
	$sSQL = "SELECT * FROM customer_incident WHERE incident_id = '".mysqli_real_escape_string($link, $data['iid'])."' AND cust_id = '".mysqli_real_escape_string($link, $data['cid'])."' AND user_id = '".mysqli_real_escape_string($link, $data['uid'])."' AND status = 1";
	$aRs = mysqli_query($link, $sSQL);
	if (mysqli_num_rows($aRs) <= 0) {
		$error = '<script>alert("Invoice does not exist or you have already updated this payment details."); window.top.location="index.php";</script>';
	}
	
	if (!$error) {
		$incidentID = $data['iid'];
		
		$recipients = "";
		for ($i=1; $i<=5; $i++) {
			if ($_REQUEST['contactName_'.$i] != "" && $_REQUEST['contactEmail_'.$i] != "") {
				$recipients .= $_REQUEST['contactName_'.$i]."::".$_REQUEST['contactEmail_'.$i]."\n";
			}
		}
		
		$sSQL = "UPDATE `customer_incident` SET 
		`recipients` = '".mysqli_real_escape_string($link, $recipients)."', 
		`last_modified_time` = '".$time."'
		WHERE `cust_id` = '".mysqli_real_escape_string($link, $data['cid'])."' AND 
		 `user_id` = '".mysqli_real_escape_string($link, $data['uid'])."' AND 
		`incident_id` = '".mysqli_real_escape_string($link, $incidentID)."'";
		mysqli_query($link, $sSQL);
	}
	dbClose($link);
}
else $error = true;

if (!$error) echo '<script>alert("Thank you! We will notify the company"); window.top.location="index.php";</script>';
else {
	if (strlen($error) > 1) echo $error;
	else echo "Update Failed! Please try again.";
}
?>