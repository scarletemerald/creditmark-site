<?php 
require_once("../function.php");

$error = false;

// Clear session/cookie
unset($_SESSION["AP_login"]);
unset($_SESSION["AP_uid"]);
unset($_SESSION["AP_eml"]);
setcookie('registered_user@creditmark.org', '', time()-3600);
$_SESSION = array();
$_COOKIES = array();

if ($_POST['loginEmail'] != "") {
	$link = dbConnect();
	
	$sSQL = "SELECT * FROM user WHERE 
		active = 1 AND user_email = '".mysqli_real_escape_string($link, $_POST['loginEmail'])."'";
	
	$aRs = mysqli_query($link, $sSQL);
	
	if (mysqli_num_rows($aRs) > 0) { // Record Exists
		$data = mysqli_fetch_assoc($aRs);
		$subject = "[CreditMark.org] Forget Password Request";
		$content = '
		<p style="font-family: Arial, sans-serif; font-size: 12px; color: #000;">Dear '.$data['user_name'].',<br />
		<br />
		You have requested to retrieve your password for <a href="'.WL_PATH.'">CreditMark.org</a>.<br />
		<br />
		Your password is:<br />
		<strong>'.$data['user_password'].'</strong><br />
		<br />
		Please <a href="'.WL_PATH.'login.php">click here</a> to login.<br />
		<br />
		Best Regards,<br />
		CreditMark.org
		';
		$mail = sendEmail($data['user_email'], $subject, $content);
		if (!$mail) $error = "Failed to send email. Please try again later.";
	}
	else $error = "Email does not exist in our system.";
	dbClose($link);
}
else $error = "Please enter your email first.";

if (!$error) echo "Please check your email for your password.";
else echo $error;
?>