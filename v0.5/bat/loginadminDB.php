<?php 
require_once("../function.php");

$error = false;

// Clear session/cookie
unset($_SESSION["AP_login"]);
unset($_SESSION["AP_uid"]);
unset($_SESSION["AP_eml"]);
setcookie('registered_user@creditmark.org', '', time()-3600);
$_SESSION = array();
$_COOKIES = array();

if ($_POST['loginEmail'] != "" && $_POST['loginPassword'] != "") {
	$link = dbConnect();
	
	
	$sSQL = "SELECT * FROM user_admin WHERE active = 1 AND 
		user_email = '".mysqli_real_escape_string($link, $_POST['loginEmail'])."' AND 
		user_password = '".mysqli_real_escape_string($link, $_POST['loginPassword'])."'";
	
	$aRs = mysqli_query($link, $sSQL);
	
	if (mysqli_num_rows($aRs) > 0) { // Login successfully
		$data = mysqli_fetch_assoc($aRs);
		$_SESSION['AP_login'] = "true";
		$_SESSION['AP_uid'] = $data['user_id'];
		$_SESSION['AP_ut'] = $data['user_type'];
		$_SESSION['AP_eml'] = $data['user_email'];
		
		$pg = 'admin_account.php';
		//$pg = 'admin_manage_company.php';
		
		$sSQL = "UPDATE user_admin SET last_login = '".date('Y-m-d H:i:s')."' WHERE user_id = '".$data['user_id']."'";
		mysqli_query($link, $sSQL);
		
		if ($_POST['staylogin'] == 1) {
			$time = time() + (3600 * 24 * 90); // 90 days
			setcookie('registered_user@creditmark.org', serialize($_SESSION), $time);
		}
	}
	else $error = "Login Failed! Please try again.";
	dbClose($link);
}
else $error = "Login Failed! Please try again.";

if (!$error) echo 'Login Successfully!<script>window.location="'.$pg.'";</script>';
else echo $error;
?>