<?php 
require_once("../function.php");

$error = false;

if ($_SESSION['AP_login'] && $_SESSION['AP_uid'] != "" && $_REQUEST['rateCompanyName'] != "") {
	
	$status = 2; // Paused
	if ($_REQUEST['ratepPaymentDate'] != "") $status = 3;
	
	$rateInvoiceSubmitted = '';
	if (is_array($_REQUEST['rateInvoiceSubmitted'])) {
		foreach ($_REQUEST['rateInvoiceSubmitted'] as $val) {
			$rateInvoiceSubmitted .= $val.";";
		}
	}
	
	$ratePlaceOrder = '';
	if (is_array($_REQUEST['ratePlaceOrder'])) {
		foreach ($_REQUEST['ratePlaceOrder'] as $val) {
			$ratePlaceOrder .= $val.";";
		}
	}
	$rateAgree = 0;
	if ($_REQUEST['rateAgree'] != "") $rateAgree = 1;
	
	$link = dbConnect();
	
	$time = date("Y-m-d H:i:s");
	
	$sSQL = "SELECT * FROM customer WHERE company_name = '".mysqli_real_escape_string($link, $_REQUEST['rateCompanyName'])."'";
	$aRs = mysqli_query($link, $sSQL);
	if (mysqli_num_rows($aRs) <= 0) {
		/*$sSQL = "INSERT INTO `customer` (`company_name`, `company_uen`, `company_url`, `currency_default`, `active`, `created_time`, `last_modified_time`)
		VALUES
			('".mysqli_real_escape_string($link, strtoupper($_REQUEST['rateCompanyName']))."', '".mysqli_real_escape_string($link, strtoupper($_REQUEST['rateBizRegNo']))."', '".mysqli_real_escape_string($link, strtolower($_REQUEST['rateCompanyURL']))."', '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceCurrency'])."', 1, '".$time."', '".$time."')";
			$aRs = mysqli_query($link, $sSQL);
			
			$company_id = mysqli_insert_id($link);*/
		$error = 'You have to select a company from the suggested list. Please try again.';
	}
	else {
		$company_id = mysqli_fetch_assoc($aRs);
		$company_id = $company_id['cust_id'];
		
		$sSQL = "UPDATE customer SET company_url = '".mysqli_real_escape_string($link, $_REQUEST['rateCompanyURL'])."' WHERE cust_id = '".$company_id."'";
		$aRs = mysqli_query($link, $sSQL);
		
		$rateInvoiceDate = ($_REQUEST['rateInvoiceDate'] != "")?date("Y-m-d", strtotime($_REQUEST['rateInvoiceDate'])):"";
		$rateInvoiceDueDate = ($_REQUEST['rateInvoiceDueDate'] != "")?date("Y-m-d", strtotime($_REQUEST['rateInvoiceDueDate'])):"";
		$ratePaymentDate = ($_REQUEST['ratePaymentDate'] != "")?date("Y-m-d", strtotime($_REQUEST['ratePaymentDate'])):"";
		/*
		$sSQL = "INSERT INTO `customer_contact` (`user_id`, `cust_id`, `contact_name`, `contact_email`, `active`, `created_time`, `last_modified_time`)
	VALUES
		('".$_SESSION['AP_uid']."', '".$company_id."', '".mysqli_real_escape_string($link, $_REQUEST['rateContactName'])."', '".mysqli_real_escape_string($link, $_REQUEST['rateContactEmail'])."', 1,  '".$time."', '".$time."')";
		$aRs = mysqli_query($link, $sSQL);
		
		$contact_id = mysqli_insert_id($link);
		
		$sSQL = "INSERT INTO `customer_incident` (`user_id`, `cust_id`, `contact_id`, `invoice_no`, `invoice_date`, `invoice_due`, `invoice_currency`, `invoice_amount`, `invoice_interest`, `invoice_interest_period`, `payment_received`, `status`, `i_agree`, `created_time`, `last_modified_time`)
	VALUES
		('".$_SESSION['AP_uid']."', '".$company_id."', '".$contact_id."', '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceNo'])."', '".date("Y-m-d", strtotime($_REQUEST['rateInvoiceDate']))."', '".date("Y-m-d", strtotime($_REQUEST['rateInvoiceDueDate']))."', '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceCurrency'])."', '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceAmount'])."', '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceInterest'])."', '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceInterestPeriod'])."', '".date("Y-m-d", strtotime($_REQUEST['ratePaymentDate']))."', '".$status."',  '".$rateAgree."', '".$time."', '".$time."')";
		$aRs = mysqli_query($link, $sSQL);
		$incident_id = mysqli_insert_id($link);
		*/
		$sSQL = "INSERT INTO `customer_incident` (`user_id`, `cust_id`, `invoice_date`, `invoice_due`, `payment_received`, `status`, `i_agree`, `created_time`, `last_modified_time`)
	VALUES
		('".$_SESSION['AP_uid']."', '".$company_id."', '".$rateInvoiceDate."', '".$rateInvoiceDueDate."', '".$ratePaymentDate."', '".$status."',  '".$rateAgree."', '".$time."', '".$time."')";
		$aRs = mysqli_query($link, $sSQL);
		$incident_id = mysqli_insert_id($link);
		$sSQL = "INSERT INTO `customer_rating` (`user_id`, `cust_id`, `incident_id`, `rating`, `supplier_type`, `invoice_submission`, `invoice_issued_ok`, `placed_order`, `product_delivered`, `created_time`, `last_modified_time`)
	VALUES
		('".$_SESSION['AP_uid']."', '".$company_id."', '".$incident_id."', '".mysqli_real_escape_string($link, $_REQUEST['rateCompanyRatingVal'])."', '".mysqli_real_escape_string($link, $_REQUEST['rateCompanyUserType'])."', '".mysqli_real_escape_string($link, $rateInvoiceSubmitted)."', '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceConfirm'])."', '".mysqli_real_escape_string($link, $ratePlaceOrder)."', '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceConfirm2'])."',  '".$time."', '".$time."')";
		$aRs = mysqli_query($link, $sSQL);
		if (!$aRs) $error = true;
	}
	dbClose($link);
}
else $error = true;
if (!$error) echo '<script>if (confirm("You have registered successfully! Do you wish to review another Company now?")) { window.location="customer_manage_reviews.php?add=1";
}
else {
window.location="account.php";
}</script>';
else {
	if (strlen($error) > 1) echo $error;
	else echo "<script>alert('Registration Failed! Please update your details later.');window.location='account.php';</script>";
}
?>