<?php
	session_start(); 

	$config   = '../hybridauth-2.3.0/hybridauth/config.php';
	require_once("../hybridauth-2.3.0/hybridauth/Hybrid/Auth.php" );
	
	try{
		$hybridauth = new Hybrid_Auth( $config );
	}
	catch( Exception $e ){
		echo "Ooophs, we got an error: " . $e->getMessage();
	}
	
	$provider  = @ $_GET["provider"];
	$return_to = @ $_GET["return_to"];
	
	if( ! $return_to ){
		echo "Invalid params!";
	}

	if( ! empty( $provider ) && $hybridauth->isConnectedWith( $provider ) )
	{
		$return_to = $return_to . ( strpos( $return_to, '?' ) ? '&' : '?' ) . "connected_with=" . $provider ;
		
?>
<script language="javascript"> 
	if(  window.opener ){
		window.opener.parent.location.href = "<?php echo $return_to; ?>";
	}

	window.self.close();
</script>
<?php
		die();
	}
	if( ! empty( $provider ) )
	{
		$params = array();
	
		if( $provider == "OpenID" ){
			$params["openid_identifier"] = @ $_REQUEST["openid_identifier"];
		}
	
		if( isset( $_REQUEST["redirect_to_idp"] ) ){
			$adapter = $hybridauth->authenticate( $provider, $params );
		}
		else{
	?>Loading...
	
	<script>
		window.location.href = window.location.href + "&redirect_to_idp=1";
	</script>
	<?php }
			die();
	} ?>