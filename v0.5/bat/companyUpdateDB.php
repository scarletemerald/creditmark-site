<?php 
require_once("../function.php");

$error = false;

if ($_SESSION['AP_login'] && $_SESSION['AP_uid'] != "" && $_SESSION['AP_ut'] == 2 && $_REQUEST['editCompanyID'] != "") {
	
	$link = dbConnect();
	$time = date("Y-m-d H:i:s");
	
	$cust = getCustomer($_REQUEST['editCompanyID'], -1);
	$cust = array_pop($cust);
	if (sizeof($cust) <= 0) { // Non Exist Customer
		$error = 'The page you\'ve requested is unavailable. Please try again later.';
	}
	else {
		
		$photo = '';
		if (isset($_FILES['editCompanyLogo'])) {
			require_once("../file.php");
			$photo = fileProcess($_FILES['editCompanyLogo'], "../upload/photo/");
			if (!is_file("../upload/photo/".$photo)) $photo = "";
		}
		
		$sSQL = "UPDATE customer SET
			company_name = '".mysqli_real_escape_string($link, $_REQUEST['editCompanyName'])."',
			company_uen = '".mysqli_real_escape_string($link, $_REQUEST['editCompanyUEN'])."',
			company_url = '".mysqli_real_escape_string($link, $_REQUEST['editCompanyURL'])."',
			currency_default = '".mysqli_real_escape_string($link, $_REQUEST['editDefaultCurrency'])."',
			standard_credit_terms = '".mysqli_real_escape_string($link, $_REQUEST['editCreditTerms'])."',
			company_procurement_policies = '".mysqli_real_escape_string($link, $_REQUEST['editProcurementPolicies'])."',
			company_comment = '".mysqli_real_escape_string($link, $_REQUEST['editAdditionalInfo'])."',";
		if ($photo != "") $sSQL .= "company_logo = '".mysqli_real_escape_string($link, $photo)."',";
		$sSQL .= "last_modified_time = '".$time."'
			WHERE cust_id = '".mysqli_real_escape_string($link, $_REQUEST['editCompanyID'])."'";
		$aRs = mysqli_query($link, $sSQL);
		if (!$aRs) $error = true;
	}
	dbClose($link);
}
else $error = true;
if (!$error)  echo "<script>alert('Your company details are updated.'); window.top.location.reload();</script>";
else {
	if (strlen($error) > 1) echo $error;
	else echo "<script>alert('Submission Failed! Please try again later.'); window.top.location = 'search_company_main.php';</script>";
}
?>