<?php 
require_once("../function.php");

$error = false;

$data = base64_decode($_REQUEST['params']);
$data = unserialize($data);

if ($_REQUEST['transNo'] != "" && $_REQUEST['datePaid'] != "" && $data['iid'] != "" && $data['uid'] != "" && $data['cid'] != "") {
	$link = dbConnect();
	$time = date("Y-m-d H:i:s");
	
	$sSQL = "SELECT * FROM customer_incident WHERE incident_id = '".mysqli_real_escape_string($link, $data['iid'])."' AND cust_id = '".mysqli_real_escape_string($link, $data['cid'])."' AND user_id = '".mysqli_real_escape_string($link, $data['uid'])."' AND status = 1";
	$aRs = mysqli_query($link, $sSQL);
	if (mysqli_num_rows($aRs) <= 0) {
		$error = '<script>alert("Invoice does not exist or you have already updated this payment details."); window.top.location="index.php";</script>';
	}
	
	if (!$error) {
		$incidentID = $data['iid'];
		$paymentDate = ($_REQUEST['datePaid'] != "")?date("Y-m-d", strtotime($_REQUEST['datePaid'])):"";
		$status = 3;
		$sSQL = "UPDATE `customer_incident` SET 
		`payment_received` = '".mysqli_real_escape_string($link, $paymentDate)."', 
		`transaction_no` = '".mysqli_real_escape_string($link, $_REQUEST['transNo'])."', 
		`status` = '".mysqli_real_escape_string($link, $status)."', 
		`last_modified_time` = '".$time."'
		WHERE `cust_id` = '".mysqli_real_escape_string($link, $data['cid'])."' AND 
		 `user_id` = '".mysqli_real_escape_string($link, $data['uid'])."' AND 
		`incident_id` = '".mysqli_real_escape_string($link, $incidentID)."'";
		mysqli_query($link, $sSQL);
	}
	dbClose($link);
}
else $error = true;

if (!$error) echo '<script>alert("Thank you! We will notify the company"); window.top.location="index.php";</script>';
else {
	if (strlen($error) > 1) echo $error;
	else echo "Update Failed! Please try again.";
}
?>