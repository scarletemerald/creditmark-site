<?php 
require_once("../function.php");

$error = false;

$buyer_Id = base64_decode($_REQUEST['id']);
$stats = $_REQUEST['stats'];

if ($login && $_SESSION['AP_ut'] == 9999 && $buyer_Id > 0) {
	$link = dbConnect();
	$time = date("Y-m-d H:i:s");
	
	$sSQL = "UPDATE user SET active = '".mysqli_real_escape_string($link, $stats)."', last_modified_time = '".$time."' WHERE user_id = '".mysqli_real_escape_string($link, $buyer_Id)."'";
	mysqli_query($link, $sSQL);
	
	$sSQL = "UPDATE customer SET active = '".mysqli_real_escape_string($link, $stats)."', last_modified_time = '".$time."' WHERE user_id = '".mysqli_real_escape_string($link, $buyer_Id)."'";
	mysqli_query($link, $sSQL);
	
	dbClose($link);
}
else $error = true;

if (!$error) echo 'Status Updated';
else {
	if (strlen($error) > 1) echo $error;
	else echo "Update Failed! Please try again.";
}
?>