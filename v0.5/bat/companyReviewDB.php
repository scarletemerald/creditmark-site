<?php 
require_once("../function.php");

$error = false;
if ($_SESSION['AP_login'] && $_SESSION['AP_uid'] != "" && $_SESSION['AP_ut'] == 1 && $_REQUEST['rateCompanyName'] != "") {
	
	$status = 2; // Paused
	if ($_REQUEST['ratePaymentDate'] != "") $status = 3;
	
	$rateInvoiceDate = ($_REQUEST['rateInvoiceDate'] != "")?date("Y-m-d", strtotime($_REQUEST['rateInvoiceDate'])):"";
	$rateInvoiceDueDate = ($_REQUEST['rateInvoiceDueDate'] != "")?date("Y-m-d", strtotime($_REQUEST['rateInvoiceDueDate'])):"";
	$ratePaymentDate = ($_REQUEST['ratePaymentDate'] != "")?date("Y-m-d", strtotime($_REQUEST['ratePaymentDate'])):"";
	
	$rateInvoiceSubmitted = '';
	
	if (is_array($_REQUEST['rateInvoiceSubmitted'])) {
		foreach ($_REQUEST['rateInvoiceSubmitted'] as $val) {
			$rateInvoiceSubmitted .= $val.";";
		}
	}
	
	$ratePlaceOrder = '';
	if (is_array($_REQUEST['ratePlaceOrder'])) {
		foreach ($_REQUEST['ratePlaceOrder'] as $val) {
			$ratePlaceOrder .= $val.";";
		}
	}
	$rateAgree = 0;
	if ($_REQUEST['rateAgree'] != "") $rateAgree = 1;
	
	$link = dbConnect();
	
	$time = date("Y-m-d H:i:s");
	
	$cust = getCustomer("", -1, "", "", " AND company_name = '".mysqli_real_escape_string($link, $_REQUEST['rateCompanyName'])."'");
	$cust = array_pop($cust);
	if (sizeof($cust) > 0) { // Existing Customer
		$company_id = $cust['cust_id'];
		/*
		$cust = getCustomerContact($company_id, $_SESSION['AP_uid'], "", "", " AND contact_name = '".mysqli_real_escape_string($link, $_REQUEST['rateContactName'])."'");
		$cust = array_pop($cust);
		if (sizeof($cust) > 0) { // Existing Customer
			$contact_id = $cust['contact_id'];
		}
		else {
		 $sSQL = "INSERT INTO `customer_contact` (`user_id`, `cust_id`, `contact_name`, `contact_email`, `active`, `created_time`, `last_modified_time`)
		VALUES
			('".$_SESSION['AP_uid']."', '".$company_id."', '".mysqli_real_escape_string($link, $_REQUEST['rateContactName'])."', '".mysqli_real_escape_string($link, $_REQUEST['rateContactEmail'])."', 1,  '".$time."', '".$time."')";
			$aRs = mysqli_query($link, $sSQL);
			
			$contact_id = mysqli_insert_id($link);
		}
		*/
		
		if ($_REQUEST['incidentID'] == "") {
			/*$sSQL = "INSERT INTO `customer_incident` (`user_id`, `cust_id`, `contact_id`, `invoice_no`, `invoice_date`, `invoice_due`, `invoice_currency`, `invoice_amount`, `invoice_interest`, `invoice_interest_period`, `payment_received`, `status`, `i_agree`, `created_time`, `last_modified_time`)
		VALUES
			('".$_SESSION['AP_uid']."', '".$company_id."', '".$contact_id."', '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceNo'])."', '".mysqli_real_escape_string($link, $rateInvoiceDate)."', '".mysqli_real_escape_string($link, $rateInvoiceDueDate)."', '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceCurrency'])."', '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceAmount'])."', '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceInterest'])."', '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceInterestPeriod'])."', '".mysqli_real_escape_string($link, $ratePaymentDate)."', '".$status."',  '".$rateAgree."', '".$time."', '".$time."')";*/
			$sSQL = "INSERT INTO `customer_incident` (`user_id`, `cust_id`, `invoice_date`, `invoice_due`, `payment_received`, `status`, `i_agree`, `created_time`, `last_modified_time`)
		VALUES
			('".$_SESSION['AP_uid']."', '".$company_id."', '".mysqli_real_escape_string($link, $rateInvoiceDate)."', '".mysqli_real_escape_string($link, $rateInvoiceDueDate)."', '".mysqli_real_escape_string($link, $ratePaymentDate)."', '".$status."',  '".$rateAgree."', '".$time."', '".$time."')";
			$aRs = mysqli_query($link, $sSQL);
			$incident_id = mysqli_insert_id($link);
		}
		else {
			/*$sSQL = "UPDATE `customer_incident` SET 
				`cust_id` = '".$company_id."', 
				`contact_id` = '".$contact_id."', 
				`invoice_no` = '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceNo'])."', 
				`invoice_date` = '".mysqli_real_escape_string($link, $rateInvoiceDate)."', 
				`invoice_due` = '".mysqli_real_escape_string($link, $rateInvoiceDueDate)."', 
				`invoice_currency` = '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceCurrency'])."', 
				`invoice_amount` = '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceAmount'])."', 
				`invoice_interest` = '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceInterest'])."', 
				`invoice_interest_period` = '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceInterestPeriod'])."', 
				`payment_received` = '".mysqli_real_escape_string($link, $ratePaymentDate)."', 
				`status` = '".mysqli_real_escape_string($link, $status)."', 
				`i_agree` = '".mysqli_real_escape_string($link, $rateAgree)."',
				`last_modified_time` = '".$time."'
				WHERE 
				`user_id` = '".$_SESSION['AP_uid']."' AND `incident_id` = '".mysqli_real_escape_string($link, $_REQUEST['incidentID'])."'";*/
			$sSQL = "UPDATE `customer_incident` SET 
				`cust_id` = '".$company_id."', 
				`invoice_date` = '".mysqli_real_escape_string($link, $rateInvoiceDate)."', 
				`invoice_due` = '".mysqli_real_escape_string($link, $rateInvoiceDueDate)."', 
				`payment_received` = '".mysqli_real_escape_string($link, $ratePaymentDate)."', 
				`status` = '".mysqli_real_escape_string($link, $status)."', 
				`i_agree` = '".mysqli_real_escape_string($link, $rateAgree)."',
				`last_modified_time` = '".$time."'
				WHERE 
				`user_id` = '".$_SESSION['AP_uid']."' AND `incident_id` = '".mysqli_real_escape_string($link, $_REQUEST['incidentID'])."'";
				$aRs = mysqli_query($link, $sSQL);
				$incident_id = $_REQUEST['incidentID'];
		}
		
		if ($_REQUEST['rateID'] == "") {
			$sSQL = "INSERT INTO `customer_rating` (`user_id`, `cust_id`, `incident_id`, `rating`, `supplier_type`, `invoice_submission`, `invoice_issued_ok`, `placed_order`, `product_delivered`, `created_time`, `last_modified_time`)
		VALUES
			('".$_SESSION['AP_uid']."', '".$company_id."', '".mysqli_real_escape_string($link, $incident_id)."', '".mysqli_real_escape_string($link, $_REQUEST['rateCompanyRatingVal'])."', '".mysqli_real_escape_string($link, $_REQUEST['rateCompanyUserType'])."', '".mysqli_real_escape_string($link, $rateInvoiceSubmitted)."', '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceConfirm'])."', '".mysqli_real_escape_string($link, $ratePlaceOrder)."', '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceConfirm2'])."',  '".$time."', '".$time."')";
			$aRs = mysqli_query($link, $sSQL);
		}
		else {
			$sSQL = "UPDATE `customer_rating` SET 
				`cust_id` = '".$company_id."',
				`incident_id` = '".mysqli_real_escape_string($link, $incident_id)."', 
				`rating` = '".mysqli_real_escape_string($link, $_REQUEST['rateCompanyRatingVal'])."', 
				`supplier_type` = '".mysqli_real_escape_string($link, $_REQUEST['rateCompanyUserType'])."', 
				`invoice_submission` = '".mysqli_real_escape_string($link, $rateInvoiceSubmitted)."', 
				`invoice_issued_ok` = '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceConfirm'])."', 
				`placed_order` = '".mysqli_real_escape_string($link, $ratePlaceOrder)."', 
				`product_delivered` = '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceConfirm2'])."', 
				`last_modified_time` = '".$time."'
				WHERE 
				`user_id` = '".$_SESSION['AP_uid']."' AND `rat_id` = '".mysqli_real_escape_string($link, $_REQUEST['rateID'])."'";
		}
		if (!$aRs) $error = true;
	}
	else {
		$error = 'You have to select a company from the suggested list. Please try again.';
		/*$sSQL = "INSERT INTO `customer` (`company_name`, `company_uen`, `company_url`, `currency_default`, `active`, `created_time`, `last_modified_time`)
	VALUES
		('".mysqli_real_escape_string($link, strtoupper($_REQUEST['rateCompanyName']))."', '".mysqli_real_escape_string($link, strtoupper($_REQUEST['rateCompanyUEN']))."', '".mysqli_real_escape_string($link, strtolower($_REQUEST['rateCompanyURL']))."', '".mysqli_real_escape_string($link, $_REQUEST['rateInvoiceCurrency'])."', 1, '".$time."', '".$time."')";
		$aRs = mysqli_query($link, $sSQL);
		
		$company_id = mysqli_insert_id($link);*/
		
	}
	dbClose($link);
}
else $error = true;

$pg = 'search_company_main.php';
if ($_REQUEST['rateID'] != "") $pg = 'customer_manage_reviews.php';

if (!$error)  echo "<script>alert('Submitted! Your review will be included in the aggregation of the ratings.'); window.top.location = '".$pg."';</script>";
else {
	if (strlen($error) > 1) echo $error;
	else echo "<script>alert('Submission Failed! Please try again later.'); window.top.location = '".$pg."';</script>";
}
?>