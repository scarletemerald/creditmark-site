<?php 
require_once("../function.php");

$error = false;

if ($_REQUEST['iid'] != "" && $_REQUEST['emailNo'] != "" && $_REQUEST['emailContent'] != "" && $_REQUEST['emailSubject'] != "") {
	$link = dbConnect();
	$time = date("Y-m-d H:i:s");
	
	$sSQL = "SELECT * FROM customer_incident WHERE incident_id = '".mysqli_real_escape_string($link, $_REQUEST['iid'])."' AND user_id = '".mysqli_real_escape_string($link, $_SESSION['AP_uid'])."'";
	$aRs = mysqli_query($link, $sSQL);
	if (mysqli_num_rows($aRs) <= 0) {
		$error = '<script>alert("Case does not exist."); window.parent.location.reload();</script>';
	}
	
	if (!$error) {
		$incidentID = $_REQUEST['iid'];
		$emlno = $_REQUEST['emailNo'];
		$sSQL = "SELECT * FROM customer_incident_eml WHERE incident_id = '".mysqli_real_escape_string($link, $incidentID)."' AND eml_no = '".mysqli_real_escape_string($link, $emlno)."'";
		$aRs = mysqli_query($link, $sSQL);
		if (mysqli_num_rows($aRs) <= 0) {
			$sSQL = "INSERT INTO customer_incident_eml (incident_id, eml_no, subject, content, links, date_created, date_modified) VALUES (
				'".mysqli_real_escape_string($link, $incidentID)."',
				'".mysqli_real_escape_string($link, $emlno)."',
				'".mysqli_real_escape_string($link, $_REQUEST['emailSubject'])."',
				'".mysqli_real_escape_string($link, nl2br($_REQUEST['emailContent']))."',
				'".mysqli_real_escape_string($link, $_REQUEST['links'])."',
				'".$time."', '".$time."')";
		}
		else {
			$sSQL = "UPDATE `customer_incident_eml` SET 
				`subject` = '".mysqli_real_escape_string($link, $_REQUEST['emailSubject'])."', 
				`content` = '".mysqli_real_escape_string($link, nl2br($_REQUEST['emailContent']))."', 
				`links` = '".mysqli_real_escape_string($link, $_REQUEST['links'])."', 
				`date_modified` = '".$time."'
				WHERE incident_id = '".mysqli_real_escape_string($link, $incidentID)."' AND eml_no = '".mysqli_real_escape_string($link, $emlno)."'";
		}
		mysqli_query($link, $sSQL);
	}
	dbClose($link);
}
else $error = true;

if (!$error) echo '<script>alert("Email Message saved!"); window.location="preview_email.php?iid='.$_REQUEST['iid'].'&eml='.$_REQUEST['emailNo'].'";</script>';
else {
	if (strlen($error) > 1) echo $error;
	else echo "Update Failed! Please try again.";
}
?>