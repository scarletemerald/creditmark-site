<?php 
require_once("../function.php");

$error = false;

/*
Array
(
    [incidentID] => 
	[companyID] => 2
    [contactID] => 28
    [invoiceNo] => INV2015-01-02395
    [invoiceDate] => 16-02-2015
    [invoiceDueDate] => 15-03-2015
    [invoiceAmount] => 10000
    [invoiceInterest] => 15.48
    [invoice_interest_period] => 1
    [paymentDate] => 14-03-2015
    [pauseCampaign] => on
    [stripHTML] => true
)

*/

if ($_REQUEST['companyID'] != "" && $_REQUEST['contactID'] != "" && $_REQUEST['invoiceNo'] != "" &&
	$_REQUEST['invoiceDate'] != "" && $_REQUEST['invoiceAmount'] != "" && $_REQUEST['invoiceDueDate'] != "") {
	$link = dbConnect();
	$time = date("Y-m-d H:i:s");
	
	$sSQL = "SELECT * FROM customer WHERE cust_id = '".mysqli_real_escape_string($link, $_REQUEST['companyID'])."'";
	$aRs = mysqli_query($link, $sSQL);
	if (mysqli_num_rows($aRs) <= 0) {
		$error = 'You have to select a company from the suggested list. Please try again.';
	}
	else {
		$coy = mysqli_fetch_assoc($aRs);
		$coy_id = $coy['cust_id'];
		$sSQL = "SELECT * FROM customer_contact WHERE contact_id = '".mysqli_real_escape_string($link, $_REQUEST['contactID'])."' AND user_id = '".$_SESSION['AP_uid']."'";
		$aRs = mysqli_query($link, $sSQL);
		if (mysqli_num_rows($aRs) <= 0) {
			$sSQL = "INSERT INTO customer_contact (user_id, cust_id, contact_name, contact_email, active, created_time, last_modified_time) VALUES (
				'".mysqli_real_escape_string($link, $_SESSION['AP_uid'])."', 
				'".mysqli_real_escape_string($link, $coy_id)."',
				'".mysqli_real_escape_string($link, $_REQUEST['rateContactName'])."', 
				'".mysqli_real_escape_string($link, $_REQUEST['rateContactEmail'])."', 
				'1', '".$time."', '".$time."')";
			$aRs = mysqli_query($link, $sSQL);
			$contact_id = mysqli_insert_id($link);
		}
		else {
			$contact = mysqli_fetch_assoc($aRs);
			$contact_id = $contact['contact_id'];
		}
	}
	
	
	if (!$error) {
		$incidentID = $_REQUEST['incidentID'];
		
		$invoiceDate = date("Y-m-d", strtotime($_REQUEST['invoiceDate']));
		$invoiceDueDate = date("Y-m-d", strtotime($_REQUEST['invoiceDueDate']));
		$paymentDate = '';
		if ($_REQUEST['paymentDate'] != "") $paymentDate = date("Y-m-d", strtotime($_REQUEST['paymentDate']));
		
		$status = 1;
		if ($_REQUEST['paymentDate'] != "") $status = 3;
		else if ($_REQUEST['pauseCampaign'] == "on") $status = 2;
		
		
		if ($incidentID == "") {
			$sSQL = "INSERT INTO `customer_incident` (`user_id`, `cust_id`, `contact_id`, `invoice_no`, `invoice_date`, `invoice_due`, `invoice_currency`, `invoice_amount`, `invoice_interest`, `invoice_interest_period`, `payment_received`, `status`, `created_time`, `last_modified_time`)
VALUES
	('".mysqli_real_escape_string($link, $_SESSION['AP_uid'])."', '".mysqli_real_escape_string($link, $coy_id)."', 
	'".mysqli_real_escape_string($link, $contact_id)."', '".mysqli_real_escape_string($link, $_REQUEST['invoiceNo'])."', 
	'".mysqli_real_escape_string($link, $invoiceDate)."', '".mysqli_real_escape_string($link, $invoiceDueDate)."', 
	'".mysqli_real_escape_string($link, $_REQUEST['invoiceCurrency'])."',
	'".mysqli_real_escape_string($link, number_format($_REQUEST['invoiceAmount'], 2, ".", ""))."', 
	'".mysqli_real_escape_string($link, number_format($_REQUEST['invoiceInterest'], 2, ".", ""))."', 
	'".mysqli_real_escape_string($link, $_REQUEST['invoice_interest_period'])."', '".mysqli_real_escape_string($link, $paymentDate)."', 
	'".$status."', '".$time."', '".$time."')";
			mysqli_query($link, $sSQL);
			$incidentID = mysqli_insert_id($link);
		}
		else {
			$sSQL = "UPDATE `customer_incident` SET 
			`cust_id` = '".mysqli_real_escape_string($link, $coy_id)."', 
			`contact_id` = '".mysqli_real_escape_string($link, $contact_id)."', 
			`invoice_no` = '".mysqli_real_escape_string($link, $_REQUEST['invoiceNo'])."', 
			`invoice_date` = '".mysqli_real_escape_string($link, $invoiceDate)."', 
			`invoice_due` = '".mysqli_real_escape_string($link, $invoiceDueDate)."', 
			`invoice_currency` = '".mysqli_real_escape_string($link, $_REQUEST['invoiceCurrency'])."',
			`invoice_amount` = '".mysqli_real_escape_string($link, number_format($_REQUEST['invoiceAmount'], 2, ".", ""))."', 
			`invoice_interest` = '".mysqli_real_escape_string($link, number_format($_REQUEST['invoiceInterest'], 2, ".", ""))."', 
			`invoice_interest_period` = '".mysqli_real_escape_string($link, $_REQUEST['invoice_interest_period'])."', 
			`payment_received` = '".mysqli_real_escape_string($link, $paymentDate)."', 
			`status` = '".mysqli_real_escape_string($link, $status)."', 
			`last_modified_time` = '".$time."'
			WHERE `user_id` = '".mysqli_real_escape_string($link, $_SESSION['AP_uid'])."' AND 
			`incident_id` = '".mysqli_real_escape_string($link, $incidentID)."'";
			mysqli_query($link, $sSQL);
		}
	}
	dbClose($link);
}
else $error = true;

if (!$error) echo '<script>alert("You have updated the Case successfully!"); window.top.location="incident_manage.php";</script>';
else {
	if (strlen($error) > 1) echo $error;
	else echo "Update Failed! Please try again.";
}
?>