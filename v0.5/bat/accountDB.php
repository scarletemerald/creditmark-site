<?php 
require_once("../function.php");
require_once("../file.php");

$error = false;

if ($_REQUEST['accountUserName'] != "" && $_REQUEST['accountEmail'] != "" && $_REQUEST['accountPassword'] != "" && $_REQUEST['accountCompanyName'] != "") {
	$link = dbConnect();
	
	$photo = '';
	if (isset($_FILES['profilePhoto'])) {
		$photo = fileProcess($_FILES['profilePhoto'], "../upload/photo/");
		if (!is_file("../upload/photo/".$photo)) $photo = "";
	}
	$time = date("Y-m-d H:i:s");
	
	$sSQL = "SELECT * FROM user WHERE user_id = '".mysqli_real_escape_string($link, $_SESSION['AP_uid'])."'";
	$aRs = mysqli_query($link, $sSQL);
	if (mysqli_num_rows($aRs) <= 0) {
		$error = '<script>alert("An unknown error has occurred. Please relogin."); window.parent.location="logout.php";</script>';
	}
	
	if (!$error) {
		$sSQL = "UPDATE `user` SET 
			`user_name` = '".mysqli_real_escape_string($link, $_REQUEST['accountUserName'])."',
			`user_email` = '".mysqli_real_escape_string($link, $_REQUEST['accountEmail'])."', 
			`user_password` = '".mysqli_real_escape_string($link, $_REQUEST['accountPassword'])."',  
			`user_phone` = '".mysqli_real_escape_string($link, $_REQUEST['accountPhone'])."', 
			`user_company` = '".mysqli_real_escape_string($link, $_REQUEST['accountCompanyName'])."', 
			`user_paypal` = '".mysqli_real_escape_string($link, $_REQUEST['bankPayPal'])."', 
			`user_bank_branch` = '".mysqli_real_escape_string($link, $_REQUEST['bankBranchCode'])."', 
			`user_bank_acc` = '".mysqli_real_escape_string($link, $_REQUEST['bankAccount'])."', 
			`user_bank_address` = '".mysqli_real_escape_string($link, $_REQUEST['bankAddress'])."', 
			`user_bank_swift` = '".mysqli_real_escape_string($link, $_REQUEST['bankSwiftCode'])."', 
			`user_bank_iban` = '".mysqli_real_escape_string($link, $_REQUEST['bankIBANCode'])."', ";
		if ($photo != "") {
			$sSQL .= "`user_photo` = '".mysqli_real_escape_string($link, $photo)."', ";
		}
		$sSQL .= "`last_modified_time` = '".$time."'
		WHERE user_id = '".mysqli_real_escape_string($link, $_SESSION['AP_uid'])."'";
		
		$aRs = mysqli_query($link, $sSQL);
		if (!$aRs) $error = true;
	}
	dbClose($link);
}
else $error = true;

if (!$error) echo '<script>alert("Your account is updated."); window.parent.location.reload();</script>';
else {
	if (strlen($error) > 1) echo $error;
	else echo "Update Failed! Please try again.";
}
?>