<?php 
require_once("../function.php");

$error = false;

if ($_REQUEST['accountUserName'] != "" && $_REQUEST['accountEmail'] != "" && $_REQUEST['accountPassword'] != "") {
	$link = dbConnect();
	
	$time = date("Y-m-d H:i:s");
	
	$sSQL = "SELECT * FROM user WHERE user_email = '".mysqli_real_escape_string($link, $_REQUEST['accountEmail'])."'";
	$aRs = mysqli_query($link, $sSQL);
	if (mysqli_num_rows($aRs) > 0) {
		$error = "Your account email already exists. Please login instead.";
	}
	
	if (!$error) {
		$sSQL = "INSERT INTO `user` (
			`user_name`,  
			`user_type`,
			`user_email`, 
			`user_password`, 
			`user_phone`,
			`registered_time`, 
			`last_modified_time` 
		) VALUES (
			'".mysqli_real_escape_string($link, $_REQUEST['accountUserName'])."', 
			'1', 
			'".mysqli_real_escape_string($link, $_REQUEST['accountEmail'])."', 
			'".mysqli_real_escape_string($link, $_REQUEST['accountPassword'])."', 
			'".mysqli_real_escape_string($link, $_REQUEST['accountPhone'])."', 
			'".$time."', 
			'".$time."'
		)";
		
		$aRs = mysqli_query($link, $sSQL);
		if ($aRs) {
			
			$uid = mysqli_insert_id($link);
			
			$_SESSION['AP_login'] = "true";
			$_SESSION['AP_uid'] = $uid;
			$_SESSION['AP_ut'] = 1;
			$_SESSION['AP_eml'] = $_REQUEST['accountEmail'];
		}
		else $error = true;
	}
	dbClose($link);
}
else $error = true;

if (!$error) echo '<script>$("#register-form").hide();$("#register3-form").show();setTimeout(\'$(".modal").modal("hide");$("body").removeClass("modal-open");$(".modal-backdrop").remove();\',3000 );</script>';
else {
	if (strlen($error) > 1) echo $error;
	else echo "Registration Failed! Please try again.";
}
?>