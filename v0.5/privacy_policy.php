<?php include_once('header.php'); ?>
<div class="container">
        <div class="row">
            <div class="grid_12">
                <div class="header2">
                    <h2><strong>Privacy</strong> Policy</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="grid_12">
                <div class="post5">
            <p>Last Updated: 3 August 2016</p>
            <p>1.   This Privacy Policy explains how personal data is collected, used and disclosed by us, in relation to your access and use of our services (the “Services”), which are provided through our website, www.creditmark.org and/or mobile application operated by us (collectively the “Site”). This Privacy Policy applies to all personal data collected or submitted on this Site. This notice is available on the homepage of this Site and at every login page where personal data may be requested. By accessing, using and providing any personal data through our Site, you are expressly consenting to the manner in which we collect, use disclose and transfer personal data as set out below.</p>
            <p>2.   Please note that this Privacy Policy does not govern the collection, use and disclosure of information by companies that we do not control, nor by individuals not employed or managed by us. If you visit a website that we mention or link to, be sure to review its Privacy Policy before providing the site with information.</p>
            <h4>Nature of personal data collected?</h4>
            <p>3.   We limit the collection of personal data to that which we need to administer and improve the Site, to provide our Services to you, and to fulfill any legal and regulatory requirements.</p>
            <p>4.   The categories of personal data that we collect may include, but are not limited to:</p>
                <ul>
                    <li>Contact information like your name, email address, phone number, billing address and/or shipping address to allow us to communicate with you;</li>
                    <li>Credit card information;</li>
                </ul>
            <h4>How personal data is collected</h4>
            <p>5.   We do not require you to provide any personal data in order to have general access to the Site. However, in order to access or use certain information, features or Services at the Site, you may be required to provide personal data. The circumstances under which personal data is primarily collected may include, but are not limited to:</p>
                <ul>
                    <li>When you register an account to access and use the Services;</li>
                    <li>When you purchase a Service;</li>
                    <li>When you make payment for Services rendered; and</li>
                    <li>If you provide us with comments or suggestions, request information about our Services, or contact our Helpdesk Administrators via phone, email or other forms of communication.</li>
                </ul>
            <h4>How personal data is used and disclosed</h4>
            <p>6.   We use and disclose the personal data provided on the Site to perform the Services you request. We limit the purposes for which we use personal data to:</p>
                <ul>
                    <li>Facilitate the provision of our Services to you;</li>
                    <li>Administer and improve our Site;</li>
                    <li>Contact you with information on Services, new Services or products or upcoming events;</li>
                    <li>Respond to your queries, requests and instructions;</li>
                    <li>Troubleshoot i.e. analyse and solve problems arising from your usage of the Services;</li>
                    <li>Comply with legal requirements, the requests of public agencies, law enforcement and regulatory officials, or orders of court;</li>
                    <li>Send you email notifications;</li>
                    <li>Process payments you make via our Site;</li>
                    <li>Fulfill any other purposes for which we have obtained your consent.</li>
                </ul>
            <p>7.   If you would no longer like to receive e-mails from us, please click on the ‘Unsubscribe’ button or let us know by sending an email to our Data Protection Officer. All financial transactions on our Site are handled through our payment service provider who has its own respective privacy policies which you can review at https://www.paypal.com/au/webapps/mpp/ua/privacy-full.</p>
            <p>8.   We will not: (a) without your express consent, provide your personal data to any third parties for the purpose of direct marketing; (b) disclose your personal data except to those to whom disclosure is necessary to effect the purposes of our Site and who are similarly bound to hold your data in confidence; or (c) disclose your personal data unless required to do so by law or in the good faith belief that such preservation or disclosure is reasonably necessary to: (i) comply with legal process; (ii) respond to claims that any of your data or content violates the rights of third parties; or (iii) protect our rights, property, or personal safety and that of our users or the public.</p>
          </div>
          <div class="span6">
            <h4>Access and Correction Obligations</h4>
            <p>9.   You may access, review, update, correct or delete your personal data through your account or by contacting us. We shall respond to an access request as soon as reasonably possible and make a correction as soon as practicable.</p>
            <h4>Retention Obligations</h4>
            <p>10.  We will retain your personal data for as long as is needed to provide the Services to you. We may retain and use personal data for as long as is necessary as required by law or for other legitimate business purposes including complying with our legal obligations, resolving disputes and enforcing our agreements.</p>
            <h4>Cookies</h4>
            <p>11.  We may place a “cookie” in your computer's browser. Cookies are small text files which provide us with information about how often someone visits our Site, and what they do during those visits. Cookies do not themselves contain any personally identifiable information but if you provide such information to us, for example by registering with us, it may be linked to the data stored in the cookie.</p>
            <p>12.  You have the ability to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies, or receive a warning before a cookie is stored, if you prefer. Please refer to your browser’s instructions or help screen to learn more about these functions. If you choose to decline cookies, you may not be able to fully experience the interactive features of this Site or any other web sites that you visit.</p>
            <h4>Protection Obligations</h4>
            <p>13.  The security of personal data, is very important to us. We will maintain reasonable security arrangements in respect of personal data to prevent unauthorised access, collection, use, disclosure, copying, modification, disposal or similar risks. However, despite such reasonable efforts, we cannot guarantee the security of information transmitted through the Internet is impenetrable. We will implement technical and organizational measures in accordance with good industry practices. You are solely responsible for keeping all passwords confidential and configuring the appropriate access control settings.</p>
            <h4>Transfer Obligations </h4>
            <p>14.  Your personal data may be accessed by or transferred to our servers anywhere in the world, as is necessary to facilitate the provision of Services to you. Your personal data may also be accessed by or transferred to our affiliates and data processors and/or intermediaries for the purposes of providing you with the Services. When transferring personal data outside of your jurisdiction, we will take reasonable steps to ensure that the privacy and security of your personal data is accorded a standard of protection comparable to the protection under your local data protection laws. By providing us with personal data, you consent to such transfer and/or access.</p>
            <h4>Changes to Privacy Policy</h4>
            <p>15.  We may revise this Privacy Policy from time to time due to legislative changes, changes in technology or our privacy practices or new uses of personal data not previously disclosed in this Privacy Policy. Material revisions will be effected by posting a notice on the Site prior to the change becoming effective. Your continued use of this Site will indicate your acceptance of those changes. We encourage you to periodically refer to the Privacy Policy for the latest information on our privacy practices.</p>
            <h4>Contact Information</h4>
            <p>Questions or comments related to this Privacy Policy and/or complaints should be submitted to the following designated person:</p>
            <p>Data Protection Officer</p>
            <p>Mr. Mark Laudi</p>
            <p>Address: 137 Cecil Street, Singapore 069537</p>
            <p>Tel: (+65) 6223 2249</p>
            <p>Email: privacy@creditmark.org</p>
                </div>
            </div>
        </div>
    </div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-27668603-3', 'auto');
  ga('send', 'pageview');

</script>
<?php include_once('footer.php'); ?>