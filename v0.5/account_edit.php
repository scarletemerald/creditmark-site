<?php
require_once('function.php');
if ($login) { 
	$user = getUserAccount(); 
?>
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/contact-form.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script>
    <script src="js/TMForm.js"></script>
    <script src="js/modal.js"></script>
    <form id="account-form" class="contact-form" enctype="multipart/form-data">
                <div class="contact-form-loader"></div>
                <fieldset>
                    <div class="row">
                        <div class="grid_7" style="width: 97% !important;">
                            <p>Your Account Details</p>
                            <label class="accountUserName">
                                <a class="hastip" title="Your Name"><input type="text" name="accountUserName" placeholder="Your Name*" data-constraints="@Required" value="<?=$user['user_name'];?>" /></a>
                                <span class="empty-message">*This field is required.</span>
                            </label>
                            
                            <label class="accountJob">
                                <input type="text" name="accountJob" placeholder="Your Job Title*" data-constraints="@Required" value="<?=$user['user_job_title'];?>" />
                                <span class="empty-message">*This field is required.</span>
                            </label>
                            
                            
                            <label class="accountLinkedIn">
                                <a class="hastip" title="Please include http://"><input type="text" name="accountLinkedIn" placeholder="Your LinkedIn Profile URL*" data-constraints="@Required @URL" value="<?=$user['linkedin'];?>" /> /></a>
                                <span class="empty-message">*This field is required.</span>
                                <span class="error-message">*URL is invalid/incorrect format. Please include http://</span>
                            </label>
                            <label class="accountEmail">
                                <input type="text" name="accountEmail" placeholder="Your E-mail*" data-constraints="@Required @Email" value="<?=$user['user_email'];?>" />
                                <span class="empty-message">*This field is required.</span>
                                <span class="error-message">*This doesn't look right. Please check that this is a valid email.</span>
                            </label>
                            <label class="accountPhone">
                                <a class="hastip" title="Enter numbers only - omit +-() symbols."><input type="tel" name="accountPhone" placeholder="Your Preferred Phone Number*" value="<?=$user['user_phone'];?>"
                                       data-constraints="@Required @JustNumbers" /></a>
                                <span class="empty-message">*This field is required.</span>
                                <span class="error-message">*Sorry, this doesn't match the format we require. Please only enter numbers.</span>
                            </label>
                            <label class="accountPassword">
                                <input type="password" name="accountPassword" placeholder="Password*" value="<?=$user['user_password'];?>" data-constraints="@Required" />
                                <span class="empty-message">*This field is required.</span>
                            </label>
                            
                            <label class="accountPassword2">
                                <input type="password" name="accountPassword2" placeholder="Confirm Password*" value="<?=$user['user_password'];?>" data-constraints="@Required @matchPassword" data-contraints-match="accountPassword" />
                                <span class="empty-message">*This field is required.</span>
                                <span class="error-message">*Passwords do not match.</span>
                            </label>
                            
                            
                            <?php if ($_SESSION['AP_ut'] == 1) { ?>
                            <p>Your Company Details</p>
                            <label class="accountCompanyName">
                                <input type="text" name="accountCompanyName" placeholder="Your Company Name*" data-constraints="@Required" value="<?=$user['user_company'];?>" />
                                <span class="empty-message">*This field is required.</span>
                            </label>
                            <?php /*
							<label class="bankPayPal">
                            	<a class="hastip" title="Why do you need my PayPal details? If you provide it, we will display your PayPal email address in CreditMark emails, in order to offer your customers a variety of payment avenues. You can add this later by logging into your CreditMark account. Register for an Paypal account here."> <input type="text" name="bankPayPal" class="@Email" placeholder="Your PayPal Account E-mail" value="<?=$user['user_paypal'];?>" /></a>
                                <span class="error-message">*This is not a valid email.</span>
                            </label>
                            <label class="bankBranchCode">
                                <input type="text" name="bankBranchCode" placeholder="Your Bank Branch Code" value="<?=$user['user_bank_branch'];?>" />
                            </label>
                            <label class="bankAccount">
                                <input type="text" name="bankAccount" placeholder="Your Bank Account Number" value="<?=$user['user_bank_acc'];?>" />
                            </label>
                            <label class="bankAddress">
                                <input type="text" name="bankAddress" placeholder="Your Bank Address" value="<?=$user['user_bank_address'];?>" />
                            </label>
                            <label class="bankSwiftCode">
                                <input type="text" name="bankSwiftCode" placeholder="Your Bank Swift Code" value="<?=$user['user_bank_swift'];?>" />
                            </label>
                            <label class="bankIBANCode">
                                <input type="text" name="bankIBANCode" placeholder="Your Bank IBAN Code" value="<?=$user['user_bank_iban'];?>" />
                            </label>
							*/ ?>
                            
                            <?php } ?>
                            
                            <a class="hastip" title="<strong>Preferred Dimension Ratio:</strong><br />120px (Width) by 150px (Height)<br><br><strong>Preferred File Type:</strong><br>.jpg, .png, .gif"><span class="btn btn-default btn-file">
                                Upload your profile photo <input type="file" name="profilePhoto" placeholder="">
                            </span></a>
                            
                        </div>
                    </div>
                    <div class="contact-form-buttons">
                        <a href="#" data-type="submit" class="btn-default">Update</a>
                    </div>
                </fieldset>
                <div class="modal fade response-message">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">My Account</h4>
                            </div>
                            <div class="modal-body">
                                Sorry, your update was unsuccessful. Please try again. If you are still unsuccessful, please contact us.
                            </div>
                        </div>
                    </div>
                </div>
            </form>
<?php } ?>            