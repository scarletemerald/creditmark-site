<?php 
require_once(dirname(__FILE__)."/config.php");
function dbConnect() {
	$link = mysqli_connect(DBHOST, DBUSER, DBPASS, DBNAME);
	mysqli_query($link, "SET NAMES utf8");
	return $link;
}
function dbConnectPM() {
	$link = mysqli_connect(PM_DBHOST, PM_DBUSER, PM_DBPASS, PM_DBNAME);
	mysqli_query($link, "SET NAMES utf8");
	return $link;
}
function dbClose($link) {
	if (!is_null($link)) {
		mysqli_close($link);
	}
}
function getUserAccount($user_id="") {
	if ($user_id == "") $user_id = $_SESSION['AP_uid'];
	$link = dbConnect();
	$sSQL = "SELECT * FROM user WHERE 
		user_id = '".mysqli_real_escape_string($link, $user_id)."'";
	$aRs = mysqli_query($link, $sSQL);
	$data = array();
	while($row = mysqli_fetch_assoc($aRs)) {
		$data = $row;
	}
	dbClose($link);
	return $data;
}
function getApprovalBuyersAccount() {
	$link = dbConnect();
	$sSQL = "SELECT * FROM user WHERE 
		user_type = 2 AND active = 0";
	$aRs = mysqli_query($link, $sSQL);
	$data = array();
	while($row = mysqli_fetch_assoc($aRs)) {
		$data[] = $row;
	}
	dbClose($link);
	return $data;
}
function getUsersAccount() {
	$link = dbConnect();
	$sSQL = "SELECT * FROM user WHERE 
		user_type IN (1,2) AND active = 1";
	$aRs = mysqli_query($link, $sSQL);
	$data = array();
	while($row = mysqli_fetch_assoc($aRs)) {
		$data[] = $row;
	}
	dbClose($link);
	return $data;
}
function getAdminAccount($user_id="") {
	if ($user_id == "") $user_id = $_SESSION['AP_uid'];
	$link = dbConnect();
	$sSQL = "SELECT * FROM user_admin WHERE 
		user_id = '".mysqli_real_escape_string($link, $user_id)."'";
	$aRs = mysqli_query($link, $sSQL);
	$data = array();
	while($row = mysqli_fetch_assoc($aRs)) {
		$data = $row;
	}
	dbClose($link);
	return $data;
}
function getCustomer($cid="", $user_id="", $limit=0, $conid="", $cond="", $link=NULL, $othercol="") {
	if (!$link) $link = dbConnect();
	if ($user_id == "") $user_id = $_SESSION['AP_uid'];
	else if ($user_id == -1) $user_id = "";
	$sSQL = "SELECT *".$othercol." FROM customer WHERE 1=1";
	if ($cid != "") $sSQL .= " AND cust_id = '".mysqli_real_escape_string($link, $cid)."'";
	if ($cond != "") {
		$sSQL .= $cond;
	}
	if ($limit > 0) $sSQL .= " LIMIT 0, ".$limit;
	//echo $sSQL.'<br />';
	//exit();
	$aRs = mysqli_query($link, $sSQL);
	$data = array();
	while($row = mysqli_fetch_assoc($aRs)) {
		$data[$row['cust_id']] = $row;
		$sSQL2 = "SELECT * FROM customer_contact WHERE 
			user_id = '".mysqli_real_escape_string($link, $user_id)."' 
			AND cust_id = '".mysqli_real_escape_string($link, $row['cust_id'])."'";
		if ($conid != "") $sSQL2 .= " AND contact_id = '".mysqli_real_escape_string($link, $conid)."'";
		//echo $sSQL2.'<br />';
		$aRs2 = mysqli_query($link, $sSQL2);
		while($row2 = mysqli_fetch_assoc($aRs2)) {
			$data[$row['cust_id']]['contact'][] = $row2;
		}
		if ($user_id > 0 && sizeof($data[$row['cust_id']]['contact']) <= 0) unset($data[$row['cust_id']]);
	}
	dbClose($link);
	return $data;
}
function getCustomerByContact($cid="", $user_id="", $limit=0, $conid="", $cond="", $link=NULL, $othercol="") {
	if (!$link) $link = dbConnect();
	if ($user_id == "") $user_id = $_SESSION['AP_uid'];
	else if ($user_id == -1) $user_id = "";
	
	$sSQL2 = "SELECT * FROM customer_contact WHERE 
		user_id = '".mysqli_real_escape_string($link, $user_id)."'";
	
	if ($cid != "") $sSQL2 .= "AND cust_id = '".mysqli_real_escape_string($link, $row['cust_id'])."'";
	if ($conid != "") $sSQL2 .= " AND contact_id = '".mysqli_real_escape_string($link, $conid)."'";
	
	if ($cond != "") {
		$sSQL2 .= $cond;
	}
	
	if ($limit > 0) $sSQL2 .= " LIMIT 0, ".$limit;
	
	//echo $sSQL2.'<br />';
	//exit();
	$aRs = mysqli_query($link, $sSQL2);
	$data = array();
	while($row = mysqli_fetch_assoc($aRs)) {		
		$cid = $row['cust_id'];
		
		$sSQL = "SELECT *".$othercol." FROM customer WHERE 1=1 AND cust_id = '".mysqli_real_escape_string($link, $cid)."'";
		
		//echo $sSQL.'<br />';
		$aRs2 = mysqli_query($link, $sSQL);
		while($row2 = mysqli_fetch_assoc($aRs2)) {
			$data[$row2['cust_id']] = $row2;
		}
		
		$data[$row['cust_id']]['contact'][] = $row;
		
		if ($user_id > 0 && sizeof($row) <= 0) unset($data[$row['cust_id']]);
	}
	dbClose($link);
	return $data;
}
function getCustomerContact($cid="", $user_id=-1, $limit=0, $conid="", $cond="", $link=NULL) {
	if (!$link) $link = dbConnect();
	
	$sSQL2 = "SELECT * FROM customer_contact WHERE 
		user_id = '".mysqli_real_escape_string($link, $user_id)."'";
	if ($cid != "") $sSQL2 .= "	AND cust_id = '".mysqli_real_escape_string($link, $cid)."'";
	if ($conid != "") $sSQL2 .= " AND contact_id = '".mysqli_real_escape_string($link, $conid)."'";
	if ($cond != "") $sSQL2 .= $cond;
	if ($limit > 0) $sSQL2 .= " LIMIT 0, ".$limit;
	//echo $sSQL2.'<br />';
	$aRs2 = mysqli_query($link, $sSQL2);
	while($row2 = mysqli_fetch_assoc($aRs2)) {
		$data[] = $row2;
	}
	dbClose($link);
	return $data;
}
function getCustomerClaims($cid="", $user_id="", $limit=0, $cond="") {
	$link = dbConnect();
	
	$sSQL2 = "SELECT * FROM customer_claim WHERE 1=1";
	if ($user_id != "") $sSQL2 .= " AND user_id = '".mysqli_real_escape_string($link, $user_id)."'";
	if ($cid != "") $sSQL2 .= "	AND cust_id = '".mysqli_real_escape_string($link, $cid)."'";
	if ($cond != "") $sSQL2 .= $cond;
	if ($limit > 0) $sSQL2 .= " LIMIT 0, ".$limit;
	//echo $sSQL2.'<br />';
	$aRs2 = mysqli_query($link, $sSQL2);
	while($row2 = mysqli_fetch_assoc($aRs2)) {
		$data[] = $row2;
	}
	dbClose($link);
	return $data;
}
function getCustomerReviews($cid="", $user_id="", $limit=0, $cond="", $link=NULL) {
	if (!$link) $link = dbConnect();
	
	$sSQL2 = "SELECT * FROM customer_rating WHERE 1=1";
	if ($user_id != "") $sSQL2 .= " AND user_id = '".mysqli_real_escape_string($link, $user_id)."'";
	if ($cid != "") $sSQL2 .= "	AND cust_id = '".mysqli_real_escape_string($link, $cid)."'";
	if ($cond != "") $sSQL2 .= $cond;
	if ($limit > 0) $sSQL2 .= " LIMIT 0, ".$limit;
	//echo $sSQL2.'<br />';
	$aRs2 = mysqli_query($link, $sSQL2);
	while($row2 = mysqli_fetch_assoc($aRs2)) {
		$data[] = $row2;
	}
	dbClose($link);
	return $data;
}
function checkOustandingInvoices($user_id="") {
	if ($user_id == "") $user_id = $_SESSION['AP_uid'];
	$link = dbConnect();
	$sSQL = "SELECT * FROM customer_incident WHERE payment_received IS NULL AND invoice_due < '".date("Y-m-d")."' AND user_id = '".$user_id."'";
	$aRs = mysqli_query($link, $sSQL);
	return mysqli_num_rows($aRs);
}
function getIncident($incident_id="", $user_id="", $limit=0, $cust_id="") {
	$link = dbConnect();
	if ($user_id != -1) $user_id = $_SESSION['AP_uid'];
	$sSQL = "SELECT * FROM customer_incident WHERE status <> -1";
	if ($user_id != -1) $sSQL .= " AND user_id = '".$user_id."'";
	if ($incident_id != "") $sSQL .= " AND incident_id = '".mysqli_real_escape_string($link, $incident_id)."'";
	if ($cust_id != "") $sSQL .= " AND cust_id = '".mysqli_real_escape_string($link, $cust_id)."'";
	$sSQL .= " ORDER BY invoice_due";
	if ($limit > 0) $sSQL .= " LIMIT 0, ".$limit;
	//echo $sSQL.'<br />';
	$aRs = mysqli_query($link, $sSQL);
	$data = array();
	while($row = mysqli_fetch_assoc($aRs)) {
		$iid = $row['incident_id'];
		$data[$iid] = $row;
		$cust = getCustomer($row['cust_id'], -1, "", $row['contact_id']);
		$data[$iid]['customer'] = array_pop($cust);
	}
	dbClose($link);
	return $data;
}
function getIncidentStatus($status) {
	if ($status == 2) $status = "Paused";
	else if ($status == 3) $status = "Closed";
	else $status = "Active";
	return $status;
}
function avgDaysToPay($cid) {
	
	$oCusts = getCustomer($cid, -1);
	if (empty($oCusts)) {
		return '-';
	}
	else {
		$interval = 30;
		$html = array(0=>'0-30 days',
			1=>'31-60 days',
			2=>'61-90 days',
			3=>'91-120 days',
			4=>'121-150 days',
			5=>'151-180 days',
			6=>'>180 days');
		$paidInvoices = 0;
		$daystook = 0;
		$result = array();
		foreach ($html as $val) {
			$result[$val] = 0;
		}
		$gotPaidInvoices = false;
		foreach ($oCusts as $ocust) {
			$oIncs = getIncident("", -1, "", $ocust['cust_id']); // regardless of users
			
			foreach ($oIncs as $oinc) {
				if ($oinc['payment_received'] != "" && $oinc['payment_received'] != "0000-00-00") { // paid
					//echo floor(strtotime($oinc['payment_received']) - strtotime($oinc['invoice_date']));
					$daystook = floor((strtotime($oinc['payment_received']) - strtotime($oinc['invoice_date']))/24/60/60);
					$key = floor(($daystook-1)/$interval);
					if ($key <= 0) $key = 0;
					else if ($key > 6) $key = 6;
					$result[$html[$key]]++;
					$paidInvoices++;
					$gotPaidInvoices = true;
				}
			}
		}
		if ($gotPaidInvoices) {
			
			$rHTML = '
<ul class="barChart">
';
			foreach ($result as $key=>$val) {
				$rHTML .= '
<li>
<div class="label">'.$key.'</div>
<div class="bar"><div class="fill" style="width: '.($val/$paidInvoices*100).'%;"></div></div>
<div class="value">'.$val.'</div>
</li>
';
			}
			$rHTML .= '
</ul>
';
		}
		else {
			$rHTML = '
<ul class="barChart">
';
			foreach ($result as $key=>$val) {
				$rHTML .= '
<li>
<div class="label">'.$key.'</div>
<div class="bar"><div class="fill" style="width: 0%;"></div></div>
<div class="value">'.$val.'</div>
</li>
';
			}
			$rHTML .= '
</ul>
';
		}
		return $rHTML; //$avgval[$cust['company_name']][1] = number_format(round($daystook/$paidInvoices, 2), 2);
	}
}
function generateSerial($num=12) {
	$char = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
				'0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
	for ($x=0; $x<$num; $x++) $serial[] = $char[rand(0, sizeof($char)-1)];
	return implode("", $serial);
}
function sendEmail($to, $subject, $content) {
	//define the receiver of the email
		//$to = 'youraddress@example.com';
	//define the subject of the email
		//$subject = 'Test HTML email'; 
	//create a boundary string. It must be unique 
	//so we use the MD5 algorithm to generate a random hash
	$random_hash = md5(date('r', time())); 
	//define the headers we want passed. Note that they are separated with \r\n
	$headers = "From: service@creditmark.org\r\nReply-To: service@creditmark.org";
	//add boundary string and mime type specification
	$headers .= "\r\nContent-Type: multipart/alternative; boundary=\"PHP-alt-".$random_hash."\""; 
	//define the body of the message.
	$message = '--PHP-alt-'.$random_hash.'
Content-Type: text/plain; charset="iso-8859-1" 
Content-Transfer-Encoding: 7bit
'.strip_tags($content).'

--PHP-alt-'.$random_hash.' 
Content-Type: text/html; charset="iso-8859-1" 
Content-Transfer-Encoding: 7bit

'.$content.'

--PHP-alt-'.$random_hash.'--
';
	//send the email
	//return mail( $to, $subject, $message, $headers );
	return 1;
}
function getEmailTemplate($iid, $emlno) {
	$link = dbConnect();
	$sSQL = "SELECT * FROM customer_incident_eml WHERE incident_id = '".mysqli_real_escape_string($link, $iid)."' AND eml_no = '".mysqli_real_escape_string($link, $emlno)."'";
	$aRs = mysqli_query($link, $sSQL);
	if (mysqli_num_rows($aRs) <= 0) {
		$file = 'template/emailer'.$emlno.'.html';
		if (is_file($file)) {
			$fp = fopen($file, 'r');
			$body = fread($fp, filesize($file));
			$body = explode("--------------------------------------------------BREAK--------------------------------------------------", $body);
			fclose($fp);
			$body = array($body[1], $body[2], $body[3]);
		}
	}
	else {
		$body = mysqli_fetch_assoc($aRs);
		$body = array($body['subject'], $body['content'], $body['links']);
	}
	dbClose($link);
	return $body;
}
function topRated($lmt=10) {
	$link = dbConnect();
	$sSQL = "SELECT cust_id, AVG(rating) AS rate, COUNT(*) AS cnt FROM customer_rating GROUP BY cust_id ORDER by rate DESC, cnt DESC LIMIT 0, ".$lmt;
	$aRs = mysqli_query($link, $sSQL);
	$return = array();
	while ($data = mysqli_fetch_assoc($aRs)) {
		$return[] = $data['cust_id'];
	}
	dbClose($link);
	return $return;
}
function mostPopular($lmt=10) {
	$link = dbConnect();
	$sSQL = "SELECT id, COUNT(*) AS cnt FROM track GROUP BY id ORDER by cnt DESC, created_time DESC LIMIT 0, ".$lmt;
	$aRs = mysqli_query($link, $sSQL);
	$return = array();
	while ($data = mysqli_fetch_assoc($aRs)) {
		// No Reviews Yet
		if (calcRating($data['id']) <= -1) // No Rating
			$return[] = $data['id'];
	}
	dbClose($link);
	return $return;
}
function calcRating($coy) {
	$link = dbConnect();
	$sSQL = "SELECT IF(AVG(rating) IS NULL,-1,AVG(rating)) AS rate FROM customer_rating WHERE cust_id = '".mysqli_real_escape_string($link, $coy)."'";
	$aRs = mysqli_query($link, $sSQL);
	$data = mysqli_fetch_assoc($aRs);
	dbClose($link);
	return ceil($data['rate']*2)/2;
}
function rating($num, $x=2) {
	$title = array('I am in a payment dispute with this customer', 'I am in a payment dispute with this customer', "This customer usually pays late and doesn't respond to enquiries", 'This customer usually pays late but responds to enquiries', 'This customer sometimes pays late but responds to enquiries', 'This customer always pays on time and is a pleasure to deal with');
	$score = $num;	
	if ($score >= 0) {
		for ($i=1; $i<=$num; $i++) {
			echo '<i class="fa fa-star fa-'.$x.'x maincolor" title="'.$title[floor($score)].'"></i>';
		}
		if ($i > $num && $num > ($i-1)) {
			echo '<i class="fa fa-star-half-o fa-'.$x.'x maincolor" title="'.$title[floor($score)].'"></i>';
			$num++;
		}
		for ($j=5-$num; $j>0; $j--) {
			echo '<i class="fa fa-star-o fa-'.$x.'x maincolor" title="'.$title[floor($score)].'"></i>';
		}
	}
	else if ($_SESSION['AP_ut'] == 1 || $_SESSION['AP_ut'] == "") echo '<div style="display: inline-block; vertical-align: middle; font-size: xx-small; font-style: italic;">Be the first to rate this company.</div>';
	else echo '<div style="display: inline-block; vertical-align: middle; font-size: xx-small; font-style: italic;">No ratings yet</div>';
}

function getSearch() {
	$link = dbConnect();
	$sSQL = "SELECT COUNT(*) AS cnt FROM track WHERE type = 'PAGE' and user_id = '".mysqli_real_escape_string($link, $_SESSION['AP_uid'])."' AND user_type = '".mysqli_real_escape_string($link, $_SESSION['AP_ut'])."' GROUP BY id";
	$aRs = mysqli_query($link, $sSQL);
	$data = mysqli_num_rows($aRs);
	dbClose($link);
	return $data;
}
function getCountry($cty) {
	$link = dbConnect();
	$sSQL = "SELECT country FROM country WHERE country_code = '".mysqli_real_escape_string($link, $cty)."'";
	$aRs = mysqli_query($link, $sSQL);
	$data = $cty;
	while ($row = mysqli_fetch_assoc($aRs)) {
		$data = $row['country'];
	}
	dbClose($link);
	return $data;
}
function track($type, $id, $link) {
	$sSQL = "INSERT INTO `track` (`type`, `id`, `user_id`, `user_type`, `created_time`) VALUES
	('".mysqli_real_escape_string($link, $type)."', 
	'".mysqli_real_escape_string($link, $id)."', 
	'".mysqli_real_escape_string($link, $_SESSION['AP_uid'])."', 
	'".mysqli_real_escape_string($link, $_SESSION['AP_ut'])."', 
	 '".date("Y-m-d H:i:s")."')";
	mysqli_query($link, $sSQL);
	dbClose($link);
}
function debug($var) {
	if (DEBUG_MODE) {
		echo '<pre style="font-family: Courier; font-size: 9px; background-color: #FFF; border: 1px solid #000; padding: 15px; color: #000; word-wrap: break-word;">';
		print_r($var);
		echo '</pre>';
	}
}

$headerclass = 'common-header';
if (strpos($_SERVER['PHP_SELF'], "index.php") !== false) $headerclass = '__absolute'; // Home Page

$maincontentclass = '';
if (strpos($_SERVER['PHP_SELF'], "index.php") === false) $maincontentclass = ' class="common-content"';

$login = ($_SESSION['AP_login'] && $_SESSION['AP_uid'] > 0);

if (!$login) {
	$config   = dirname(__FILE__).'/hybridauth-2.6.0/hybridauth/config.php';
	require_once( dirname(__FILE__)."/hybridauth-2.6.0/hybridauth/Hybrid/Auth.php" );
	try{
		$hybridauth = new Hybrid_Auth( $config );
	}
	catch( Exception $e ){
		echo "Ooophs, we got an error: " . $e->getMessage();
	}

	if (!$login && isset( $_GET["connected_with"] ) && $hybridauth->isConnectedWith( $_GET["connected_with"] ) ){
		$provider = $_GET["connected_with"];
		$adapter = $hybridauth->getAdapter( $provider );
		$user_data = $adapter->getUserProfile();
	}
}
?>