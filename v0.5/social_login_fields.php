<?php
/* 
FB User Data
(
    [identifier] => 940004132676545
    [webSiteURL] => 
    [profileURL] => https://www.facebook.com/app_scoped_user_id/940004132676545/
    [photoURL] => https://graph.facebook.com/940004132676545/picture?width=150&height=150
    [displayName] => Howie Yan
    [description] => 
    [firstName] => Howie
    [lastName] => Yan
    [gender] => male
    [language] => en_US
    [age] => 
    [birthDay] => 1
    [birthMonth] => 10
    [birthYear] => 1981
    [email] => yan.howie@gmail.com
    [emailVerified] => yan.howie@gmail.com
    [phone] => 
    [address] => 
    [country] => 
    [region] => Singapore
    [city] => 
    [zip] => 
    [username] => 
    [coverInfoURL] => https://graph.facebook.com/940004132676545?fields=cover
)

LinkedIn User Data
(
    [identifier] => SxixsJyXXA
    [webSiteURL] => 
    [profileURL] => https://www.linkedin.com/in/howieyan
    [photoURL] => https://media.licdn.com/mpr/mprx/0_hqAXN0ArC5Cq1J2AhrlUNgr-CLlc1y2AC1LUNylCgh5F24Rlu-3HVpLuuwAHKUDj3c1VURfZS2ZU
    [displayName] => Howie Yan
    [description] => A Web Development Professional with years of experience developing complex web based applications and websites. 

Possesses strong technical skills and capability to lead software development team as well as being a compatible team player. 

Capable of managing Web Projects from conceptualization to completion with strong deadline sensitivity.

Strong understanding in Internet Space including Internet Advertising.

Specialties: PHP, AJAX, JSON, Open Social, jQuery, C#, Web 2.0, ASP, JSP, ActionScript, Flex, Adobe Flash, Adobe Photoshop, Adobe Dreamweaver, Adobe Premiere, MSSQL, mySQL, Windows Server, Linux, Apache, IIS, Javascript, HTML, CSS, DHTML etc.
    [firstName] => Howie
    [lastName] => Yan
    [gender] => 
    [language] => 
    [age] => 
    [birthDay] => 1
    [birthMonth] => 10
    [birthYear] => 1981
    [email] => yan.howie@gmail.com
    [emailVerified] => yan.howie@gmail.com
    [phone] => 
    [address] => 
    [country] => 
    [region] => 
    [city] => 
    [zip] => 
)

Twitter User Data
(
    [identifier] => 365777759
    [webSiteURL] => 
    [profileURL] => http://twitter.com/HowieY
    [photoURL] => http://pbs.twimg.com/profile_images/1522930224/13373-685970-20090927062741.jpg
    [displayName] => HowieY
    [description] => I am who I am
    [firstName] => Howie
    [lastName] => 
    [gender] => 
    [language] => 
    [age] => 
    [birthDay] => 
    [birthMonth] => 
    [birthYear] => 
    [email] => 
    [emailVerified] => 
    [phone] => 
    [address] => 
    [country] => 
    [region] => Singapore
    [city] => 
    [zip] => 
)

*/
?>