function include(url){
  document.write('<script src="'+url+'"></script>');
  return false ;
}

/* cookie.JS
========================================================*/
include('js/jquery.cookie.js');


/* DEVICE.JS
========================================================*/
include('js/device.min.js');

/* Stick up menu
========================================================*/
include('js/tmstickup.js');
$(window).load(function() { 
  if ($('html').hasClass('desktop')) {
      $('#stuck_container').TMStickUp({
      })
  }  
});

/* Easing library
========================================================*/
include('js/jquery.easing.1.3.js');


/* ToTop
========================================================*/
include('js/jquery.ui.totop.js');
$(function () {   
  $().UItoTop({ easingType: 'easeOutQuart' });
});



/* DEVICE.JS AND SMOOTH SCROLLIG
========================================================*/
/*include('js/jquery.mousewheel.min.js');
include('js/jquery.simplr.smoothscroll.min.js');
$(function () { 
  if ($('html').hasClass('desktop')) {
      $.srSmoothscroll({
        step:10,
        speed:10
      });
  }   
});*/

function openModal(url) {
	$('#modalcontent').attr('src', url);
	if (url.indexOf('preview_email') > -1) 
		$('.modal .modal-title').html('Preview Email');
	else if (url.indexOf('incident_import') > -1) 
		$('.modal .modal-title').html('Import Cases');
	else if (url.indexOf('incident_edit') > -1) 
		$('.modal .modal-title').html('Case');
	else if (url.indexOf('customer_edit') > -1) 
		$('.modal .modal-title').html('Customer Contact');
	else if (url.indexOf('company_page_edi') > -1)  {
		$('.modal .modal-title').html('Company Profile Page Edit');
		$('.modal .modal-dialog').addClass('modal-lg');
	}
	else if (url.indexOf('company_page') > -1)  {
		$('.modal .modal-title').html('Company Profile Page');
		$('.modal .modal-dialog').addClass('modal-lg');
	}
	else if (url.indexOf('search_company') > -1)  {
		$('.modal .modal-title').html('Search Results');
		$('.modal .modal-dialog').addClass('modal-lg');
	}
	$('.modal').modal();
	
	$('.modal').on('hidden.bs.modal', function (e) {
		$('#modalcontent').attr('src', '');
	});
}
function claimPage(cid) {
	if (confirm('Are you sure you wish to claim this company page?')) {
		$.post('company_claim.php', { 'cid': cid }, function(d) {
			alert(d);
		});
	}
}

/* Copyright Year
========================================================*/
var currentYear = (new Date).getFullYear();
$(document).ready(function() {
  $("#copyright-year").text( (new Date).getFullYear() );
});


/* Superfish menu
========================================================*/
include('js/superfish.js');
include('js/jquery.mobilemenu.js');

/* ToolTipsy
========================================================*/
include('js/tooltipsy.min.js');
$(document).ready(function() { 

  $('.hastip').tooltipsy(); 
  
  $('select').change(function() {
	  if ($("option:selected", this).val() == "") {
	  	$(this).removeClass('white');
	  }
	  else $(this).addClass('white');
  });
  
});


/* Orientation tablet fix
========================================================*/
$(function(){
// IPad/IPhone
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
	// Menu Android
	if(window.orientation!=undefined){
  var regM = /ipod|ipad|iphone/gi,
   result = ua.match(regM)
  if(!result) {
   $('.sf-menu li').each(function(){
    if($(">ul", this)[0]){
     $(">a", this).toggle(
      function(){
       return false;
      },
      function(){
       window.location.href = $(this).attr("href");
      }
     );
    } 
   })
  }
 }
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')