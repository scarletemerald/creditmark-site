<?php
require_once('function.php');
$noresult = true;
if ($_REQUEST['cid'] != "") {
	$link = dbConnect();
	$cid = base64_decode($_REQUEST['cid']);
	$coys = getCustomer("", -1, 1, "", " AND active = 1 AND cust_id RLIKE '".mysqli_real_escape_string($link, $cid)."'", $link);
	$coy = array();
	if (sizeof($coys) > 0) {
		$noresult = false;
		foreach ($coys as $c) $coy = $c;
	}
}

if ($login && $_SESSION['AP_ut'] == 1) {
	$reviews = getCustomerReviews("", $_SESSION['AP_uid']);
	$searchno = sizeof($reviews)/5 - getSearch();
}

$mycompany = ($_SESSION['AP_ut'] == 2 && $coy['user_id'] == $_SESSION['AP_uid']);
?>
<link rel="stylesheet" href="css/grid.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/contact-form.css">
<script src="js/jquery.js"></script>
<script src="js/jquery-migrate-1.2.1.js"></script>
<script src="js/script.js"></script>
<script src="js/TMForm.js"></script>
<script src="js/modal.js"></script>
<?php if ($noresult) { ?>
<h5>The page you had requested is not available.<br />Please try again.</h5>
<?php } else if ($login && $_SESSION['AP_ut'] == 1 && isset($searchno) && $searchno <= 0) { ?>
<h5>You will need to <a href="customer_manage_reviews.php?add=1&cid=<?=base64_encode($coy['cust_id']);?>" target="_top">add five (5) reviews</a> first before you can view the results.</h5>
<?php } else { ?>
<?php

if (!$login) { 
	$writeReviewAction = "<button type=\"button\" onClick=\"alert('You have to login as a Supplier to write a review.'); window.top.location='login.php';\" class=\"btn-smallest\">Write Review</button>";
	$claimCompanyAction = "<button type=\"button\" onClick=\"alert('You have to login as a Buyer to claim a company page.'); window.top.location='login.php';\" class=\"btn-smallest\">Claim Company</button>";
}
else if ($_SESSION['AP_ut'] == 1) { // Supplier
	$writeReviewAction = "<button type=\"button\" onClick=\"openModal('write_review.php?cid=".base64_encode($coy['cust_id'])."');\" class=\"btn-smallest\">Write Review</button>";
	$claimCompanyAction = "";
}
else if ($_SESSION['AP_ut'] == 2) { // Buyer
	$writeReviewAction = "";
	$claimCompanyAction = "<button type=\"button\" onClick=\"claimPage('".base64_encode($coy['cust_id'])."');\" class=\"btn-smallest\">Claim Company</button>";
}

if ($coy['user_id'] > 0) {
	$claimCompanyAction = '<span class="claimed">Claimed</span>';
}
?>
<div style="float: right; text-align: right;">
<?php $rate = calcRating($coy['cust_id']); ?>
<?=rating($rate, 3); ?><br />
<?=$writeReviewAction;?>
<?php if ($rate >= 4) { ?>
<br /><img src="images/badge-rated.png" style="width: 80px; float: none; border: 0;" title="Recommended for Prompt Payment Discount. Reward this buyer for their track record in paying promptly by offering a discount." />
<?php } ?>
</div>
<h2>
	<?php if (is_file('upload/photo/'.$coy['company_logo'])) { ?>
    <?php if ($coy['company_url'] != "") { ?>
    <a href="<?=$coy['company_url'];?>" target="_blank">
    <?php } ?>
		<img src="upload/photo/<?=$coy['company_logo'];?>" style="vertical-align: middle; float: none; border: 0; max-height: 150px; max-width: 150px; margin-right: 15px;" />
		<?php if ($coy['company_url'] != "") { ?>
</a>
<?php } ?>
	<?php } ?>
<div style="display: inline-block; vertical-align: middle; max-width: 50%;"><?php if ($coy['company_url'] != "") { ?>
    <a href="<?=$coy['company_url'];?>" target="_blank">
    <?php } ?><?=$coy['company_name'];?><?php if ($coy['company_url'] != "") { ?>
</a>
<?php } ?> <?=$claimCompanyAction;?>
<?php if ($mycompany) { ?>
<a class="btn-medium" style="width: 100px; padding: 10px;" onclick="openModal('company_page_edit.php?cid=<?=base64_encode($coy['cust_id']);?>');">edit</a>
<?php } ?></div>
</h2>
<br />
<table>
    <colgroup>
        <col width="200" />
        <col />
    </colgroup>
    <tbody>
    <tr>
        <td><strong>UEN:</strong></td>
        <td><?=$coy['company_uen'];?></td>
    </tr>
    <tr>
        <td><strong>Website:</strong></td>
        <td><a href="<?=$coy['company_url'];?>" target="_blank"><?=$coy['company_url'];?></a></td>
    </tr>
    <?php if ($login && (in_array($_SESSION['AP_ut'], array(1,3,9999)) || $mycompany)) { ?>
    <tr>
        <td><strong>Currency:</strong></td>
        <td><?=$coy['currency_default'];?></td>
    </tr>
    <tr>
        <td><strong>Standard Credit Terms:</strong></td>
        <td><?=$coy['standard_credit_terms'];?> Days</td>
    </tr>
    <tr>
        <td><strong>Days to Pay:</strong></td>
        <td><?=avgDaysToPay($coy['cust_id']);?></td>
    </tr>
    <tr>
        <td><strong>Procurement Policies:</strong></td>
        <td><?=nl2br($coy['company_procurement_policies']);?></td>
    </tr>
    <tr>
        <td><strong>Additional Information:</strong></td>
        <td><?=nl2br($coy['company_comment']);?></td>
    </tr>
    <?php } ?>
    </tbody>
</table>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-27668603-3', 'auto');
  ga('send', 'pageview');

</script>
<?php 
$_REQUEST['t'] = "PAGE";
$_REQUEST['i'] = $coy['cust_id'];
include_once('track.php'); ?>
<?php } ?>