<?php
require_once('function.php');
$noresult = true;
if ($login && $_REQUEST['cid'] != "") {
	$link = dbConnect();
	$cid = base64_decode($_REQUEST['cid']);
	$coys = getCustomer("", -1, 1, "", " AND active = 1 AND cust_id = '".mysqli_real_escape_string($link, $cid)."'", $link);
	$coy = array();
	if (sizeof($coys) > 0) {
		$noresult = false;
		foreach ($coys as $c) $coy = $c;
	}
}
$mycompany = ($_SESSION['AP_ut'] == 2 && $coy['user_id'] == $_SESSION['AP_uid']);
if (!$mycompany) $noresult = true;
//debug($coy);
?>
<link rel="stylesheet" href="css/grid.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/contact-form.css">
<script src="js/jquery.js"></script>
<script src="js/jquery-migrate-1.2.1.js"></script>
<script src="js/script.js"></script>
<script src="js/TMForm.js"></script>
<script src="js/modal.js"></script>
<?php if ($noresult) { ?>
<h5>The page you had requested is not available.<br />Please try again.</h5>
<?php } else { ?>
<form id="companyedit-form" class="contact-form" enctype="multipart/form-data">
	<input type="hidden" name="editCompanyID" value="<?=$coy['cust_id'];?>" />
    <div class="contact-form-loader"></div>
    <fieldset>
        <div class="row" style="margin-top: 0px; margin-bottom: 12px;">
            <div class="grid_12">
            	<br />
                <label class="editCompanyName">
                    <input type="text" name="editCompanyName" id="editCompanyName" class="coysearch" placeholder="Company Name*" data-constraints="@Required" value="<?=$coy['company_name'];?>" />
                    <span class="empty-message">*This field is required.</span>
                </label>
                <label class="editCompanyUEN">
                    <input type="text" name="editCompanyUEN" placeholder="Business Registered Number*" data-constraints="@Required" value="<?=$coy['company_uen'];?>" />
                    <span class="empty-message">*This field is required.</span>
                </label>
                <label class="editCompanyURL">
                    <a class="hastip" title="Make absolutely certain that the company you are reviewing and the web address match! Please include http://"><input type="text" name="editCompanyURL" id="editCompanyURL" placeholder="Company URL*" data-constraints="@URL" value="<?=$coy['company_url'];?>" /></a>
                    <span class="error-message">*Sorry, the URL is in an incorrect format. It should look like this: http://www.company.com or http://prefix.company.com.</span>
                </label>
                <br />
                <label class="editDefaultCurrency">
                    <select name="editDefaultCurrency" id="editDefaultCurrency" class="select-menu" data-constraints="@Required"<?php if ($coy['currency_default'] != "") echo ' style="color: #fff;"';?>>
                        <option value="">Default Currency*</option> 
                        <?php foreach ($currencyArr as $cur) { ?>
                        <option value="<?=$cur;?>"<?php if ($coy['currency_default']==$cur) echo ' selected';?>><?=$cur;?></option>
                        <?php } ?>
                    </select>
                    <span class="empty-message">*This field is required.</span>
                </label>
                <label class="editCreditTerms">
                    <select name="editCreditTerms" id="editCreditTerms" class="select-menu" data-constraints="@Required"<?php if ($coy['standard_credit_terms'] != "") echo ' style="color: #fff;"';?>>
                        <option value="">Standard Credit Terms*</option> 
                        <optgroup label="Standard Days">
                            <option value="14"<?php if ($coy['standard_credit_terms']==14) echo ' selected';?>>14 Days</option>
                            <option value="30"<?php if ($coy['standard_credit_terms']==30) echo ' selected';?>>30 Days</option>
                            <option value="60"<?php if ($coy['standard_credit_terms']==60) echo ' selected';?>>60 Days</option>
                            <option value="90"<?php if ($coy['standard_credit_terms']==90) echo ' selected';?>>90 Days</option>
                            <option value="120"<?php if ($coy['standard_credit_terms']==120) echo ' selected';?>>120 Days</option>
                        </optgroup>
                        <optgroup label="Other Days">
                        <?php for ($i=1; $i<=365; $i++) { 
                            if (!in_array($i, array(14, 30, 60, 90, 120))) {
                        ?>
                        <option value="<?=$i;?>"<?php if ($coy['standard_credit_terms']==$i) echo ' selected';?>><?=$i;?> Days</option>
                        <?php }
                        } ?>
                        </optgroup>
                    </select>
                    <span class="empty-message">*This field is required.</span>
                </label>
                
                <label class="editProcurementPolicies" style="height: 200px;">
                    <textarea name="editProcurementPolicies" placeholder="Procurement Policies"><?=$coy['company_procurement_policies'];?></textarea>
                </label>
                
                <label class="editAdditionalInfo" style="height: 200px;">
                    <textarea name="editAdditionalInfo" placeholder="Additional Information"><?=$coy['company_comment'];?></textarea>
                </label>
                
                <a class="hastip" title="<strong>Preferred Dimension Ratio:</strong><br />120px (Width) by 150px (Height)<br><br><strong>Preferred File Type:</strong><br>.jpg, .png, .gif"><span class="btn btn-default btn-file">
                    Upload your company logo <input type="file" name="editCompanyLogo" placeholder="">
                </span></a>
                
            </div>
         </div>
        <div class="contact-form-buttons">
            <a href="#" data-type="submit" class="btn-default">Update</a>
        </div>
    </fieldset>
    <div class="modal fade response-message">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Register</h4>
                </div>
                <div class="modal-body">
                    Sorry, your registration was unsuccessful. Please try again. If you are still unsuccessful, please contact us.
                </div>
            </div>
        </div>
    </div>
</form>
<?php } ?>