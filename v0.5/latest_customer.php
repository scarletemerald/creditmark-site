<?php if ($login) { 
$custs = getCustomerContact("", "", 5, "", " AND active = 1 ORDER BY contact_id DESC"); 
?>
          <table class="table data">
        	<thead>
            	<tr>
                    <td>Contact</td>
                    <td width="100">No of Case(s)</td>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($custs as $cust) {
				$coy = getCustomer($cust['cust_id']);
				$coy = array_pop($coy);
				$incidents = getIncident("", "", "", $cust['cust_id']); ?>
            	<tr>
                    <td><strong><?=$cust['contact_name'];?>, <span style="font-style: italic;"><?=$coy['company_name'];?></span></strong><br />
                    <?=$cust['contact_email'];?></td>
                    <td><?=(sizeof($incidents) <= 0)?'<a href="incident_manage.php?add=1&cid='.$cust['cust_id'].'">Add Case</a>':'<a href="incident_manage.php?cid='.$cust['cust_id'].'">'.sizeof($incidents).'</a>';?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="push-right"><a href="incident_manage.php">See All</a></div>
<?php
}
else {
	header("Location: index.php");
}
?>