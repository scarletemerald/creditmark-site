<?php
require_once('function.php');
if ($login && $_GET['iid'] != "" && $_GET['eml'] != "") { 
	$incidents = getIncident($_GET['iid']);
	$incident = array_pop($incidents);
	
	$user = getUserAccount($incident['user_id']);
	$company = getCustomer($incident['cust_id'], $incident['user_id'], "", $incident['contact_id']);
	$company = array_pop($company);
	$contact = array_pop($company['contact']);
	
	$template = getEmailTemplate($_GET['iid'], $_GET['eml']);
	
	if ($template[0] != "") {
	
		$body1 = $template[1];
		$body1 = str_replace("[CustomerName1]", $contact['contact_name'], $body1);
		$body1 = str_replace("[UserCompany]", $user['user_company'], $body1);
		$body1 = str_replace("[InvoiceNumber]", $incident['invoice_no'], $body1);
		$body1 = str_replace("[InvoiceCurrency]", $incident['invoice_currency'], $body1);
		$body1 = str_replace("[InvoiceAmount]", number_format($incident['invoice_amount'], 2, ".", ","), $body1);
		$body1 = str_replace("[InvoiceDate]", date("d M Y", strtotime($incident['invoice_due'])), $body1);
		$body1 = str_replace("[InterestRate]", $incident['invoice_interest']."%", $body1);
		$body1 = str_replace("[InterestTime]", $incident['invoice_interest_period']." day(s)", $body1);
		if ($incident['invoice_interest_period'] <= 30) {
			$interest = $incident['invoice_amount']*($incident['invoice_interest']/100)*30/$incident['invoice_interest_period'];
		}
		else $interest = 0;
		$body1 = str_replace("[InvoiceInterestNow]", number_format($interest, 2, ".", ","), $body1);
		$body1 = str_replace("[InvoiceAmountNow]", number_format($incident['invoice_amount']+$interest, 2, ".", ","), $body1);
		$body1 = str_replace("[UsersEmailAddress]", $user['user_email'], $body1);
		$body1 = str_replace("[UserTelephone]", $user['user_phone'], $body1);
		$body1 = str_replace("[UserName]", $user['user_name'], $body1);
		
?>
        <link rel="stylesheet" href="css/grid.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/contact-form.css">
        <script src="js/jquery.js"></script>
        <script src="js/jquery-migrate-1.2.1.js"></script>
        <script src="js/TMForm.js"></script>
        <script src="js/modal.js"></script>
        <form id="incident-email-form" class="contact-form" enctype="multipart/form-data">
           <input type="hidden" name="iid" value="<?=$incident['incident_id'];?>" />
           <input type="hidden" name="emailNo" value="<?=$_GET['eml'];?>" />
           <textarea name="links" style="display: none;"><?=$template[2];?></textarea>
            <div class="contact-form-loader"></div>
            <fieldset>
                <div class="row">
                    <div class="grid_7" style="width: 97% !important;">
                        <p>
                        <?php
                        echo '<strong>From: </strong>CreditMark &lt;service@creditmark.org&gt;<br />';
                        echo '<strong>To: </strong>'.$contact['contact_name'].' &lt;'.$contact['contact_email'].'&gt;<br />';
                        echo '<strong>Subject: </strong>'.$template[0].'<br />';
                        ?>
                        </p>
                        <label class="emailSubject">
                            <input type="text" name="emailSubject" data-constraints="@Required" id="emailSubject" placeholde="Email Subject" value="<?=$template[0];?>" />
                            <span class="empty-message">*This field is required.</span>
                        </label>
                        <label class="emailContent" style="height: auto;">
                            <textarea name="emailContent" data-constraints="@Required" id="emailContent" placeholde="Email Content" style="height: 200px;"><?=strip_tags(preg_replace('/<br\s*\/?>/i', "\n", preg_replace('#(\r|\r\n|\n)#', "", $body1)));?></textarea>
                            <span class="empty-message">*This field is required.</span>
                        </label>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="contact-form-buttons">
                    <a href="preview_email.php?iid=<?=$incident['incident_id'];?>&eml=<?=$_GET['eml'];?>" data-type="button" class="btn-default">Cancel</a><a href="#" data-type="submit" class="btn-default">Update</a>
                </div>
            </fieldset>
            <div class="modal fade response-message">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">My Account</h4>
                        </div>
                        <div class="modal-body">
                            Failed to Update. Please try again.
                        </div>
                    </div>
                </div>
            </div>
        </form>
<?php }
} ?>