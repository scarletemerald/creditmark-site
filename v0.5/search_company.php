<?php
require_once('function.php');
$noresult = true;
?>
<link rel="stylesheet" href="css/grid.css">
<link rel="stylesheet" href="css/style.css">
<script src="js/jquery.js"></script>
<script src="js/jquery-migrate-1.2.1.js"></script>
<script src="js/script.js"></script>
<script src="js/modal.js"></script>
<?php
if ($_REQUEST['cty'] == "") { ?>
<h5>Please select a country first.<br /><br />Please try again.</h5>
<?php 
exit();
}
else if ($_REQUEST['query'] == "") { ?>
<h5>Please key in something in the search box.<br /><br />Please try again.</h5>
<?php 
exit();
} else {
	if (strlen($_REQUEST['query']) < 3) { ?>
<h5>Enter your corporate customer's full name.<br /><br />Please try again.</h5>		
<?php 
		exit();
	} else {
		$link = dbConnect();
		$coys = getCustomer("", -1, 100, "", " AND active = 1 AND company_country = '".mysqli_real_escape_string($link, $_REQUEST['cty'])."' AND LOWER(company_name) LIKE LOWER('%".mysqli_real_escape_string($link, $_REQUEST['query'])."%')", $link);
		if (sizeof($coys) > 0) $noresult = false;
	}
	if ($noresult) { ?>
<h5>There is no result for your search keyword of "<strong><?=htmlentities($_REQUEST['query']);?></strong>".<br /><br />Please try again.</h5>
<?php } else { 
?>
Search result(s) for "<strong><?=htmlentities($_REQUEST['query']);?></strong>":
<?php if (sizeof($coys) >= 200) echo '<div style="float: right; color: #1abc9c; font-size: xx-small; font-style: italic; vertical-align: middle;"><i class="fa fa-exclamation-circle fa-3x"></i> Your search is resulting in a large amount of results. You might want to change your search keyword(s) to something more precise.</div>'; ?>
<table class="records">
    <colgroup>
        <col />
        <col width="150" />
        <col width="250" />
        <col width="200" />
    </colgroup>
    <thead>
    <tr>
        <td>Company</td>
        <td>Rating</td>
        <td>Days to Pay</td>
        <td>&nbsp;</td>
    </tr>
    </thead>
    <?php foreach($coys as $c) { 
	
		$_REQUEST['t'] = "SEARCH";
		$_REQUEST['i'] = $c['cust_id'];
		include('track.php');
	
		if (!$login) { 
			$writeReviewAction = "<button type=\"button\" onClick=\"alert('You have to login as a Supplier to write a review.'); window.top.location='login.php';\" class=\"btn-smallest\">Write Review</button>";
			$claimCompanyAction = "<button type=\"button\" onClick=\"alert('You have to login as a Buyer to claim a company page.'); window.top.location='login.php';\" class=\"btn-smallest\">Claim Company</button>";
		}
		else if ($_SESSION['AP_ut'] == 1) { // Supplier
			$writeReviewAction = "<button type=\"button\" onClick=\"window.location = 'write_review.php?cid=".base64_encode($c['cust_id'])."';\" class=\"btn-smallest\">Write Review</button>";
			$claimCompanyAction = "";
		}
		else if ($_SESSION['AP_ut'] == 2) { // Buyer
			$writeReviewAction = "";
			$claimCompanyAction = "<button type=\"button\" onClick=\"claimPage('".base64_encode($c['cust_id'])."');\" class=\"btn-smallest\">Claim Company</button>";
		}
		if ($c['user_id'] > 0) {
			$claimCompanyAction = '<span class="claimed">Claimed</span>';
		}
	?> 
    <tr>
        <td><a href="company_page.php?cid=<?=base64_encode($c['cust_id']);?>"><?=$c['company_name'];?></a></td>
        <td><?=rating(calcRating($c['cust_id'])); ?></td>
        <td><?=avgDaysToPay($c['cust_id']);?></td>
        <td><?=$writeReviewAction;?> <?=$claimCompanyAction;?></td>
    </tr>
    	<?php } ?>
</table>
	<?php 
	}
}?>