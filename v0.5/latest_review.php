<?php if ($login) { 
$reviews = getCustomerReviews("", $_SESSION['AP_uid']);
?>
		  <div><strong>Total Review(s): </strong><?=sizeof($reviews);?></div>
          <table class="table data">
        	<thead>
            	<tr>
                    <td>Company</td>
                    <td>Star Rating</td>
            </thead>
            <tbody>
            <?php 
			if (sizeof($reviews) > 0) {
			foreach ($reviews as $review) { 
				$user = getUserAccount($review['user_id']);
				$cust = getCustomer($review['cust_id'], -1);
				$cust = array_pop($cust);
			?>
            	<tr>
                    <td><a href="#" onclick="openModal('company_page.php?cid=<?=base64_encode($cust['cust_id']);?>');"><?=$cust['company_name'];?></a></td>
                    <td><?=rating(($review['rating']>0)?$review['rating']:0);?></td>
                </tr>
            <?php }
			} else { ?>
            	<tr>
                	<td colspan="2">You have not provided any reviews yet. Click <a href="search_company_main.php">here</a> now to add one.</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="push-right"><a href="customer_manage_reviews.php">See All</a></div>
<?php
}
else {
	header("Location: index.php");
}
?>