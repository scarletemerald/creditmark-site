<?php include_once('header.php'); ?>
<div class="container">
        <div class="row">
            <div class="grid_12">
                <div class="header2">
                    <h2><strong>Contact</strong> Us</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="grid_12">
                <form id="contact-form" class="contact-form">
                    <div class="contact-form-loader"></div>
                    <div class="header">
                        <h3>Contact Form</h3>
                    </div>
                    <fieldset>
                        <div class="row">
                            <div class="grid_6">
                                <label class="name">
                                    <input type="text" name="name" placeholder="Your name" data-constraints="@Required @JustLetters" />
                                    <span class="empty-message">*This field is required.</span>
                                </label>
                                <label class="email">
                                    <input type="email" name="email" placeholder="Your e-mail" value=""
                                           data-constraints="@Required @Email" />
                                    <span class="empty-message">*This field is required.</span>
                                    <span class="error-message">*This doesn't look right. Please check that this is a valid email.</span>
                                </label>
                            </div>
                            <div class="grid_12">
                                <label class="message">
                                    <textarea name="message" placeholder="What's on your mind? (2,000 characters)" data-constraints='@Required @Length(min=1,max=2,000)'></textarea>
                                    <span class="empty-message">*Don't forget to tell us your query.</span>
                                    <span class="error-message">*Please elaborate.</span>
                                </label>
                            </div>
                        </div>
                        <!-- <label class="recaptcha"><span class="empty-message">*This field is required.</span></label> -->
                        <div class="contact-form-buttons">
                            <action="mailto:register@pitchmark.org" data-type="submit" class="btn-default">Send Message</a>
                        </div>
                    </fieldset>
                    <div class="modal fade response-message">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Modal title</h4>
                                </div>
                                <div class="modal-body">
                                    Thank you for your message. It has been sent. We will be in touch by the end of the next business day.
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-27668603-3', 'auto');
  ga('send', 'pageview');

</script>    
<?php include_once('footer.php'); ?>