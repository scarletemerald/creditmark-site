</section>


<!--========================================================
                          FOOTER
=========================================================-->
<footer id="footer">
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="grid_12">
                    <div class="privacy-block">
                        <a href="index.php">CreditMark</a> &copy; <span id="copyright-year"></span>  All Rights Reserved <span class="divider1"> | </span> <a href="privacy_policy.php">Privacy Policy</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="js/script.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-27668603-3', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>