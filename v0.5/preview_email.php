<?php
require_once('function.php');
//Trial Files
?>
<link rel="stylesheet" href="css/font-awesome-4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<div style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: black; line-height: 1;">
<?php
$incidents = getIncident($_GET['iid']);
$incident = array_pop($incidents);

$user = getUserAccount($incident['user_id']);
$company = getCustomer($incident['cust_id'], $incident['user_id'], "", $incident['contact_id']);
$company = array_pop($company);
$contact = array_pop($company['contact']);

$template = getEmailTemplate($_GET['iid'], $_GET['eml']);
if ($template[0] != "") {
$body1 = $template[1];
$body1 = str_replace("[CustomerName1]", $contact['contact_name'], $body1);
$body1 = str_replace("[UserCompany]", $user['user_company'], $body1);
$body1 = str_replace("[InvoiceNumber]", $incident['invoice_no'], $body1);
$body1 = str_replace("[InvoiceCurrency]", $incident['invoice_currency'], $body1);
$body1 = str_replace("[InvoiceAmount]", number_format($incident['invoice_amount'], 2, ".", ","), $body1);
$body1 = str_replace("[InvoiceDate]", date("d M Y", strtotime($incident['invoice_due'])), $body1);
$body1 = str_replace("[InterestRate]", $incident['invoice_interest']."%", $body1);
$body1 = str_replace("[InterestTime]", $incident['invoice_interest_period']." day(s)", $body1);
if ($incident['invoice_interest_period'] <= 30) {
	$interest = $incident['invoice_amount']*($incident['invoice_interest']/100)*30/$incident['invoice_interest_period'];
}
else $interest = 0;
$body1 = str_replace("[InvoiceInterestNow]", number_format($interest, 2, ".", ","), $body1);
$body1 = str_replace("[InvoiceAmountNow]", number_format($incident['invoice_amount']+$interest, 2, ".", ","), $body1);
$body1 = str_replace("[UsersEmailAddress]", $user['user_email'], $body1);
$body1 = str_replace("[UserTelephone]", $user['user_phone'], $body1);
$body1 = str_replace("[UserName]", $user['user_name'], $body1);

$body1 .= $template[2];

if ($user['user_paypal'] != "") {
	$body1 = str_replace("[PaypalLink]", '&bull; <a href="https://www.paypal.com/cgi-bin/webscr?business='.$user['user_paypal'].'&cmd=_xclick&currency_code='.$incident['invoice_currency'].'&amount='.number_format($incident['invoice_amount'], 2, ".", "").'&item_name='.$incident['invoice_no'].'" target="_blank">Make payment now by PayPal</a><br />', $body1);
}
else $body1 = str_replace("[PaypalLink]", "", $body1);
$invoiceData = array();
$invoiceData['uid'] = $incident['user_id'];
$invoiceData['cid'] = $incident['cust_id'];
$invoiceData['iid'] = $incident['incident_id'];
$body1 = str_replace("[Link1]", "javascript:void(0);", $body1); // WL_PATH."invoice_paid.php?params=".base64_encode(serialize($invoiceData)), $body1);
$body1 = str_replace("[Link2]", "javascript:void(0);", $body1); // WL_PATH."invoice_nottally.php?params=".base64_encode(serialize($invoiceData)), $body1);
$body1 = str_replace("[Link3]", "javascript:void(0);", $body1); // WL_PATH."invoice_wrongrecipient.php?params=".base64_encode(serialize($invoiceData)), $body1);
$body1 = str_replace("[Link4]", "javascript:void(0);", $body1); // WL_PATH."login.php", $body1);
echo '<div align="right"><a href="email_edit.php?iid='.$_GET['iid'].'&eml='.$_GET['eml'].'"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x" title="edit"></i><i class="fa fa-pencil fa-stack-1x fa-inverse" title="edit"></i></span></a></div>';
echo '<strong>From: </strong>CreditMark &lt;service@creditmark.org&gt;<br />';
echo '<strong>To: </strong>'.$contact['contact_name'].' &lt;'.$contact['contact_email'].'&gt;<br />';
echo '<strong>Subject: </strong>'.$template[0].'<br /><br />';
echo $body1;

}
else echo 'Error loading email template. Please try again later.';
?>
</div>