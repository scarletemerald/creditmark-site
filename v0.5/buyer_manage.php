<?php include("header.php"); ?>
<?php if ($login && $_SESSION['AP_ut'] == 9999) {
$buyers = getApprovalBuyersAccount(); // Get All Non Approved
?>
<div class="container">
    <div class="row">
        <div class="grid_12">
          <div class="header2">
            <h2><strong>Manage</strong> Pending Buyer(s)</h2>
          </div>
          <table class="table data">
        	<thead>
            	<tr>
                    <td>Registered User</td>
                    <td width="80">Actions</td>
                </tr>
            </thead>
            <tbody>
            <?php if (sizeof($buyers) > 0) {
			foreach ($buyers as $buyer) { 
			?>
            	<tr>
                    <td><strong><?=$buyer['user_name'];?></strong><br />
                    <?=$buyer['user_email'];?><br />
                    <?=$buyer['user_phone'];?><br />
                    <?=$buyer['user_job_title'];?><br />
                    <?=$buyer['user_company'];?><br />
                    <?=$buyer['user_company_url'];?></td>
                    <td><a href="#" onclick="updateAccount('<?=base64_encode($buyer['user_id']);?>', 1);"><i class="fa fa-check-circle-o fa-2x" title="Approve"></i></a>
                    <a href="#" onclick="updateAccount('<?=base64_encode($buyer['user_id']);?>', -1);"><i class="fa fa-times-circle-o fa-2x" title="Reject"></i></a></td>
                </tr>
            <?php }
			} else { ?>
            	<tr>
                	<td colspan="4">There are no pending buyer registrations.</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="modal fade response-message">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title">Buyer</h4>
                    </div>
                    <div class="modal-body">
                      <iframe id="modalcontent" src="" width="100%" height="500px"></iframe>
                    </div>
                  </div>
                </div>
              </div>
      </div>
    </div>
</div>
<script>
function updateAccount(id, stats) {
	var s = (stats==1)?"APPROVE":"REJECT";
	if (confirm("Are you sure you wish to "+s+" this registered buyer?")) {
		$.post('bat/buyerDB.php', { 'id': id, 'stats': stats }, function(d) {
			alert(d);
			window.location.reload();
		});
	}
}
</script>

<?php include_once("footer.php"); ?>
<?php
}
else {
	header("Location: index.php");
}
?>