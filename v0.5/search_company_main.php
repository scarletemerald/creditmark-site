<?php include("header.php"); ?>
<?php if ($login) { 
?>
<link href="css/autocomplete.css" media="all" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<script>
var options, a;
jQuery(function(){
	alert( $('coycty').val());
	options = { serviceUrl:'getCustomers.php', showNoSuggestionNotice:true, minChars:3, lookupLimit:10, deferRequestBy: 500, orientation: 'auto', params: { 'cty': 'SGP' },	
				onSearchStart: function (query) {
			$('.autocomplete-suggestions').html('<div class="autocomplete-suggestion" data-index="0">Please wait while we search our extensive database. This might take a few moments.</div>').show();
		},
	onSelect: function (suggestion) {
		$.post('track.php', { 't': 'SEARCH', 'i': suggestion.id });
	}};
	a = $('input.coysearch').autocomplete(options);
});
function updateSearch(val) {
		options['params'] = { 'cty': val };
		console.log(options);
		$('input.coysearch').autocomplete('setOptions', options);
	}
</script>
<div class="container">
    <div class="row">
        <div class="grid_12">
          <div class="header2">
            <h2><strong>Search</strong> Customer</h2>
          </div>
      </div>
    </div>
    
    
    <div class="row">
        <div class="grid_12">
                	<div class="sselect">
                            <select name="coycty" id="coycty" class="select" onChange="updateSearch($(this).val());">
                            <option value="SGP">Singapore</option>
                    	<option value="AUS">Australia</option>
                            </select>
                        </div>
            <input name="coy" id="coy" type="text" placeholder="Enter your corporate customer's full name" class="coysearch" /><a href="javascript:void(0);" onClick="openModal('search_company.php?query='+encodeURIComponent($('#coy').val())+'&cty='+encodeURIComponent($('#coycty').val()));" class="btn-default" style="display: inline-block;">Search</a>
        </div>
    </div>
    
    <?php include_once('mod_promptpayers.php'); ?>
	
	<?php include_once('mod_mostpopular.php'); ?>
    
    <div class="modal fade response-message">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Customer</h4>
            </div>
            <div class="modal-body">
              <iframe id="modalcontent" src="" width="100%" height="500px"></iframe>
            </div>
          </div>
        </div>
      </div>
</div>
<script>
function deleteThis(id) {
	if (confirm("Are you sure you wish to paused all Cases for this Customer?")) {
		$.post('bat/customer_del.php', { 'id': id }, function(d) {
			alert(d);
			window.location.reload();
		});
	}
}
$(document).ready(function() {
	
<?php if ($_GET['add'] == 1) { ?>
	openModal('customer_edit.php');
<?php } ?>
<?php if (!empty($avgval)) { ?>
	var barChartData = {
		labels : [<?php
		$cnt = 0;
		foreach ($avgval as $coy=>$val) { 
			if ($cnt > 0) echo ', ';
			echo '"'.$coy.'"';
			$cnt++;
		}
		?>],
		datasets : [
			{
				label	: "Days until due date (your invoices)",
				fillColor : "rgba(220,220,220,0.5)",
				strokeColor : "rgba(220,220,220,0.8)",
				highlightFill: "rgba(220,220,220,0.75)",
				highlightStroke: "rgba(220,220,220,1)",
				data : [<?php
				$cnt = 0;
				foreach ($avgval as $coy=>$val) { 
					if ($cnt > 0) echo ', ';
					echo $val[0];
					$cnt++;
				}?>]
			},
			{
				label	: "Average time to pay (All CreditMark customers)",
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : [<?php
				$cnt = 0;
				foreach ($avgval as $coy=>$val) { 
					if ($cnt > 0) echo ', ';
					echo $val[1];
					$cnt++;
				}?>]
			}
		]
	}
	
	var options = {
		legendTemplate : "<ul class=\"Days until due date (your invoices)-legend\"><li><div style=\"background-color:rgba(220,220,220,0.5); display: inline-block; width: 20px; height: 20px; margin-right: 10px; vertical-align: middle;\"></div>Days until due date (your invoices)</li><li><div style=\"background-color:rgba(151,187,205,0.5); display: inline-block; width: 20px; height: 20px; margin-right: 10px; vertical-align: middle;\"></div>Average time to pay (All CreditMark customers)</li></ul>"
	}
	// Get context with jQuery - using jQuery's .get() method.
	var ctx = $("#canvas").get(0).getContext("2d");
	// This will get the first returned node in the jQuery collection.
	var myNewChart = new Chart(ctx).Bar(barChartData, options).generateLegend();
	$('#canvas').parent('div').append(myNewChart);
<?php } ?>
});
</script>

<?php include_once("footer.php"); ?>
<?php
}
else {
	header("Location: index.php");
}
?>