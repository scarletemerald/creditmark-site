<?php
require_once('function.php');
if ($login) {
?>
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/contact-form.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script>
    <script src="js/TMForm.js"></script>
    <script src="js/modal.js"></script>
    <form id="incident-import-form" class="contact-form" enctype="multipart/form-data">
       <div class="contact-form-loader"></div>
        <fieldset>
            <div class="row">
                <div class="grid_7" style="width: 97% !important;">
                    <p style="padding-top: 0px;">You can now upload your customer's payment track record in bulk from QuickBooks, Microsoft Excel, or any other accounting software.</p>
                    <p>As always, we will only ask you for your customer's name and:<br>1. The date of your invoice<br>2. Your credit terms (eg, 30 days), and<br>3. The date on which you actually received payment.</p>
                    <p>You should <u>not</u> provide any other details (items, amounts, names and addresses, etc).</p><br />
                    <a href="template/case_template.csv" target="_blank" class="btn-medium">Download File Template</a></p>
                    <label class="importFile">
                        <p>Browse File: <input data-constraints="@Required" type="file" name="importFile" placeholder="" /><br />
                        <span style="color: red;">Format: .csv only. Your file can be up to 5MB in size.</span></p>
                        <span class="empty-message">*This field is required.</span>
                    </label>
                    <br /><br />
                </div>
            </div>
            <div class="contact-form-buttons">
                <a href="#" data-type="submit" class="btn-default">Import</a>
            </div>
        </fieldset>
        <div class="modal fade response-message">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Import Case</h4>
                    </div>
                    <div class="modal-body">
                        Sorry, we were unable to import your file. Please try again. If you are still unsuccessful, please contact us.
                    </div>
                </div>
            </div>
        </div>
    </form>
<?php } ?>            