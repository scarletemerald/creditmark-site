<?php include("header.php"); ?>
<?php if ($login) { 
$incidents = getIncident($_GET['iid'], "", "", $_GET['cid']); 
if ($_GET['cid']) {
	$cust = getCustomer($_GET['cid'], -1);
	$cust = array_pop($cust);
}
else $cust['company_name'] = 'All';

$emailFreq = array(-60,-30,-21,-14,-7,-3,0,3);
 ?>

<div class="container">
    <div class="row">
        <div class="grid_12">
          <div class="header2">
            <div class="push-right">
                <button class="btn btn-medium push-right" onClick="openModal('incident_edit.php');">add new case</button>
            	<button class="btn btn-medium push-right" onClick="openModal('incident_import.php');">import cases</button>
                <div class="clearfix">
                <br />
            	<select id="view-filter"class=" push-right" onChange="$('tr.incident').hide();$('tr.incident'+this.value).show();">
                    <option value="1">View Active/Paused Only</option>
                	<option value="">View All</option>
                </select></div>
            </div>
            <h2><strong>Manage</strong> Email(s)</h2>
          </div>
          <table class="table data">
        	<thead>
            	<tr>
                    <td>Customer</td>
                    <td align="center">Invoice No.</td>
                    <td align="center">Days Till Due</td>
                    <td align="center">Days to Next Email</td>
                    <td align="center">Amount</td>
                    <td align="center">Payment Received</td>
                    <td align="center">Reply from Customer</td>
                    <td width="80" align="center">Status</td>
                    <td width="80" align="center">Actions</td>
                </tr>
            </thead>
            <tbody>
            <?php 
			if (sizeof($incidents) > 0) {
			foreach ($incidents as $incident) { 
				$company = $incident['customer'];
				$contact = array_pop($company['contact']);
				
				$daysdue = floor((strtotime($incident['invoice_due']) - time())/24/60/60);
				$daysduetxt = $daysdue;
				if ($daysdue <= 0) $daysduetxt = '<span style="color: red;">'.$daysdue.'</span>';
				
				$status = getIncidentStatus($incident['status']);
				
				// Next Email
				$daysemail = '-';
				$emlno = '-';
				foreach ($emailFreq as $k=>$emlF) {
					if ($daysdue >= $emlF) {
						$daysemail = $daysdue-$emlF;
						$emlno = $k;
					}
				}
				if ($daysdue > 0) { // Calc Halfway
					$halfway = floor((strtotime($incident['invoice_due']) - strtotime($incident['invoice_date']))/24/60/60/2);
					if ($halfway > 3 && $daysemail >= $halfway) {
						$daysemail = $daysemail - $halfway;
						$emlno++;
					}
				}
				if (!in_array($incident['status'], array(1,2))) {
					$daysduetxt = "-";
					$daysemail = '-';
					$emlno = "-";
					$istatus = 3;
				}
				else $istatus = 1;
				if ($emlno != "-" || $emlno === 0) $emlno++;
			?>
            	<tr class="incident incident<?=$istatus;?>">
                    <td><?='<strong>'.$company['company_name'].'</strong><br />'.$contact['contact_name'].'<br /><a href="mailto:'.$contact['contact_email'].'">'.$contact['contact_email'].'</a>';?></td>
                    <td align="center"><?=$incident['invoice_no'];?></td>
                    <td align="center"><?=$daysduetxt;?></td>
                    <td align="center"><?=$daysemail;?><?php if ($emlno != "-") { ?><br /><a href="#" onClick="openModal('preview_email.php?iid=<?=$incident['incident_id'];?>&eml=<?=$emlno;?>');">preview email</a><?php } ?></td>
                    <td align="center"><?=$incident['invoice_currency'].number_format($incident['invoice_amount'], 2, ".", ",");?></td>
                    <td align="center"><?php
                    if ($incident['payment_received'] != "") echo date("d M Y", strtotime($incident['payment_received']));
					else {
						echo '<input type="checkbox" name="pr_'.$incident['incident_id'].'" id="pr_'.$incident['incident_id'].'" onClick="setPrDate(\''.$incident['incident_id'].'\', this.checked);" />
						<div id="prdatediv_'.$incident['incident_id'].'" style="display: none;">
						<a class="hastip" title="<strong>Payment Received Date</strong><br />Please be sure to enter the correct date payment was received."><input type="text" name="prdate_'.$incident['incident_id'].'" style="width: 80px;" id="prdate_'.$incident['incident_id'].'" value="" /></a>
						<button type="button" onClick="saveDate(\''.$incident['incident_id'].'\');">Save</button>
						</div>';
					}
					?></td>
                    <td align="center"><?php
                    if ($incident['recipients'] != "") { ?>
                        <strong>Corrected Recipient(s)</strong>:
                        <?php $ra = explode(chr(10), $incident['recipients']);
                        foreach ($ra as $r) {
                            if ($r != "") {
                                $r = explode("::", $r);
                                echo '<br />- '.$r[0].' &lt;'.$r[1].'&gt;';
                            }
                        } 
					}
					else if ($incident['not_tally'] == 1) {?>
                   <strong>Not Tally with Customer's Record</strong>
                    <?php } else if ($incident['transaction_no'] != "") { ?>
                   <strong>Transaction Number: </strong><br /><?=$incident['transaction_no'];?>
                    <?php } ?></td>
                    <td align="center"><?=$status;?></td>
                    <td><?php if ($incident['status'] < 3) { ?><a href="#" onclick="openModal('incident_edit.php?iid=<?=$incident['incident_id'];?>');"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x" title="edit"></i><i class="fa fa-pencil fa-stack-1x fa-inverse" title="edit"></i></span></a><?php if ($incident['status'] == 1) { ?><a href="#" onclick="deleteThis('<?=$incident['incident_id'];?>');"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x" title="pause"></i><i class="fa fa-pause fa-stack-1x fa-inverse" title="pause"></i></span></a><?php } else if ($incident['status'] == 2) { ?><a href="#" onclick="addThis('<?=$incident['incident_id'];?>');"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x" title="play"></i><i class="fa fa-play fa-stack-1x fa-inverse" title="play"></i></span></a><?php } ?><?php } else { ?><a href="#" onclick="openModal('incident_edit.php?iid=<?=$incident['incident_id'];?>');"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x" title="view"></i><i class="fa fa-eye fa-stack-1x fa-inverse" title="view"></i></span></a><?php } ?></td>
                </tr>
            <?php }
			} else { ?>
            	<tr>
                	<td colspan="9">You have no case yet. Please click above to add one now.</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        
              <?php include('customer_manage.php'); ?>
        <div class="modal fade response-message">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Case</h4>
                </div>
                <div class="modal-body">
                  <iframe id="modalcontent" src="" width="100%" height="500px"></iframe>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
</div>

<script>
function saveDate(id) {
	var date = $('#prdate_'+id).val();
	if (date == "") alert("Please enter a date!");
	else if (/^(0[1-9]|[12][0-9]|3[01])[-//.](0[1-9]|1[012])[-//.](20)\d\d$/.test(date) ) {
		$.post('bat/incident_del.php', { 'id': id, 'prdate': date }, function(d) {
			alert(d);
			window.location.reload();
		});
		
	}
	else alert("Please enter the date in dd-mm-yyyy format only.");
}
function setPrDate(id, val) {
	if (val) {
		$('#prdate_'+id).val('<?=date('d-m-Y');?>');
		$('#prdatediv_'+id).show();
	}
	else {
		$('#prdate_'+id).val('');
		$('#prdatediv_'+id).hide();
	}
}
function addThis(id) {
	if (confirm("Are you sure you wish to un-pause this Case?")) {
		$.post('bat/incident_del.php', { 'id': id, 'status': 1 }, function(d) {
			alert(d);
			window.location.reload();
		});
	}
}
function deleteThis(id) {
	if (confirm("Are you sure you wish to pause this Case?")) {
		$.post('bat/incident_del.php', { 'id': id, 'status': 2 }, function(d) {
			alert(d);
			window.location.reload();
		});
	}
}
$(document).ready(function() {
	$('tr.incident').hide();
	$('tr.incident1').show();
<?php if ($_GET['add'] == 1) { ?>
	openModal('incident_edit.php<?php if ($_GET['cid'] != "") echo "?cid=".$_GET['cid'];?>');
<?php } ?>
});
</script>
<?php include_once("footer.php"); ?>
<?php
}
else {
	header("Location: index.php");
}
?>