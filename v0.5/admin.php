<?php include_once('header.php'); ?>
<div class="container">
	<div class="row">
        <div class="grid_12">
            <form id="loginadmin-form" class="contact-form">
                <div class="contact-form-loader"></div>
                <div class="header">
                    <h3>Admin Login</h3>
                </div>
                <fieldset>
                    <div class="row">
                        <div class="grid_12">
                            <label class="loginEmail">
                                <input type="text" name="loginEmail" placeholder="E-mail:" data-constraints="@Required @Email" />
                                <span class="empty-message">*This field is required.</span>
                                <span class="error-message">*This is not a valid email.</span>
                            </label>
                            <label class="loginPassword">
                                <input type="password" name="loginPassword" placeholder="Password:" value="" data-constraints="@Required" />
                                <span class="empty-message">*This field is required.</span>
                            </label>
                        </div>
                    </div>
                    <div class="contact-form-buttons">
                        <a href="#" data-type="submit" class="btn-default">Login Now</a>
                    </div>
                </fieldset>
                <div class="modal fade response-message">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Login</h4>
                            </div>
                            <div class="modal-body">
                                Failed to Login. Please try again.
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            
        </div>
    </div>
</div>
<?php include_once('footer.php'); ?>