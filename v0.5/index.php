<?php include_once('header.php'); ?>

    <link href="css/autocomplete.css" media="all" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.autocomplete.js"></script>
    <script>
	var options, a;
	jQuery(function(){
		options = { serviceUrl:'getCustomers.php', showNoSuggestionNotice:true, minChars:3, lookupLimit:10, deferRequestBy: 500, orientation: 'auto', params: { 'cty': 'SGP' },
		onSearchStart: function (query) {
			$('.autocomplete-suggestions').html('<div class="autocomplete-suggestion" data-index="0">Please wait while we search our extensive database. This might take a few moments.</div>').show();
		}};
		a = $('input.coysearch').autocomplete(options);
	});
	function updateSearch(val) {
		options['params'] = { 'cty': val };
		console.log(options);
		$('input.coysearch').autocomplete('setOptions', options);
	}
	</script>
    <div class="banner1">
        <div class="container">
            <div class="row">
                <div class="grid_12">
                    <h6><strong>WHEN WILL YOU GET PAID?</strong></h6><br><h2>Discover the payment track record of your customers and prospects.<br></h2>
                    <a class="btn-default" href="login.php">REGISTER NOW</a>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper1">
        <div class="container">
        	<div class="row">
                <div class="grid_12">
                	<div class="sselect">
                            <select name="coycty" id="coycty" class="select" onChange="updateSearch($(this).val());">
                            <option value="SGP" selected>Singapore</option>
                        	<option value="AUS">Australia</option>
                            </select>
                        </div>
                    <input name="coy" id="coy" type="text" placeholder="Enter your corporate customer's full name" class="coysearch" /><a href="javascript:void(0);" onClick="<?php if ($login) { ?>openModal('search_company.php?query='+encodeURIComponent($('#coy').val())+'&cty='+encodeURIComponent($('#coycty').val()));<?php } else { ?>alert('Please login to view search results.');<?php } ?>" class="btn-default" style="display: inline-block;">Search</a>
                </div>
            </div>
<a href="//www.mynewsdesk.com/follow/77295" class="mynewsdesk-follow-button" data-width="175" data-show-count="true">Follow CreditMark</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//www.mynewsdesk.com/javascripts/follow-button.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","mynewsdesk-fjs");</script>
            <div class="row">
                <div class="grid_4">
                    <div class="post1">
                        <div class="header">
                            <h3>DISCOVER</h3>
                            <img height="20%" src="images/cm-discover.jpg">
                        </div>
                        <div class="content" style="line-height: 1.5;">
                        	Find out when your customers pay other suppliers.<br /><br/>
                            Make more informed choices who to supply to, and plan your cash flows better.
                        </div>
                    </div>
                </div>
                <div class="grid_4">
                    <div class="post1">
                        <div class="header">
                            <h3>RATE</h3>
                            <img height="20%" src="images/cm-share.jpg">
                        </div>
                        <div class="content" style="line-height: 1.5;">
                            Share your experiences.<br /><br />This rewards prompt payers, and helps other suppliers find out how long companies take to pay.
                        </div>
                    </div>
                </div>

                <div class="grid_4">
                    <div class="post1">
                        <div class="header">
                            <h3>IMPROVE</h3>
                        	<img height="20%" src="images/cm-improve.jpg">
                        </div>
                        <div class="content" style="line-height: 1.5;">
                            The more details you share, the more you bring about positive change in the payment processes of large companies, and help small & medium enterprises survive and thrive.
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="grid_12">&nbsp;
                </div>
            </div>
            
            <?php include_once('mod_promptpayers.php'); ?>
<iframe id="settings-widget" src="http://www.mynewsdesk.com/story_widgets/1107" width="100%" height="450" border="0" scroll="no" frameborder="0"></iframe>            
            <?php include_once('mod_mostpopular.php'); ?>
            
           			 <div class="modal fade response-message">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title">Search Result</h4>
                            </div>
                            <div class="modal-body">
                              <iframe id="modalcontent" src="" width="100%" height="500px"></iframe>
                            </div>
                          </div>
                        </div>
                      </div>
            </div>
        </div>
    </div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-27668603-3', 'auto');
  ga('send', 'pageview');

</script>
<?php include_once('footer.php'); ?>
