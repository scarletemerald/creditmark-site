<?php
require_once('function.php');
if ($login) {
	if ($_GET['iid'] != "") {
		$incident = getIncident($_GET['iid']); 
		$incident = $incident[$_GET['iid']];
		$invoice_due_days = floor((strtotime($incident['invoice_due']) - strtotime($incident['invoice_date']))/3600/24);
		$cust = getCustomer($incident['cust_id'], $_SESSION['AP_uid'], "", $incident['contact_id']);
		$cust = array_pop($cust);
		if ($incident['contact_id'] != "") {
			$contact = getCustomerContact($incident['cust_id'], $incident['user_id'], "", $incident['contact_id']);
			$contact = array_pop($contact);
		}
		if ($incident['status'] == 3) $viewonly = true;
	}
	else if ($_GET['cid'] != "") {
		$cust = getCustomer($_GET['cid'], $_SESSION['AP_uid']);
		$cust = array_pop($cust);
	}
	else {
		$cust = getCustomerByContact("", $_SESSION['AP_uid']);
		$cust = array_pop($cust);
	}
	
	//print_r($cust);
	//exit();
	
?>
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
<link href="css/autocomplete.css" media="all" rel="stylesheet" type="text/css" />  
    <link rel="stylesheet" href="css/contact-form.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
    <script src="js/TMForm.js"></script>
    <script src="js/modal.js"></script>
    <?php if (sizeof($cust) > 0) {
		if ($viewonly) {
			$cust = getCustomer($incident['cust_id'], $_SESSION['AP_uid'], "", $incident['contact_id']);
			$cust = array_pop($cust);
			if ($incident['contact_id'] != "") $contact = array_pop($cust['contact']);
	 ?>
        <div class="row">
            <div class="grid_7" style="width: 97% !important;">
            	<table>
                <tr><td><strong>Company:</strong></td><td><?=$cust['company_name'];?></p>
                <tr><td><strong>Contact Person:</strong></td><td><?=$contact['contact_name'].' &lt;'.$contact['contact_email'].'&gt;';?></td></tr>
                <tr><td><strong>Invoice No:</strong></td><td><?=$incident['invoice_no'];?></td></tr>
                <tr><td><strong>Invoice Date:</strong></td><td><?=date("d M Y", strtotime($incident['invoice_date']));?></td></tr>
                <tr><td><strong>Invoice Due:</strong></td><td><?=date("d M Y", strtotime($incident['invoice_due']));?></td></tr>
                <tr><td><strong>Invoice Currency:</strong></td><td><?=$incident['invoice_currency'];?></td></tr>
                <tr><td><strong>Invoice Amount:</strong></td><td><?=$incident['invoice_amount'];?></td></tr>
                <tr><td><strong>Invoice Interest:</strong></td><td><?=$incident['invoice_interest'];?></td></tr>
                <tr><td><strong>Invoice Interest Period:</strong></td><td><?=$incident['invoice_interest_period'];?> Day(s)</td></tr>
                <tr><td><strong>Invoice Payment Date:</strong></td><td><?=date("d M Y", strtotime($incident['payment_received']));?></td></tr>
                </table>
            </div>
        </div>
	<?php
    	} else { ?>
		<form id="incident-form" class="contact-form" enctype="multipart/form-data">
				   <input type="hidden" name="incidentID" value="<?=$incident['incident_id'];?>" />
				   <input type="hidden" name="companyID" id="companyID" value="<?=$incident['cust_id'];?>" />
				   <input type="hidden" name="contactID" id="contactID" value="<?=$incident['contact_id'];?>" />
					<div class="contact-form-loader"></div>
					<fieldset>
						<div class="row">
							<div class="grid_7" style="width: 97% !important;">
								<p style="padding-top: 0px;">Tell us about the Case.</p>
								<label class="rateCompanyName">
                    <input type="text" name="rateCompanyName" id="rateCompanyName" class="coysearch" placeholder="Company Name*" data-constraints="@Required" value="<?=$cust['company_name'];?>" />
                    <span class="empty-message">*This field is required.</span>
                </label>
                            <label class="rateContactName">
                                <input type="text" name="rateContactName" id="rateContactName" class="coyconsearch" placeholder="Contact Person Name*"  data-constraints="@Required"  value="<?=$contact['contact_name'];?>" />
                   				 <span class="empty-message">*This field is required.</span>
                            </label>
                            
                            <label class="rateContactEmail">
                                <input type="text" name="rateContactEmail" id="rateContactEmail" placeholder="Contact Person E-mail*" data-constraints="@Required @Email" value="<?=$contact['contact_email'];?>" />
                                <span class="error-message">*This doesn't look right. Please check that this is a valid email.</span>
                                <span class="empty-message">*This field is required.</span>
                            </label>
								<?php if ($incident['recipients'] != "") { ?>
								<p style="padding-top: 0px;">These are contact(s) submitted by your client as the 'correct' recipient(s):<br />
								<?php $recipients = explode("\n", $incident['recipients']);
								foreach ($recipients as $recipient) { 
									if ($recipient != "") {
										$r = explode("::", $recipient); ?>
										&bull; <?=$r[0]." (".$r[1].")<br />"; ?>
								<?php 
									}
								} ?>
								<br /><br />
							   </p>
								<?php } ?>
								
								<label class="invoiceNo">
									<input type="text" name="invoiceNo" placeholder="Invoice Number*" data-constraints="@Required" value="<?=$incident['invoice_no'];?>" />
									<span class="empty-message">*This field is required.</span>
								</label>
								
								<label class="invoiceDate">
									<a class="hastip" title="Date Format: DD-MM-YYYY"><input type="text" name="invoiceDate" id="invoiceDate" placeholder="Invoice Date*" data-constraints="@Required @JustDate" value="<?=($incident['invoice_date']!="")?date("d-m-Y", strtotime($incident['invoice_date'])):"";?>" /></a>
									<span class="empty-message">*This field is required.</span>
									<span class="error-message">*Date format as dd-mm-yyyy.</span>
								</label>
								
								<label class="invoiceDue">
									<select name="invoiceDue" id="invoiceDue" data-constraints="@Required" class="select-menu" onchange="calcDueDate(this.value);">
										<option value="">Your Invoice to the customer was due after...*</option> 
										<optgroup label="Standard Days">
											<option value="14"<?php if ($invoice_due_days == 14) echo ' selected'; ?>>14 Days</option>
											<option value="30"<?php if ($invoice_due_days == 30) echo ' selected'; ?>>30 Days</option>
											<option value="60"<?php if ($invoice_due_days == 60) echo ' selected'; ?>>60 Days</option>
											<option value="90"<?php if ($invoice_due_days == 90) echo ' selected'; ?>>90 Days</option>
											<option value="120"<?php if ($invoice_due_days == 120) echo ' selected'; ?>>120 Days</option>
										</optgroup>
										<optgroup label="Other Days">
										<?php for ($i=1; $i<=365; $i++) { 
											if (!in_array($i, array(14, 30, 60, 90, 120))) {
										?>
										<option value="<?=$i;?>" <?php if ($invoice_due_days == $i) echo ' selected'; ?>><?=$i;?> Days</option>
										<?php }
										} ?>
										</optgroup>
									</select>
									<span class="empty-message">*This field is required.</span>
								</label>
								<label class="invoiceDueDate">
									<a class="hastip" title="Date Format: DD-MM-YYYY"><input type="text" name="invoiceDueDate" id="invoiceDueDate" placeholder="Invoice Due Date*" data-constraints="@Required @JustDate" value="<?=($incident['invoice_due']!="")?date("d-m-Y", strtotime($incident['invoice_due'])):"";?>" onblur="calcDueDay()" /></a>
									<span class="empty-message">*This field is required.</span>
									<span class="error-message">*Date format as dd-mm-yyyy.</span>
								</label>
								
								<label class="invoiceCurrency">
									<select name="invoiceCurrency" id="invoiceCurrency" class="select-menu" data-constraints="@Required">
										<option value="">Invoice Currency*</option> 
										<?php foreach ($currencyArr as $cur) { ?>
										<option value="<?=$cur;?>"<?php 
										if ($incident['invoice_currency'] == $cur) echo ' selected'; 
										else if ($incident['invoice_currency'] == "" && $_GET['cid'] == $cust['cust_id'] && $cust['currency_default'] == $cur) echo ' selected'; ?>><?=$cur;?></option>
										<?php } ?>
									</select>
									<span class="empty-message">*This field is required.</span>
								</label>
								
								<label class="invoiceAmount">
									<input type="text" name="invoiceAmount" placeholder="Invoice Amount*" data-constraints="@Required @JustDigits" value="<?=$incident['invoice_amount'];?>" />
									<span class="empty-message">*This field is required.</span>
									<span class="error-message">*Please enter only numbers.</span>
								</label>
								
								<label class="invoiceInterest">
									<input type="text" name="invoiceInterest" placeholder="Interest Due on Overdue Invoices (%)*" data-constraints="@Required @JustDigits" value="<?=$incident['invoice_interest'];?>" />
									<span class="empty-message">*This field is required.</span>
									<span class="error-message">*Please enter only numbers.</span>
								</label>
								
								<label class="invoice_interest_period">
									<select name="invoice_interest_period" id="invoice_interest_period" class="select-menu" data-constraints="@Required">
										<option value="">Interest Payable Time Period*</option> 
										<option value="1"<?php if ($incident['invoice_interest_period'] == 1) echo ' selected'; ?>>Per Day</option>
										<option value="7"<?php if ($incident['invoice_interest_period'] == 7) echo ' selected'; ?>>Per Week</option>
										<option value="30"<?php if ($incident['invoice_interest_period'] == 30) echo ' selected'; ?>>Per Month</option>
										<option value="365"<?php if ($incident['invoice_interest_period'] == 365) echo ' selected'; ?>>Per Year</option>
									</select>
									<span class="empty-message">*This field is required.</span>
								</label>
								
								<label class="paymentDate">
									<a class="hastip" title="Date Format: DD-MM-YYYY"><input type="text" name="paymentDate" placeholder="Payment Received Date" data-constraints="@JustDate" value="<?=($incident['payment_received']!="")?date("d-m-Y", strtotime($incident['payment_received'])):"";?>" /></a>
									<span class="error-message">*Date format as dd-mm-yyyy.</span>
								</label>
								
								<label class="pauseCampaign" style="height: 30px; color: white;">
									<input type="checkbox" name="pauseCampaign" value="on" <?=($incident['status']==2)?"checked":"";?> />
									Pause Campaign
								</label>
								
							</div>
						</div>
						<div class="contact-form-buttons">
							<a href="#" data-type="submit" class="btn-default">Update</a>
						</div>
					</fieldset>
					<div class="modal fade response-message">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title">My Account</h4>
								</div>
								<div class="modal-body">
									Sorry, your update was unsuccessful. Please try again. If you are still unsuccessful, please contact us.
								</div>
							</div>
						</div>
					</div>
				</form>
		<script>
		var options, a, currentcoy;
		jQuery(function(){
			currentcoy = '<?=$cust['cust_id'];?>';
			
			options = { serviceUrl:'getCustomers.php', showNoSuggestionNotice:true, minChars:3, orientation: 'auto', deferRequestBy: 500, params: { 'uid': '<?=$_SESSION['AP_uid'];?>' },		
				onSearchStart: function (query) {
			$('.autocomplete-suggestions').html('<div class="autocomplete-suggestion" data-index="0">Please wait while we search our extensive database. This might take a few moments.</div>').show();
		},
				onSelect: function (suggestion) {
					currentcoy = suggestion.id;
					
					$('input#companyID').val(currentcoy);
					activateContactSearch();
				}};
			a = $('input.coysearch').autocomplete(options);
			
			activateContactSearch();
			
			calcDueDay();
			<?php if ($incident['invoice_currency'] != "") { ?>
			$('#invoiceCurrency').css('color', 'white');
			<?php } ?>
			<?php if ($incident['invoice_interest_period'] != "") { ?>
			$('#invoice_interest_period').css('color', 'white');
			<?php } ?>
		});
		function activateContactSearch() {
			var options2 = { serviceUrl:'getCustomerContacts.php', showNoSuggestionNotice:true, minChars:3, lookupLimit:10, deferRequestBy: 500, orientation: 'auto', params: { 'c': currentcoy },		
				onSearchStart: function (query) {
			$('.autocomplete-suggestions').html('<div class="autocomplete-suggestion" data-index="0">Please wait while we search our extensive database. This might take a few moments.</div>').show();
		},
				onSelect: function (suggestion) {
					$('input#contactID').val(suggestion.id);
					var eml = suggestion.email;
					if (eml != "") {
						$('input#rateContactEmail').val(eml);
						$('.rateContactEmail ._placeholder').addClass('hidden');
					}
					else
						$('.rateContactEmail ._placeholder').removeClass('hidden');
				}
			};
			$('input.coyconsearch').autocomplete(options2);
		}
		function calcDueDay() {
			var invDate = ($('#invoiceDate').val()).split('-');
			invDate = invDate[2] + "-" + invDate[1] + "-" + invDate[0];
			var t1 = (new Date(invDate)).getTime();
			if (isNaN(t1)) {
			}
			else {
				var dueDate = ($('#invoiceDueDate').val()).split('-');
				dueDate = dueDate[2] + "-" + dueDate[1] + "-" + dueDate[0];
				var t2 = (new Date(dueDate)).getTime();
				
				var dueDays = Math.floor((t2-t1)/1000/3600/24);
				$('#invoiceDue').val(dueDays).css('color', 'white');
			}
		}
		function calcDueDate(val) {
			var invDate = ($('#invoiceDate').val()).split('-');
			invDate = invDate[2] + "-" + invDate[1] + "-" + invDate[0];
			var t1 = (new Date(invDate)).getTime();
			if (isNaN(t1)) {
			}
			else {
				var t2 = val*24*60*60*1000;
				var dueDate = new Date(t1+t2);
				dueDate = ((dueDate.getDate()<10)?"0"+dueDate.getDate():dueDate.getDate()) + "-" + ((dueDate.getMonth()+1<10)?"0"+(dueDate.getMonth()+1):dueDate.getMonth()+1) + "-" + dueDate.getFullYear();
				$('#invoiceDueDate').val(dueDate);
				$('.invoiceDueDate ._placeholder').addClass('hidden');
			
			}
		}
		</script>
		<?php 
		}
	} else { ?>
    <p>Please <a href="incident_manage.php" target="_top">be sure</a> you have entered your company name in your Account first.</p>
    <?php } ?>
<?php } ?>            