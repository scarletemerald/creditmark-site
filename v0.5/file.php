<?php
function getExtension($filename) {
	$pos = strrpos($filename, '.');
	if($pos===false) {
		return false;
	} 	
	else {
		return substr($filename, $pos+1);
	}
}
function fileProcess($file, $path, $filetype=array()) {
	$error = '';
	
	if(!empty($file['error']))
	{
		switch($file['error'])
		{
			case '4':
				$error = 'No file was uploaded.';
				break;
			case '6':
				$error = 'Missing a temporary folder.';
				break;
			default:
				$error = 'Unknown error.';
				break;
		}
	}
	else if (empty($file['tmp_name']) || $file['tmp_name'] == 'none') {
		$error = 'No file was uploaded.';
	}
	else {
	
		// Check Valid Extension
		if (empty($filetype)) $filetype = array("jpg", "jpeg", "gif", "png");
		$file_extension = getExtension($file["name"]);
		$is_valid_extension = false;
		foreach ($filetype as $extension) {
			if ($file_extension == $extension) {
				$is_valid_extension = true;
				break;
			}
		}
		if ($is_valid_extension) {
			// New File Name
			$valid_chars_regex = '.A-Z0-9_-';
			$file_name = preg_replace('/[^'.$valid_chars_regex.']|\.+$/i', "", basename($file['name']));
			$file_name = substr($file_name, 0, (strlen($file_extension)+1)*-1);
			$file_name = $file_name."_".date("Y-m-d H-i").".".$file_extension;
			
			// Upload File
			if (!@move_uploaded_file($file["tmp_name"], $path.$file_name)) {
				$error = "File could not be saved.";
			}
			else {
				return $file_name;
			}
		}
		else {
			$error = "Invalid file extension.";
		}
	}
	return $error;
}
?>