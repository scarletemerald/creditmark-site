<?php
include_once('function.php');
$data = base64_decode($_GET['params']);
$data = unserialize($data);
?>
<?php include("header.php"); ?>
<?php
$incident = getIncident($data['iid'], $data['uid'], "", $data['cid']);
$incident = array_pop($incident);
if (is_array($incident) && $incident['incident_id'] == $data['iid'] && $incident['user_id'] == $data['uid'] && $incident['cust_id'] == $data['cid']) {
	$user = getUserAccount($incident['user_id']);
	$company = $incident['customer'];
	$contact = array_pop($company['contact']);
?>
	<link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/contact-form.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script>
    <script src="js/TMForm.js"></script>
    <script src="js/modal.js"></script>
	<div class="container">
        <div class="row">
            <div class="grid_12">
              <div class="header2">
                <h2><strong><?=$user['user_company'];?></strong></h2>
              </div>
              <form id="incident-paid-form" class="contact-form" enctype="multipart/form-data" style="background: none;">
              <input type="hidden" name="params" value="<?=$_GET['params'];?>" />
              <table class="table-blank">
              <tr>
              	<td style="width: 150px;"><strong>To</strong></td>
                <td style="width: 500px;">
              <strong><?=$company['company_name'];?></strong><br />
              <?=$contact['contact_name'];?><br />
              <a href="mailto:<?=$contact['contact_email'];?>"><?=$contact['contact_email'];?></a>
              	</td>
              </tr>
              <tr>
              	<td colspan="2">&nbsp;</td>
              </tr>
              <tr>
              	<td><strong>Invoice No.</strong></td>
                <td><?=$incident['invoice_no'];?></td>
              </tr>
              <tr>
              	<td><strong>Invoice Date</strong></td>
                <td><?=date("d M Y", strtotime($incident['invoice_date']));?></td>
              </tr>
              <tr>
              	<td><strong>Invoice Due Date</strong></td>
                <td><?=date("d M Y", strtotime($incident['invoice_due']));?></td>
              </tr>
              <tr>
              	<td><strong>Invoice Amount</strong></td>
                <td><?=$incident['invoice_currency'].number_format($incident['invoice_amount'], 2, ".", ",");?></td>
              </tr>
              <tr>
              	<td colspan="2">&nbsp;</td>
              </tr>
              <tr>
              	<td colspan="2">If this invoice has already been paid, please provide the information required below:</td>
              </tr>
              <tr>
              	<td><strong>Transaction No./ Cheque No.</strong></td>
                <td><label class="transNo" style="padding: 0px;">
                    <input type="text" name="transNo" placeholder="Transaction No/Cheque No*" data-constraints="@Required" value="" />
                    <span class="empty-message">*This field is required.</span>
                </label></td>
              </tr>
              <tr>
              	<td><strong>Date Paid</strong></td>
                <td><label class="datePaid" style="padding: 0px;">
                    <a class="hastip" title="Date Format: DD-MM-YYYY"><input type="text" name="datePaid" placeholder="Date Paid*" data-constraints="@Required @JustDate" value="" /></a>
                    <span class="empty-message">*This field is required.</span>
                    <span class="error-message">*Date format as dd-mm-yyyy.</span>
                </label></td>
              </tr>
              <tr>
              	<td>&nbsp;</td>
              	<td><div class="contact-form-buttons" style="margin-top: 0px;">
                        <a href="#" data-type="submit" class="btn-default">Update</a>
                    </div></td>
              </tr>
              </table>
              <div class="modal fade response-message">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Invoice Paid</h4>
                            </div>
                            <div class="modal-body">
                                Sorry, we were unable to update this. Please try again. If you are still unsuccessful, please contact us.
                            </div>
                        </div>
                    </div>
                </div>
              </form>
            </div>
        </div>
    </div>
<?php
}
else {
	echo '<script>alert("Page not found"); window.location = "logout.php";</script>';
}
?>
<?php include("footer.php"); ?>