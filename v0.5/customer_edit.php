<?php
require_once('function.php');
if ($login) { 
	if ($_GET['cid'] != "") {
		$cust = getCustomer($_GET['cid']); 
		$cust = $cust[$_GET['cid']];
	}
?>
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/contact-form.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script>
    
    <link href="css/autocomplete.css" media="all" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.autocomplete.js"></script>
    <script src="js/TMForm.js"></script>
    <script src="js/modal.js"></script>
    <script>
	var options, a;
	jQuery(function(){
		options = { serviceUrl:'getCustomers.php', showNoSuggestionNotice:true, minChars:3, lookupLimit:10, deferRequestBy: 500, orientation: 'auto', params: { 'a': 1 },
		onSearchStart: function (query) {
			$('.autocomplete-suggestions').html('<div class="autocomplete-suggestion" data-index="0">Please wait while we search our extensive database. This might take a few moments.</div>').show();
		},
		onSelect: function (suggestion) {
			var uen = suggestion.uen;
			var url = suggestion.url;
			
			if (uen != "") {
				$('input#companyUEN').val(uen);
				$('.companyUEN ._placeholder').addClass('hidden');
			}
			else
				$('.companyUEN ._placeholder').removeClass('hidden');
			
			if (url != "") {
				$('input#companyURL').val(url);
				$('.companyURL ._placeholder').addClass('hidden');
			}
			else
				$('.companyURL ._placeholder').removeClass('hidden');
		}};
		a = $('#companyName').autocomplete(options);
	}); 
	</script>
    <form id="customer-form" class="contact-form" enctype="multipart/form-data">
    	<input type="hidden" name="companyID" value="<?=$cust['cust_id'];?>" />
                <div class="contact-form-loader"></div>
                <fieldset>
                    <div class="row">
                        <div class="grid_7" style="width: 97% !important;">
                            <p style="padding-top: 0px;">Tell us about your Customer.</p>
                            
                            <label class="companyName">
                                <input type="text" name="companyName" id="companyName" placeholder="Company Name*" data-constraints="@Required" value="<?=$cust['company_name'];?>" />
                                <span class="empty-message">*This field is required.</span>
                            </label>
                            <label class="companyUEN">
                                <input type="text" name="companyUEN" id="companyUEN" placeholder="Business Registered Number*" data-constraints="@Required" value="<?=$cust['company_uen'];?>" />
                                <span class="empty-message">*This field is required.</span>
                            </label>
                            
                           <label class="companyURL">
                                <a class="hastip" title="Make absolutely certain that the company you are reviewing and the web address match! Please include http://"><input type="text" name="companyURL" id="companyURL" placeholder="Company URL*" data-constraints="@URL" value="<?=$cust['company_url'];?>" /></a>
                                <span class="error-message">*Sorry, the URL is in an incorrect format. It should look like this: http://www.company.com or http://prefix.company.com.</span>
                            </label>
                            
                            <?php for ($i=0; $i<10; $i++) { ?>
                            <div class="contactpersn" <?php if ($i>0 && $cust['contact'][$i]['contact_name'] == "") echo 'style="display: none;"';?> >
                                <p>Contact Person #<?=$i+1;?></p>
                                <label class="contactName_<?=$i+1;?>">
                                    <input type="text" name="contactName_<?=$i+1;?>" placeholder="Contact Person Name*" <?php if ($i==0) echo 'data-constraints="@Required"'; ?> value="<?=$cust['contact'][$i]['contact_name'];?>" />
                                    <span class="empty-message">*This field is required.</span>
                                </label>
                                
                                <label class="contactEmail_<?=$i+1;?>">
                                    <input type="text" name="contactEmail_<?=$i+1;?>" placeholder="Contact Person E-mail*" data-constraints="<?php if ($i==0) echo '@Required '; ?>@Email" value="<?=$cust['contact'][$i]['contact_email'];?>" />
                                    <span class="empty-message">*This field is required.</span>
                                    <span class="error-message">*This doesn't look right. Please check that this is a valid email.</span>
                                </label>
                            </div>
                            <?php } ?>
                            <div align="center"><button id="addct" type="button" class="btn-medium">Add Contact (up to 10)</button></div>
                        </div>
                    </div>
                    <div class="contact-form-buttons">
                        <a href="#" data-type="submit" class="btn-default">Update</a>
                    </div>
                </fieldset>
                <div class="modal fade response-message">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">My Account</h4>
                            </div>
                            <div class="modal-body">
                                Sorry, your update was unsuccessful. Please try again. If you are still unsuccessful, please contact us.
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <script>
			$(document).ready(function() {
				$("#addct").click(function() {
					$('.contactpersn:visible:last').next('.contactpersn:hidden').show();
					if ($('.contactpersn:visible').length >= 10) $('#addct').hide();
				});
			});
			</script>
<?php } ?>            