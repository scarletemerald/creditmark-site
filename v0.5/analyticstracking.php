<!DOCTYPE html>
<html lang="en">
<head>
    <title>Home</title>
    <meta charset="utf-8">
    <meta name = "format-detection" content = "telephone=no" />
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.2.1.js"></script>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
    <![endif]-->
</head>

<body>

<!--========================================================
                          HEADER
=========================================================-->
<header id="header" class="__absolute">
    <div id="stuck_container">
        <div class="container">
            <div class="row">
                <div class="grid_4">
                    <h1><a href="index.html">Dan Adams</a></h1>
                </div>
                <div class="grid_8">
                    <nav>
                        <ul class="sf-menu">
                            <li class="current"><a href="index.html">Home</a></li>
                            <li><a href="index-1.html">About</a></li>
                            <li><a href="index-2.html">Services</a>
                                <ul>
                                    <li><a href="#">Aliquam congue</a></li>
                                    <li><a href="#">Fermentum nisl</a>
                                        <ul>
                                            <li><a href="#">Nulla vel diam</a></li>
                                            <li><a href="#">Sed in lacus ut</a></li>
                                            <li><a href="#">Aliquam congue</a></li>
                                            <li><a href="#">Fermentum nisl</a></li>
                                            <li><a href="#">Fermentum nisl</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Mauris accumsan</a></li>
                                    <li><a href="#">Nulla vel diam</a></li>
                                    <li><a href="#">Sed in lacus ut</a></li>
                                </ul>
                            </li>
                            <li><a href="index-3.html">Contact Me</a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>

<!--========================================================
                          CONTENT
=========================================================-->
<section id="content">
    <div class="banner1">
        <div class="container">
            <div class="row">
                <div class="grid_7">
                    <h6>Best professional consulting at<br><strong>your service</strong></h6>
                    <a class="btn-default" href="#">Read More</a>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper1">
        <div class="container">
            <div class="row">
                <div class="grid_8 preffix_2">
                    <div class="header1">
                        <h2><strong>Find</strong> Everything You Want <strong>to Know About Business</strong></h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="grid_4">
                    <div class="post1">
                        <div class="header">
                            <h3>Online Marketing</h3>
                        </div>
                        <img src="images/page1_img2.png" alt=""/>
                        <div class="content">
                            <a href="#" class="btn-default">more</a>
                        </div>
                    </div>
                </div>

                <div class="grid_4">
                    <div class="post1">
                        <div class="header">
                            <h3>Advertising</h3>
                        </div>
                        <img src="images/page1_img3.png" alt=""/>
                        <div class="content">
                            <a href="#" class="btn-default">more</a>
                        </div>
                    </div>
                </div>

                <div class="grid_4">
                    <div class="post1">
                        <div class="header">
                            <h3>Competition Analysis</h3>
                        </div>
                        <img src="images/page1_img4.png" alt=""/>
                        <div class="content">
                            <a href="#" class="btn-default">more</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="grid_8">
                    <div class="header2">
                        <h2><strong>Recent</strong> from the Blog</h2>
                    </div>
                    <div class="post2">
                        <img src="images/page1_img5.png" alt=""/>
                        <div class="content">
                            <time datetime="2014-01-01">March 27, 2014</time>
                            <h4>Etiam cursus leo vel</h4>
                            <p>Fusce euismod consequat ante. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque sed dolor. Aliquam congue fermentum nisl. Mauris accumsan nulla</p>
                            <a class="btn-small" href="#">Read More</a>
                        </div>
                    </div>

                    <div class="post2">
                        <img src="images/page1_img6.png" alt=""/>
                        <div class="content">
                            <time datetime="2014-01-01">March 27, 2014</time>
                            <h4>Etiam cursus leo vel</h4>
                            <p>Fusce euismod consequat ante. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque sed dolor. Aliquam congue fermentum nisl. Mauris accumsan nulla</p>
                            <a class="btn-small" href="#">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="grid_4">
                    <div class="header2">
                        <h2><strong>Services</strong></h2>
                    </div>
                    <ul class="list1 __margin1">
                        <li><a href="#">Vestibulum ante ipsum primis</a></li>
                        <li><a href="#">In faucibus orci luctus et</a></li>
                        <li><a href="#">Ultrices posuere cubilia Curae</a></li>
                        <li><a href="#">Suspendisse sollicitudin velit sed</a></li>
                        <li><a href="#">Ut pharetra augue nec augue</a></li>
                        <li><a href="#">Nam elit agna,endrerit sit amet</a></li>
                        <li><a href="#">Tincidunt ac, viverra sed, nulla</a></li>
                        <li><a href="#">Donec porta diam eu massa</a></li>
                        <li><a href="#">Quisque diam lorem</a></li>
                    </ul>
                    <div class="header2">
                        <h2><strong>Follow Me</strong></h2>
                    </div>
                    <ul class="socials1 __margin3">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-skype"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="grid_12">
                    <div class="header2 __margin2">
                        <h2><strong>Clients</strong></h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="grid_4">
                    <ul class="list2 __margin3">
                        <li><a href="#">Nulla facilisi</a></li>
                        <li><a href="#">Aenean nec eros</a></li>
                        <li><a href="#">Vestibulum ante ipsum primis</a></li>
                        <li><a href="#">In faucibus orci luctus et</a></li>
                        <li><a href="#">Ultrices posuere cubilia Curae</a></li>
                        <li><a href="#">Suspendisse sollicitudin velit sed leo</a></li>
                        <li><a href="#">Ut pharetra augue nec augue</a></li>
                        <li><a href="#">Nam elit agna,endrerit sit amet</a></li>
                        <li><a href="#">Tincidunt ac, viverra sed, nulla</a></li>
                        <li><a href="#">Donec porta diam eu massa</a></li>
                        <li><a href="#">Quisque diam lorem</a></li>
                        <li><a href="#">Interdum vitae,dapibus ac</a></li>
                        <li><a href="#">Scelerisque vitae, pede</a></li>
                        <li><a href="#">Donec eget tellus non erat</a></li>
                    </ul>
                </div>
                <div class="grid_4">
                    <ul class="list2 __margin3">
                        <li><a href="#">Suspendisse sollicitudin velit sed leo</a></li>
                        <li><a href="#">Ut pharetra augue nec augue</a></li>
                        <li><a href="#">Nam elit agna,endrerit sit amet</a></li>
                        <li><a href="#">Tincidunt ac, viverra sed, nulla</a></li>
                        <li><a href="#">Donec porta diam eu massa</a></li>
                        <li><a href="#">Quisque diam lorem</a></li>
                        <li><a href="#">Interdum vitae,dapibus ac</a></li>
                        <li><a href="#">Scelerisque vitae, pede</a></li>
                        <li><a href="#">Donec eget tellus non erat</a></li>
                        <li><a href="#">Lacinia fermentum</a></li>
                        <li><a href="#">Donec in velit vel ipsum auctor pulvinar</a></li>
                        <li><a href="#">Vestibulum iaculis lacinia est</a></li>
                        <li><a href="#">Proin dictum elementum velit</a></li>
                        <li><a href="#">Fusce euismod consequat ante</a></li>
                    </ul>
                </div>
                <div class="grid_4">
                    <ul class="list2 __margin3">
                        <li><a href="#">Vestibulum ante ipsum primis</a></li>
                        <li><a href="#">In faucibus orci luctus et</a></li>
                        <li><a href="#">Ultrices posuere cubilia Curae</a></li>
                        <li><a href="#">Suspendisse sollicitudin velit sed leo</a></li>
                        <li><a href="#">Ut pharetra augue nec augue</a></li>
                        <li><a href="#">Nam elit agna,endrerit sit amet</a></li>
                        <li><a href="#">Tincidunt ac, viverra sed, nulla</a></li>
                        <li><a href="#">Donec porta diam eu massa</a></li>
                        <li><a href="#">Quisque diam lorem</a></li>
                        <li><a href="#">Interdum vitae,dapibus ac</a></li>
                        <li><a href="#">Scelerisque vitae, pede</a></li>
                        <li><a href="#">Donec eget tellus non erat</a></li>
                        <li><a href="#">Lacinia fermentum</a></li>
                        <li><a href="#">Donec in velit vel ipsum auctor pulvinar</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>



<!--========================================================
                          FOOTER
=========================================================-->
<footer id="footer">
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="grid_12">
                    <div class="privacy-block">
                        <a href="#">Dan Adams</a> &copy; <span id="copyright-year"></span>  All Rights Reserved <span class="divider1"> | </span> <a href="index-4.html">Privacy policy</a>
                        More <a rel="nofollow" href="http://www.templatemonster.com/category/consulting-website-templates/" target="_blank">Consulting Website Templates at TemplateMonster.com</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="js/script.js"></script>
</body>
</html>