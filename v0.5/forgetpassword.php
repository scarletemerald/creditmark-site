<?php include_once('header.php'); 

unset($_SESSION["AP_login"]);
unset($_SESSION["AP_uid"]);
unset($_SESSION["AP_eml"]);
setcookie('registered_user@creditmark.org', '', time()-3600);
?>
<style>
.idpico {
	cursor: pointer;
}
</style>
<script>
$(document).ready(function() {
	$('.idpico').click(function() {
		var idp = $( this ).attr( "idp" );
		switch( idp ){
			case "facebook": case "linkedin" : 
				start_auth( "?provider=" + idp );
				break;
			default: alert( "u no fun" );
		}
	});
});
function start_auth( params ){
	var start_url = 'bat/login_social.php' + params + "&return_to=<?php echo urlencode(WL_PATH."account.php"); ?>" + "&_ts=" + (new Date()).getTime();
	window.open(
		start_url, 
		"hybridauth_social_sing_on", 
		"location=0,status=0,scrollbars=0,width=800,height=500"
	);  
}
</script>
<div class="container">
	<div class="row">
        <div class="grid_12">
           <form id="forgotpw-form" class="contact-form">
           		<div class="contact-form-loader"></div>
                <div class="header">
                    <h3>Forget Password</h3>
                </div>
                <p style="padding-top: 0px;">Please fill in your email below in order to retrieve your password.</p>
                <fieldset>
                    <div class="row">
                        <div class="grid_12">
                            <label class="loginEmail">
                                <input type="text" name="loginEmail" placeholder="E-mail:" data-constraints="@Required @Email" />
                                <span class="empty-message">*This field is required.</span>
                                <span class="error-message">*This doesn't look right. Please check that this is a valid email.</span>
                            </label>
                        </div>
                    </div>
                    <div class="contact-form-buttons">
                        <a href="#" data-type="submit" class="btn-default">Retrieve Password</a>
                    </div>
                </fieldset>
                <div class="modal fade response-message">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Forget Password</h4>
                            </div>
                            <div class="modal-body">
                                Sorry, we were unable to retrieve your password. Please try again. If you are still unsuccessful, please contact us.
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-27668603-3', 'auto');
  ga('send', 'pageview');

</script>
<?php include_once('footer.php'); ?>