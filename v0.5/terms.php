<?php include_once('header.php'); ?>
<div class="container">
        <div class="row">
            <div class="grid_12">
                <div class="header2">
                    <h2><strong>Terms </strong>of Use</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="grid_12">
                <p>Dated 3 August 2016</p>
                <p><strong>CREDITMARK TERMS OF USE</strong></p>
                <p><b>1. Acceptance of Terms of Use</b></p>
                <p>1.1 Please read these Terms of Use (“Terms”, “Terms of Use”) carefully before using www.creditmark.org which is a website established to enable both suppliers of goods and services (“Suppliers”) and customers of such goods and services (“Customers”) to provide honest and accurate feedback on the Supplier’s experiences with respect to the Customer’s payment and credit practices (“the Services”). The website and the Services are operated by CreditMark LLP  (“us”, “we”, or “our”). These Terms of Use contains the terms and conditions that govern the use of the Services provided through our website and/or mobile application operated by us (the “Site”).</p>
                <p>1.2 You confirm that you are either more than 18 years of age, or possess legal parental or guardian consent, and are fully able and competent to enter into the terms, conditions, obligations, affirmations, representations, and warranties set forth in these Terms of Use, and to abide by and comply with these Terms of Use.</p>
                <p>1.3 You are required to accept the Terms of Use before you can access and use the Services. By accessing and/or using the Services, you are consenting to be bound by the Terms. If you are agreeing to these Terms on behalf of a business, you represent and warrant that you have authority to bind that business to these Terms, and your agreement to these terms will be treated as the agreement of the business. In that event, "you" and "your" refer to that business.</p>
                <p>1.4 These Terms also apply to all visitors, users and others who access or use the Services. If you disagree with any part of the Terms, then you may not access or use the Services.</p>
                <p><strong>2. Registration and ID</strong></p>
                <p>2.1 To post or submit Content (defined in clause 5.1) to the Site, you must first register and create a User ID and password. We reserve the right to restrict access to other areas of our Site, or whole Site, at our discretion.</p>
                <p>2.2 When registering to access and use the Services, you agree to provide us with true, accurate, current and complete information and details and other information that we may reasonably require to provide you with the Services.</p>
                <p>2.3 Your registration constitutes consent to use any personal data which you provide us in such registration for the purposes set out in our <a href="privacy_policy.php">Privacy Policy</a> and for all uses ancillary thereto for which your consent may reasonably be implied.</p>
                <p>2.4 If we provide you or if you generate a User ID and Password to enable you to access the Site or other content or services, you must ensure that your User ID and Password is kept confidential. Your User ID and Password are personal to you and you may not allow any others to use your User ID or Password under any circumstances. You must notify us in writing immediately if you become aware of any unauthorised use of your account or password.</p>
                <p>2.5 If required by law (including a court order and a government or regulatory demand or requirement having the force of law), we will disclose such information, including but not limited to your registration information, as so ordered.</p>
                <p>2.6 We cannot and will not be held liable for any loss, damage or harm caused or related to the theft or misappropriation of your User ID or Password, disclosure of your User ID or Password, or your authorisation of anyone else to use your User ID or Password.</p>
                <p><strong>3. Changes to the Terms</strong></p>
                <p>We reserve the right to change the terms, conditions, and Services at any time, which will be effective when posted on the Site or when you are reasonably notified by other means. If you do not wish to be bound by such change, you may discontinue using and terminate the Service before the change becomes effective. Your continued use of the Service after the change becomes effective, indicates your agreement to the change. We strongly advise you to review the Site and/or the Terms on a regular basis to ensure you understand all terms and conditions governing use of this Site.</p>
                <p><strong>4. Limitations of Use</strong></p>
                <p>4.1 You agree not to:</p>
                <p>(i) use the Services other than in accordance with these Terms of Use;</p>
                <p>(ii) use the Site for commercial or business purposes, including, without limitation, advertising, marketing or offering goods or services, whether or not for financial or any other form of compensation or through linking with any other website or web pages;</p>
                <p>(iii) copy or use the Content in connection with a service deemed competitive by us;</p>
                <p>(iv) not copy, modify or create derivative works of our Content and/or Services;</p>
                <p>(v) use any device, software, or routine that interferes with any application, function, or use of the Services, or is intended to damage, detrimentally interfere with, surreptitiously intercept, or expropriate any system, data, or communication;</p>
                <p>(vi) sell, sublicense, time-share, or otherwise share the Services with any third party;</p>
                <p>(vii) frame or mirror the Services;</p>
                <p>(viii) decompile, disassemble or reverse-engineer the underlying software or application that is part of the Services or otherwise attempt to derive its source code;</p>
                <p>(ix) use the Services either directly or indirectly to support any activity that is illegal;</p>
                <p>(x) access the Services for purposes of monitoring its availability, performance or functionality, or for any other benchmarking or competitive purposes; or</p>
                <p>(xi) authorize any third parties to do any of the above.</p>
                <p><strong>5. Content</strong></p>
                <p>5.1 In these Terms of Use, “Content” shall include, without limitation, any audio, text, images, animation, logo, written posts, ratings, feedback, replies, comments, information, documents, data or other materials provided or otherwise made accessible on or through our Services including Content that you choose to provide, submit or transmit through our Services.</p>
                <p>5.2 Subject to clause 5.3, you must ensure that all Content provided is timely, true, complete, current and accurate and complies with the relevant laws and/or regulations of the relevant jurisdictions. The Content you provide must have sufficient information to enable the Customer to respond to any feedback published on the Site. You will not post or submit any Content to the Site that is false, intentionally misleading, defamatory, offensive, violates third party rights or contains Content that is unlawful or violates any law.</p>
                <p>5.4 You are solely responsible and assume all risks for any Content posted or supplied by you to the Site and you agree that any such Content is solely your opinion and that we have no control over the Content and veracity of the Content. You must ensure that all Content provided does not contain any personally identifiable information, confidential information and/or trade secrets and that your posting and submission of such Content complies with the relevant data privacy laws of the relevant jurisdictions and does not breach any obligations of confidentiality, contractual or otherwise.</p>
                <p>5.5 When we display Content on or via the Services, you grant us, to the extent permissible by law, a non-exclusive, worldwide, royalty-free, sub-licensable, transferable right and license to use, host, store, cache, reproduce, publish, display, distribute, transmit, modify, adapt, and create derivative works of your Content. The rights you grant in this license are for the limited purposes of allowing us to operate, improve on and develop new Services.</p>
                <p>5.6 You also warrant that</p>
                <p>(i) you are the sole legal and beneficial owner of and own all rights and interests in your Content and</p>
                <p>(ii) no third party has any rights, title and interests, including all intellectual property rights in your Content.</p>
                <p>5.7 We do not verify the truthfulness of nor do we validate or endorse any Content posted or supplied by you to the Site. The Site is not moderated and if a Customer or any third party disputes any Content or feels that the Content is false, intentionally misleading, defamatory or offensive, the Customer may either reply to the Content or communicate directly with us for the purposes of verifying the Content or taking down the Content from the Site. We can be contacted at <a href="mailto:register@creditmark.org">register@creditmark.org</a>.</p>
                <p>5.8 We reserve the right to remove your Content from the Site at any time, and for any reason, without notice.</p>
                <p><strong>5A. Customer Ratings</strong></p>
                <p>5A.1 Customer ratings published on our Site are based on factual information received by us from Suppliers. Users of Customer ratings should understand that the accuracy of such ratings depends solely on the accuracy and completeness of information supplied by Suppliers. Further, Customer ratings are statements of opinion reflective of past evidences of payment practices and cannot by that nature predict with accuracy the actual state of future practices. As a result, despite any verification of current facts, ratings can be affected by future events or conditions that were not anticipated at the time a rating was given by the Supplier.</p>
                <p>5A.3 The Customer ratings published on the Site is provided “AS IS” and to the extent permitted by law, we make no representation or warranty, express or implied as to the accuracy, timeliness, completeness, merchantability or fitness for any particular purposes of such ratings. Except to the extent permitted by law, under no circumstances shall we have any liability to any person or entity for any loss or damage resulting from or relating to, any error or other circumstance or contingency within or outside of our control or the control of our directors, officers, employees or agents in connection with the reliance, compilation, transmission, communication, interpretation, publication, delivery and/or analysis of such ratings.</p>
                <p><strong>6. Intellectual Property</strong></p>
                <p>6.1 Save for Content that you own in clause 5, we own all the worldwide rights, titles and interests in and to the Services and the Site, applications, text, pictures, videos, graphic, user interface, trade marks, logos, applications, programs, software and platform that we use to provide the Services (“our Intellectual Property Rights”). These Terms of Use do not convey any proprietary interest in or to our Intellectual Property Rights or entitlement to the use thereof except as expressly set forth here.</p>
                <p>6.2 By using the Site and the Services, you hereby grant to us a perpetual, irrevocable, worldwide, royalty-free licence to right to use the Content which shall include but not be limited to making copies of, retaining, transmitting, reformatting, displaying and distributing the Content.</p>
                <p><strong>7. Service and Site Access</strong></p>
                <p>7.1 We will use reasonable efforts to ensure that the Site and the Services are available at all times.</p>
                <p>7.2 Notwithstanding the foregoing, we</p>
                <p>(a) do not warrant that your use of the Services will be uninterrupted or error-free; or that the Services will meet your requirements; and</p>
                <p>(b) are not responsible for any delays, delivery failures, or any other loss or damage resulting from the transfer of data over communications networks and facilities, including the internet.</p>
                <p>7.3 You acknowledge that the Services may be subject to limitations, delays and other problems inherent in the use of such communications facilities.</p>
                <p><strong>8. Payment</strong></p>
                <p>8.1 Some of the Services require payment of fees (the "Paid Services”). All fees are stated in United States Dollars. You shall pay all applicable fees, as described in the applicable Services, in connection with such Services, and any related taxes or additional charges.</p>
                <p>8.2 Purchases of Paid Services are final and non-refundable, except at our sole discretion and in accordance with the rules governing each Paid Service. Termination under this Agreement may result in forfeiture of purchased Paid Services.</p>
                <p>8.3 We may change prices of Paid Services at any time. To the extent applicable, we will provide you reasonable notice of any such pricing changes by posting the new prices on or through the applicable Paid Service and/or by sending you an email notification. If you do not wish to pay the new prices, you may choose not to purchase, or to cancel, the applicable Paid Service prior to the change going into effect.</p>
                <p>A.2 Before posting or submitting any Content to the Site, please ensure that the Content does not contain any confidential information or trade secrets and that your posting and submission of such Content does not breach any obligations of confidentiality, contractual or otherwise.</p>
                <p><strong>9. Third Party Websites</strong></p>
                <p>We assume no responsibility for the content of websites linked on our site. Such links should not be interpreted as endorsement by us of those linked websites. We will not be liable for any loss or damage that may arise from your use of them. To the extent that we may display, use and/or rely on datasets for and/or through our Site and/or Services, you acknowledge that such datasets are provided by the Singapore Government and its Statutory Boards via Data.gov.sg and are governed by the Terms of Use available at https://data.gov.sg/terms. To the fullest extent permitted by law, the Singapore Government and its Statutory Boards are not liable for any damage or loss of any kind caused directly or indirectly by the use of the datasets or derived analyses or predictions.</p>
                <p><strong>10. Limitation of Liability</strong></p>
                <p>10.1 Nothing in these Terms of Use excludes or limits our liability for death or personal injury arising from our negligence, or our fraud or fraudulent misrepresentation, or any other liability that cannot be excluded or limited by law.</p>
                <p>10.2 To the extent permitted by law, we exclude all conditions, warranties, representations or other terms which may apply to the site or any Content on it, whether express or implied. The Site and Services are provided on as “AS IS” basis and we do not verify, endorse or moderate any Content posted or submitted to the Site. The Supplier and Customer are solely responsible for all Content that they post or submit to the Site.</p>
                <p>10.3 To the extent permitted by law, we shall not be liable for:</p>
                <p>(a) any indirect, incidental, exemplary punitive, or consequential damages of any kind whatsoever;</p>
                <p>(b) loss of profits, sales, revenue, data, use, goodwill, or other intangible losses;</p>
                <p>(c) damages relating to your access to, use of, or inability to access or use the services;</p>
                <p>(d) damages relating to any conduct or Content of any Supplier and/or Customer, including without limitation, defamatory, offensive, unlawful or illegal conduct or Content</p>
                <p>(e) damages relating to any conduct or Content of any third party or subscriber using the Services, including without limitation, defamatory, offensive, unlawful or illegal conduct or Content; and/or</p>
                <p>(f) damages in any manner relating to any Content.</p>
                <p>10.4 To the extent permitted by law, our total liability for any claim under these Terms of Use, including for any implied warranties, is limited to the greater of USD 100 or the amount you paid to use the applicable Services.</p>
                <p><strong>11. Indemnity</strong></p>
                <p>You hereby indemnify us and our affiliates, officers, agents, partners, and employees and undertake to keep us indemnified against any losses, damages, costs, liabilities and expenses (including, without limitation, legal expenses and any amounts paid by us to a third party in settlement of a claim or dispute on the advice of our legal advisers) incurred or suffered by us arising out of the Content you post or submit to or transmit through our Site or Services, your access or use of our Site or Services, your violation of any of these Terms of Use, or your violation of any rights of another.</p>
                <p><strong>12. Termination</strong></p>
                <p>The Services can be terminated by either party immediately without notice and without reason. We may terminate or temporarily suspend your access to the Service in the event that:</p>
                <p>(i) you breach any material provision of this Terms of Use that, (if it is capable of being cured) is not cured within 10 days from notice to you or</p>
                <p>(ii) we determine that your actions are likely to cause legal liability for us or that you have misrepresented any data or information required by us to provide you with the Services or at any other time. Upon any termination of the Service, your right to access and use the Services will automatically terminate, and you may not continue to access or use the Service. We will have no liability for any costs, losses, damages, or liabilities arising out of or related to our termination of your access to the Site and/or Services.</p>
                <p><strong>13. Waiver</strong></p>
                <p>No failure or delay by a party to exercise any right or remedy provided under this Terms of Use or by law shall constitute a waiver of that or any other right or remedy, nor shall it preclude or restrict the further exercise of that or any other right or remedy. No single or partial exercise of such right or remedy shall preclude or restrict the further exercise of that or any other right or remedy.</p>
                <p><strong>14. Severance</strong></p>
                <p>If any provision or part-provision of these Terms of Use is or becomes invalid, illegal or unenforceable, it shall be deemed modified to the minimum extent necessary to make it valid, legal and enforceable. If such modification is not possible, the relevant provision or part-provision shall be deemed deleted. Any modification to or deletion of a provision or part-provision under this clause shall not affect the validity and enforceability of the rest of these Terms.</p>
                <p><strong>15. Entire Agreement</strong></p>
                <p>These Terms of Use constitute the entire agreement between the parties and supersedes all previous discussions, correspondence, negotiations, arrangements, understandings and agreements between them relating to its subject matter.</p>
                <p><strong>16. Assignment</strong></p>
                <p>You may not assign, transfer, charge, subcontract or deal in any other manner with all or any of its rights or obligations under these Terms of Use. However, we may at any time assign, transfer, charge, subcontract or deal in any other manner, with all or any of its rights or obligations under these Terms of Use without your consent.</p>
                <p><strong>17. Survival</strong></p>
                <p>The rights and obligations of the parties as contained in the following clauses shall survive the termination or expiration of this Agreement along with any other right or legal obligation which by its nature would be reasonably expected to survive the expiration of this Agreement: Clause 5A.3 Customer Ratings; Clause 6 Intellectual Property; Clause 10 Limitation of Liability, Clause 11 Indemnity; Clause 13 Waiver; Clause 14 Severance; Clause 15 Entire Agreement; Clause 17 Survival; Clause 18 Governing Law and Jurisdiction; Clause 19 Third Party Rights; and Clause 20 No Partnership or Agency</p>
                <p><strong>18. Governing Law and Jurisdiction</p></strong>
                <p>18.1 These Terms of Use and any dispute or claim arising out of or in connection with it or its subject matter or formation (including non-contractual disputes or claims) shall be governed by and construed in accordance with the law of the Republic of Singapore.</p>
                <p>18.2 The Courts of the Republic of Singapore shall have exclusive jurisdiction to settle any dispute or claim arising out of or in connection with these Terms of Use or its subject matter or formation (including non-contractual disputes or claims).</p>
                <p><strong>19. Third Party Rights</strong></p>
                <p>No one other than a party to this Agreement, their successors and permitted assignees, shall have any right to enforce any of its terms.</p>
                <p><strong>20. No Partnership or Agency</strong></p>
                <p>Nothing in this Agreement is intended to, or shall be deemed to, establish any partnership or joint venture between any of the parties, constitute any party the agent of another party, nor authorize any party to make or enter into any commitments for or on behalf of any other party. Each party confirms it is acting on its own behalf and not for the benefit of any other person.</p>
                </div>
            </div>
        </div>
    </div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-27668603-3', 'auto');
  ga('send', 'pageview');

</script>
<?php include_once('footer.php'); ?>
