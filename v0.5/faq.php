<?php include_once('header.php'); ?>
<div class="container">
        <div class="row">
            <div class="grid_12">
                <div class="header2">
                    <h2><strong>Frequently </strong>Asked Questions</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="grid_12">
            	<div class="post4">
                    <a href="#WhatIs"><strong>What is CreditMark?</a></strong>
                </div>
                <div class="post4">
                    <a href="#WhoShould"><strong>Who should use CreditMark?</a></strong>
                </div>
                <div class="post4">
                    <a href="#HowDoes"><strong>How does CreditMark work?</a></strong>
                </div>
                <div class="post4">
                    <a href="#WhatInfo"><strong>What information does CreditMark collect?</a></strong>
                </div>
                <div class="post4">
                    <a href="#HowUse"><strong>How does CreditMark use the information I submit?</a></strong>
                </div>
                <div class="post4">
                    <a href="#NoOne"><strong>What if no one has provided a review of a customer I searched for?</a></strong>
                </div>
                <div class="post4">
                    <a href="#HowAccurate"><strong>How accurate is the data?</a></strong>
                </div>
                <div class="post4">
                    <a href="DetailsUnpaid"><strong>Are details of unpaid invoices included in the ratings?</a></strong>
                </div>
                <div class="post4">
                    <a href="#OpenIndefinitely"><strong>How can I rate a customer who has left my invoices open indefinitely?</a></strong>
                </div>
                <div class="post4">
                    <a href="#CanIleave"><strong>Can I delete my CreditMark account, and all the information I provided?</a></strong>
                </div>
                <div class="post4">
                    <a href="#Cost"><strong>How much does CreditMark cost?</a></strong>
                </div></b>
                        <br>
                        <br>
                <div class="post4" id="WhatIs">
                    <b class="label">What is CreditMark?</b>
                </div>
                    <p>CreditMark helps suppliers get an idea of how long a prospective customer is likely to take to pay their invoices, based on the experiences of other suppliers.</p>
                    <p>
                    CreditMark also provides customers the opportunity to respond with details of their procurement policy.</p><br>
                <div class="post4" id="WhoShould">
                <b class="label">Who should use CreditMark?</a></b>
                </div>
                    <p>CreditMark is used by suppliers large and small, including sole traders, freelancers - anyone who wants to know the likely time frame for getting paid <u><i>before</i></u> deciding whether to supply a product or service to a customer.</p><br>
				<div class="post4" id="HowDoes">
                    <b class="label">How does CreditMark work?</a></b>
                </div>
                    <p>CreditMark users give their customers a rating and share their past payment track record.<br>
                    By aggregating this data anonymously, suppliers get an idea when the customer is likely to settle their invoices.</p><br>
				<div class="post4" id="WhatInfo">
                    <b class="label">What information does CreditMark collect?</a></b>
                </div>
                    <p>By contributing information about a customer's payment track record, you help other suppliers decide whether to provide products and services to them.</p>
                    <p>To rate a customer, you will be prompted for details about a particular invoice you issued to them, namely the date of issue, the due date, and the date payment was received (if any).</p>
                    <p>We <b>do not</b> need to know the amount of the invoice, the identity of your contact person at the company, or any other details.</p>
                    <p>You can provide the payment track record of as many customers and as many invoices as you wish. The more you contribute, the more you help the CreditMark community globally.</p><br>
				<div class="post4" id="HowUse">
                    <b class="label">How does CreditMark use the information I submit?</a></b>
                </div>
                    <p>Customer payment track record information which suppliers provide is aggregated anonymously to derive an average time the customer takes to pay their invoices.<br>
                    Please see our <a href="privacy_policy.php">privacy policy</a> for further details.</p><br>
				<div class="post4" id="NoOne">
                    <b class="label">What if no one has provided a review of a customer I searched for?</a></b>
                </div>
                    <p>We list top search results on our home page. The customers searched for most often, and for which no payment track record has been provided, appear higher on the list.</p>
                    <p>We encourage users who have supplied products or services to that customer to provide their payment track record, which helps other suppliers understand how long they took to pay.</p><br>
				<div class="post4" id="HowAccurate">
                    <b class="label">How accurate is CreditMark data?</a></b>
                </div>
                    <p>The data is only as accurate as users provide, which is why it is important for suppliers to contribute payment track record data, and ensure it is entered accurately.</p>
                    <p>We do not check the accuracy, and make no warranty that the payment track record data is correct.</p>
                    <p>For this reason, we also allow customers to claim their profile and clarify their procurement processes.</p>
                    <p>If in doubt, suppliers should have an open and friendly conversation with the customer about payment terms.</p>
                <div class="post4" id="DetailsUnpaid"><br>
                    <b class="label">Are details of unpaid invoices included in the ratings?</a></b>
                </div>
                    <p>No, an open invoice which has not been settled does not contribute to the aggregated ratings.</p>
                    <p>Payment track record data is only included once you have marked the invoice as paid.</p>
				<div class="post4" id="OpenIndefinitely"><br>
                    <b class="label">How can I rate a customer who has left my invoices open indefinitely?</a></b>
                </div>
                    <p>Even though your invoice is not included in the aggregated rating until it is settled, you can still award 1-5 stars to the customer for their payment track record.</p>
                <div class="post4" id="Cost"><br>
                    <b class="label">Can I delete my CreditMark account, and all the information I provided?</a></b>
                </div>
                    <p>Yes. Just email us at </p>
                    <div class="post4" id="Cost"><br>
                    <b class="label">How much does CreditMark cost?</a></b>
                </div>
                    <p>CreditMark is free during beta testing.</p>
                </div>
            </div>
        </div>
    </div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-27668603-3', 'auto');
  ga('send', 'pageview');

</script>
<?php include_once('footer.php'); ?>