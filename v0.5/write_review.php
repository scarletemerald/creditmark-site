<?php include_once('function.php');
?>
<link rel="stylesheet" href="css/grid.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/contact-form.css">
<link href="css/autocomplete.css" media="all" rel="stylesheet" type="text/css" />  
<link rel="stylesheet" href="js/raty/jquery.raty.css">  
<script src="js/jquery.js"></script>
<script src="js/jquery-migrate-1.2.1.js"></script>
<script src="js/script.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<script src="js/TMForm.js"></script>
<script src="js/modal.js"></script>
<script src="js/raty/jquery.raty.js"></script>
<script>
$('.modal .modal-title', window.parent.document).html('Review a Company');
$('.modal .modal-dialog', window.parent.document).removeClass('modal-lg');
</script>
<?php
if ($login) {
	if ($_GET['cid'] != "") {
		$cust_id = base64_decode($_GET['cid']);
		
		$link = dbConnect();
		$cust = getCustomer($cust_id, -1);
		$cust = array_pop($cust);
		dbClose($link);
	}
	if ($_GET['rid'] != "") {
		$rating_id = base64_decode($_GET['rid']);
		
		$link = dbConnect();
		$review = getCustomerReviews("", $_SESSION['AP_uid'], "", " AND rate_id = '".mysqli_real_escape_string($link, $rating_id)."'", $link);
		$review = array_pop($review);
		$review['invoice_submission'] = explode(";", substr($review['invoice_submission'], 0, -1));
		$review['placed_order'] = explode(";", substr($review['placed_order'], 0, -1));
		
		$incident = getIncident($review['incident_id'], $review['user_id']);
		$incident = array_pop($incident);
		$contact = getCustomerContact($incident['cust_id'], $incident['user_id'], "", $incident['contact_id']);
		$contact = array_pop($contact);
		
	}
?>
<script>
var options, a, currentcoy;
jQuery(function(){
	currentcoy = $('#rateCompanyName').val();
	
	options = { serviceUrl:'getCustomers.php', showNoSuggestionNotice:true, minChars:3, lookupLimit:10, deferRequestBy: 500, orientation: 'auto',		
				onSearchStart: function (query) {
			$('.autocomplete-suggestions').html('<div class="autocomplete-suggestion" data-index="0">Please wait while we search our extensive database. This might take a few moments.</div>').show();
		},
		onSelect: function (suggestion) {
			currentcoy = suggestion.id;
			
			var uen = suggestion.uen;
			var url = suggestion.url;
			
			if (uen != "") {
				$('input#rateCompanyUEN').val(uen);
				$('.rateCompanyUEN ._placeholder').addClass('hidden');
			}
			else
				$('.rateCompanyUEN ._placeholder').removeClass('hidden');
			
			if (url != "") {
				$('input#rateCompanyURL').val(url);
				$('.rateCompanyURL ._placeholder').addClass('hidden');
			}
			else
				$('.rateCompanyURL ._placeholder').removeClass('hidden');
			<?php // activateContactSearch(); ?>
		}};
	a = $('input.coysearch').autocomplete(options);
	
	<?php // activateContactSearch(); ?>
	
	$('div.rating').raty({
		'half'			: true,
		'starHalf'		: 'js/raty/images/star-half.png',
		'starOff'		: 'js/raty/images/star-off.png',
		'starOn'		: 'js/raty/images/star-on.png',
		'scoreName'		: 'rateCompanyRatingVal',
		'hints'			: ['I am in a payment dispute with this customer', "This customer usually pays late/doesn't respond to enquiries", 'This customer usually pays late/responds to enquiries', 'This customer sometimes pays late/responds to enquiries', 'This customer always pays on time and is a pleasure to deal with'],
		'score': function() {
			return $(this).attr('data-score');
		}
	});
	
	$('input[name=rateCompanyRatingVal]').attr("data-constraints", "@Required");
	<?php if ($incident['invoice_date'] != "") { ?>
	calcDueDay();
	<?php } ?>
	<?php if ($incident['invoice_currency'] != "") { ?>
	$('#rateInvoiceCurrency').css('color', 'white');
	<?php } ?>
	<?php if ($incident['invoice_interest_period'] != "") { ?>
	$('#rateInvoiceInterestPeriod').css('color', 'white');
	<?php } ?>
});
<?php /*
function activateContactSearch() {
	var options2 = { serviceUrl:'getCustomerContacts.php', showNoSuggestionNotice:true, minChars:3, lookupLimit:10, deferRequestBy: 500, orientation: 'auto',		
				onSearchStart: function (query) {
			$('.autocomplete-suggestions').html('<div class="autocomplete-suggestion" data-index="0">Please wait while we search our extensive database. This might take a few moments.</div>').show();
		}, params: { 'c': currentcoy },
		onSelect: function (suggestion) {
		$.post('getCustomerContacts.php', { 'query': suggestion.id, 't': 'email', 'c': currentcoy }, function(d) {
			var d = JSON.parse(d);
			$('input#rateContactEmail').val(d.suggestions[0].value);
			if (d.suggestions[0].value != "")
				$('.rateContactEmail ._placeholder').addClass('hidden');
			else
				$('.rateContactEmail ._placeholder').removeClass('hidden');
		});
	}};
	$('input.coyconsearch').autocomplete(options2);
}
*/ ?>
function calcDueDate(val) {
	var invDate = ($('#rateInvoiceDate').val()).split('-');
	invDate = invDate[2] + "-" + invDate[1] + "-" + invDate[0];
	var t1 = (new Date(invDate)).getTime();
	if (isNaN(t1)) {
	}
	else {
		var t2 = val*24*60*60*1000;
		var dueDate = new Date(t1+t2);
		dueDate = ((dueDate.getDate()<10)?"0"+dueDate.getDate():dueDate.getDate()) + "-" + ((dueDate.getMonth()+1<10)?"0"+(dueDate.getMonth()+1):dueDate.getMonth()+1) + "-" + dueDate.getFullYear();
		$('#rateInvoiceDueDate').val(dueDate);
		$('.rateInvoiceDueDate ._placeholder').addClass('hidden');
	
	}
}
function calcDueDay() {
	var invDate = ($('#rateInvoiceDate').val()).split('-');
	invDate = invDate[2] + "-" + invDate[1] + "-" + invDate[0];
	var t1 = (new Date(invDate)).getTime();
	if (isNaN(t1)) {
	}
	else {
		var dueDate = ($('#rateInvoiceDueDate').val()).split('-');
		dueDate = dueDate[2] + "-" + dueDate[1] + "-" + dueDate[0];
		var t2 = (new Date(dueDate)).getTime();
		
		var dueDays = Math.floor((t2-t1)/1000/3600/24);
		$('#rateInvoiceDue').val(dueDays).css('color', 'white');
	}
}
function showCertifyTxt(val) {
	if (val == "") {
		$('#certifytxt').html("I certify that the information provided above is correct, and I will update it once payment has been received. I confirm that the purpose of my review is purely to share my genuine experiences with the customer's payment processes in good faith. I have not been offered any incentive or payment by the customer to write this review or to alter a previous review.");
	}
	else {
		$('#certifytxt').html("I certify that the information provided above is correct. I confirm that the purpose of my review is purely to share my genuine experiences with the company's payment processes in good faith. I have not been offered any incentive or payment by the customer to write this review or to alter a previous review.");
	}
}
</script>
<form id="reviewcoy-form" class="contact-form" enctype="multipart/form-data">
	<input type="hidden" name="rateID" id="rateID" value="<?=$review['rate_id'];?>" />
	<input type="hidden" name="incidentID" id="incidentID" value="<?=$incident['incident_id'];?>" />
    <div class="contact-form-loader"></div>
    <fieldset>
        <div class="row" style="margin-top: 0px; margin-bottom: 12px;">
            <div class="grid_8">
                <p style="padding-top: 0px;">Thank you for being part of the CreditMark community.</p>
				<p>As a member, you play a critically important part in getting companies like yours paid faster.</p>
				<p>Every time you share just a few details about your invoices to your customers below, you are bringing the community one step closer to better cash flow.</p>
				<p>The information you provide is aggregated with our existing crowdsourced data. Your details will not be shared.</p>
				<p>First, tell us a few details about your customer.</p>
                
                <label class="rateCompanyName">
                    <input type="text" name="rateCompanyName" id="rateCompanyName" class="coysearch" placeholder="Company Name*" data-constraints="@Required" value="<?=$cust['company_name'];?>" />
                    <span class="empty-message">*This field is required.</span>
                </label>
                <label class="rateCompanyUEN">
                    <input type="text" name="rateCompanyUEN" id="rateCompanyUEN" placeholder="Business Registered Number*" data-constraints="@Required" value="<?=$cust['company_uen'];?>" />
                    <span class="empty-message">*This field is required.</span>
                </label>
                <label class="rateCompanyURL" style="margin-bottom: 0px;">
                    <a class="hastip" title="Make absolutely certain that the company you are reviewing and the web address match! Please include http://"><input type="text" name="rateCompanyURL" id="rateCompanyURL" placeholder="Company URL" data-constraints="@URL" value="<?=$cust['company_url'];?>" /></a>
                    <span class="error-message">*Sorry, the URL is in an incorrect format. It should look like this: http://www.company.com or http://prefix.company.com.</span>
                </label>
            </div>
         </div>
         <?php /*
         <div class="row">
            <div class="grid_8">
                <label class="rateContactName">
                    <input type="text" name="rateContactName" id="rateContactName" class="coyconsearch" placeholder="Contact Person Name"  value="<?=$contact['contact_name'];?>" />
                </label>
                
                <label class="rateContactEmail">
                    <input type="text" name="rateContactEmail" id="rateContactEmail" placeholder="Contact Person E-mail" data-constraints="@Email" value="<?=$contact['contact_email'];?>" />
                    <span class="error-message">*This is not a valid email.</span>
                </label>
                
                <hr style="width: 90%;" />
            </div>
         </div>
		 */ ?>
         <div class="row" style="margin-bottom: 12px;">
            <div class="grid_8">
                <p style="padding-top: 12px;">Describe this customer's payment process. Give them a rating out of five (hint: mouse-over the stars for descriptions): </p>
                <label class="rateCompanyRating" style="height: auto;">
                <div class="rating" data-score="<?=($review['rating']>0)?$review['rating']:0;?>" id="rateCompanyRating"></div>
                <span class="empty-message">*This field is required.</span>
                </label>
                <p style="padding-top: 0px;">Are you a:</p>
                    
                    <label class="rateCompanyUserType" style="color: #fff; height: auto;">
                        <input type="radio" name="rateCompanyUserType" id="rateCompanyUserType1" data-constraints="@JustChecked" data-contraints-match="rateCompanyUserType" value="Current supplier" <?php if ($review['supplier_type'] == "Current supplier") echo ' checked';?> /> Current supplier
                        <br />
                        <input type="radio" name="rateCompanyUserType" id="rateCompanyUserType3" data-constraints="@JustChecked" data-contraints-match="rateCompanyUserType" value="Former supplier" <?php if ($review['supplier_type'] == "Former supplier") echo ' checked';?> /> Former supplier
                        <br /> 
                        <input type="radio" name="rateCompanyUserType" id="rateCompanyUserType4" data-constraints="@JustChecked" data-contraints-match="rateCompanyUserType" value="Other" <?php if ($review['supplier_type'] == "Other") echo ' checked';?> /> Other 								
                        <span class="error-message">*This field is required.</span>
                    </label>
             </div>
        </div>
        <div class="row" style="margin-bottom: 12px;">
            <div class="grid_8">
                <p style="padding-top: 0px;">You submitted your invoice (tick as many as apply):</p>
                    
                    <label class="rateInvoiceSubmitted" style="color: #fff; height: auto;">
                        <input type="checkbox" name="rateInvoiceSubmitted[]" id="rateInvoiceSubmitted1" data-constraints="@JustChecked" data-contraints-match="rateInvoiceSubmitted[]" value="Electronically, via the customer's procurement portal" <?php if (in_array("Electronically, via the customer's procurement portal", $review['invoice_submission'])) echo ' checked';?> /> Electronically, via the company's procurement portal
                        <br />
                        <input type="checkbox" name="rateInvoiceSubmitted[]" id="rateInvoiceSubmitted2" data-constraints="@JustChecked" data-contraints-match="rateInvoiceSubmitted[]" value="Electronically, via regular email" <?php if (in_array("Electronically, via regular email", $review['invoice_submission'])) echo ' checked';?> /> Electronically, via regular email<br />
                        <input type="checkbox" name="rateInvoiceSubmitted[]" id="rateInvoiceSubmitted3" data-constraints="@JustChecked" data-contraints-match="rateInvoiceSubmitted[]" value="Hard copy, delivered with the product/service" <?php if (in_array("Hard copy, delivered with the product/service", $review['invoice_submission'])) echo ' checked';?> /> Hard copy, delivered with the product/service
                        <br /> 
                        <input type="checkbox" name="rateInvoiceSubmitted[]" id="rateInvoiceSubmitted4" data-constraints="@JustChecked" data-contraints-match="rateInvoiceSubmitted[]" value="Hard copy, snail mailed" <?php if (in_array("Hard copy, snail mailed", $review['invoice_submission'])) echo ' checked';?> /> Hard copy, snail mailed 								
                        <span class="error-message">*This field is required.</span>
                    </label>
             </div>
        </div>
        <div class="row" style="margin-bottom: 12px;">
            <div class="grid_8">
                    <label class="rateInvoiceConfirm" style="color: #fff; height: auto; line-height: 1.2">
                        <a class="hastip" title="Do not check this box if your customer has pointed out errors in your invoice,<br />and/or you did not submit your invoice through their preferred channels."><input type="checkbox" name="rateInvoiceConfirm" id="rateInvoiceConfirm" value="1" <?=($review['invoice_issued_ok'])?" checked":"";?> />I confirm that I issued my invoice in accordance with the instructions of the customer.</a>
                    </label>
             </div>
        </div>
        
        <div class="row" style="margin-bottom: 12px;">
            <div class="grid_8">
                <p style="padding-top: 0px;">The customer placed their order with me using (tick as many as apply):</p>
                    
                    <label class="ratePlaceOrder" style="color: #fff; height: auto;">
                        <input type="checkbox" name="ratePlaceOrder[]" id="ratePlaceOrder1" data-constraints="@JustChecked" data-contraints-match="ratePlaceOrder[]" value="A purchase order (PO)" <?php if (in_array("A purchase order (PO)", $review['placed_order'])) echo ' checked';?> /> A purchase order (PO)
                        <br />
                        <input type="checkbox" name="ratePlaceOrder[]" id="ratePlaceOrder2" data-constraints="@JustChecked" data-contraints-match="ratePlaceOrder[]" value="A signed, hard copy quotation" <?php if (in_array("A signed, hard copy quotation", $review['placed_order'])) echo ' checked';?> /> A signed, hard copy quotation<br />
                        <input type="checkbox" name="ratePlaceOrder[]" id="ratePlaceOrder3" data-constraints="@JustChecked" data-contraints-match="ratePlaceOrder[]" value="A digitally-signed quotation" <?php if (in_array("A digitally-signed quotation", $review['placed_order'])) echo ' checked';?> /> A digitally-signed quotation
                        <br /> 
                        <input type="checkbox" name="ratePlaceOrder[]" id="ratePlaceOrder4" data-constraints="@JustChecked" data-contraints-match="ratePlaceOrder[]" value="A telephone/in-person conversation" <?php if (in_array("A telephone/in-person conversation", $review['placed_order'])) echo ' checked';?> /> A telephone/in-person conversation<br />
                        <input type="checkbox" name="ratePlaceOrder[]" id="ratePlaceOrder5" data-constraints="@JustChecked" data-contraints-match="ratePlaceOrder[]" value="Other" <?php if (in_array("Other", $review['placed_order'])) echo ' checked';?> /> Other
                        <span class="error-message">*This field is required.</span>
                    </label>
             </div>
        </div>
        <div class="row" style="margin-bottom: 12px;">
            <div class="grid_8">
                    <label class="rateInvoiceConfirm2" style="color: #fff; height: auto; line-height: 1.2">
                        <a class="hastip" title="Do not check this box if your product/service<br />did not meet the specifications of your customer."><input type="checkbox" name="rateInvoiceConfirm2" id="rateInvoiceConfirm2" value="1" <?=($review['product_delivered'])?" checked":"";?> />I confirm that the products/services were delivered according to specifications.</a>
                    </label>
                    <hr style="width: 90%;" />
             </div>
        </div>
        <div class="row" style="margin-bottom: 25px;">
            <div class="grid_8">
                <p style="padding-top: 0px;">Tell us about your most recent invoice with the customer</p>
                <label class="rateInvoiceNo">
                    <input type="text" name="rateInvoiceNo" placeholder="Invoice Number" value="<?=$incident['invoice_no'];?>" />
                </label>
                <label class="rateInvoiceDate">
                    <a class="hastip" title="Date Format: DD-MM-YYYY"><input type="text" name="rateInvoiceDate" id="rateInvoiceDate" placeholder="Invoice Date*" data-constraints="@Required @JustDate" value="<?=($incident['invoice_date']!="")?date('d-m-Y', strtotime($incident['invoice_date'])):"";?>" /></a>
                    <span class="empty-message">*This field is required.</span>
                    <span class="error-message">*Date format as dd-mm-yyyy.</span>
                </label>
                <?php /*
                <label class="rateInvoiceCurrency">
                    <select name="rateInvoiceCurrency" id="rateInvoiceCurrency" class="select-menu">
                        <option value="">Invoice Currency</option> 
                        <?php foreach ($currencyArr as $cur) { ?>
                        <option value="<?=$cur;?>" <?php if ($incident['invoice_currency'] == $cur) echo ' selected'; ?>><?=$cur;?></option>
                        <?php } ?>
                    </select>
                </label>
                
                <label class="rateInvoiceAmount">
                    <input type="text" name="rateInvoiceAmount" placeholder="Invoice Amount" data-constraints="@JustDigits" value="<?=$incident['invoice_amount'];?>" />
                    <span class="error-message">*Please enter only numbers.</span>
                </label>
                */ ?>
                <label class="rateInvoiceDue">
                    <select name="rateInvoiceDue" id="rateInvoiceDue" class="select-menu" data-constraints="@Required" onchange="calcDueDate(this.value);">
                        <option value="">*Your Invoice to the customer was due after...</option> 
                        <optgroup label="Standard Days">
                            <option value="14">14 Days</option>
                            <option value="30">30 Days</option>
                            <option value="60">60 Days</option>
                            <option value="90">90 Days</option>
                            <option value="120">120 Days</option>
                        </optgroup>
                        <optgroup label="Other Days">
                        <?php for ($i=1; $i<=365; $i++) { 
                            if (!in_array($i, array(14, 30, 60, 90, 120))) {
                        ?>
                        <option value="<?=$i;?>"><?=$i;?> Days</option>
                        <?php }
                        } ?>
                        </optgroup>
                    </select>
                    <span class="empty-message">*This field is required.</span>
                </label>
                <label class="rateInvoiceDueDate">
                    <a class="hastip" title="Date Format: DD-MM-YYYY"><input type="text" name="rateInvoiceDueDate" id="rateInvoiceDueDate" placeholder="Invoice Due Date*" data-constraints="@Required @JustDate" value="<?=($incident['invoice_due']!="")?date('d-m-Y', strtotime($incident['invoice_due'])):"";?>" onblur="calcDueDay()" /></a>
                    <span class="empty-message">*This field is required.</span>
                    <span class="error-message">*Date format as dd-mm-yyyy.</span>
                </label>
                <?php /*
                <label class="rateInvoiceInterest">
                        <input type="text" name="rateInvoiceInterest" placeholder="Interest Due on Overdue Invoices (%)" data-constraints="@JustDigits" value="<?=$incident['invoice_interest'];?>" />
                        <span class="error-message">*Please enter only numbers.</span>
                    </label>
                    
                    <label class="rateInvoiceInterestPeriod">
                        <select name="rateInvoiceInterestPeriod" id="rateInvoiceInterestPeriod" class="select-menu">
                            <option value="">Interest Payable Time Period</option> 
                            <option value="1" <?php if ($incident['invoice_interest_period'] == 1) echo ' selected'; ?>>Per Day</option>
                            <option value="7" <?php if ($incident['invoice_interest_period'] == 7) echo ' selected'; ?>>Per Week</option>
                            <option value="30" <?php if ($incident['invoice_interest_period'] == 30) echo ' selected'; ?>>Per Month</option>
                            <option value="365" <?php if ($incident['invoice_interest_period'] == 365) echo ' selected'; ?>>Per Year</option>
                        </select>
                    </label>
                */ ?>
                <label class="ratePaymentDate">
                    <a class="hastip" title="IMPORTANT: if your invoice has not been paid yet, leave this blank.<br />But to ensure accurate results, please return to this review and complete this date once you have received payment.<br />Your review will be excluded from the aggregated results if you do not complete this review within 90 days.<br /><br />Date Format: DD-MM-YYYY"><input type="text" name="ratePaymentDate" placeholder="Payment Received Date" onblur="showCertifyTxt(this.value)" data-constraints="@JustDate" value="<?php if ($incident['payment_received'] != "0000-00-00" && $incident['payment_received'] != "") echo date('d-m-Y', strtotime($incident['payment_received']));?>" /></a>
                    <span class="error-message">*Date format as dd-mm-yyyy.</span>
                </label>
                <hr style="width: 90%;" />
            </div>
        </div>
        <div class="row" style="margin-bottom: 12px;">
            <div class="grid_8">
                    <label class="rateAgree" style="color: #fff; height: auto; line-height: 1.2">
                        <input type="checkbox" name="rateAgree" id="rateAgree" value="1" data-constraints="@Required" /> <span id="certifytxt">I certify that the information provided above is correct, and I will update it once payment has been received. I confirm that the purpose of my review is purely to share my genuine experiences with the customer's payment processes in good faith. I have not been offered any incentive or payment by the customer to write this review or to alter a previous review.</span>*
                   	 <span class="empty-message" style="top: auto; bottom: -30px;">*This field is required.</span>
                    </label>
             </div>
        </div>

        <div class="contact-form-buttons">
            <a href="#" data-type="submit" class="btn-default">Submit</a>
        </div>
    </fieldset>
    <div class="modal fade response-message">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Rate a Company</h4>
                </div>
                <div class="modal-body">
                    Sorry, your registration was unsuccessful. Please try again. If you are still unsuccessful, please contact us.
                </div>
            </div>
        </div>
    </div>
</form>
<?php
}
else {?>
<h4>This page is unavailable.<br /><br />Please try again later.</h4>
<?php
}
?>
