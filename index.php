<?php include_once('header.php'); ?>

    <link href="css/autocomplete.css" media="all" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.autocomplete.js"></script>
    <script>
	var options, a;
	jQuery(function(){
		options = { serviceUrl:'getCustomers.php', showNoSuggestionNotice:true, minChars:3, lookupLimit:10};
		a = $('input.coysearch').autocomplete(options);
	});
	</script>
    <div class="banner1">
        <div class="container">
            <div class="row">
                <div class="grid_7">
                    <h6><strong>WHEN WILL YOU GET PAID?</strong></h6><br><h2>Discover the payment track record of your customers and prospects.<br></h2>
                    <a class="btn-default" href="login.php">REGISTER NOW</a>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper1">
        <div class="container">
        	<div class="row">
                <div class="grid_12">
                	<input name="coy" id="coy" type="text" placeholder="Search Company..." class="coysearch" /><a href="javascript:void(0);" onClick="<?php if ($login) { ?>openModal('search_company.php?query='+encodeURIComponent($('#coy').val()));<?php } else { ?>alert('Please login to view search results.');<?php } ?>" class="btn-default" style="display: inline-block;">Search</a>
                </div>
            </div>
            <div class="row">
                <div class="grid_4">
                    <div class="post1">
                        <div class="header">
                            <h3>DISCOVER</h3>
                        </div>
                        <div class="content" style="line-height: 1.5;">
                        	Find out when other suppliers to your customers got paid.<br /><br />
                            You can plan your cash flows better by monitoring how long it takes your customers on average to pay other CreditMark users.
                        </div>
                    </div>
                </div>
                <div class="grid_4">
                    <div class="post1">
                        <div class="header">
                            <h3>SHARE</h3>
                        </div>
                        <div class="content" style="line-height: 1.5;">
                        	Contribute your experiences to help other suppliers.<br /><br />
The more invoices you add, the more you will help other suppliers get an accurate picture of the payment track record of this company.
						</div>
                    </div>
                </div>

                <div class="grid_4">
                    <div class="post1">
                        <div class="header">
                            <h3>IMPROVE</h3>
                        </div>
                        <div class="content" style="line-height: 1.5;">
                        	By joining CreditMark you will promote positive change in the payment cycles of large companies to help improve cash flow for smaller companies.
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="grid_12">&nbsp;
                </div>
            </div>
            
            <?php include_once('mod_promptpayers.php'); ?>
            
            <?php include_once('mod_mostpopular.php'); ?>
            
           			 <div class="modal fade response-message">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title">Search Result</h4>
                            </div>
                            <div class="modal-body">
                              <iframe id="modalcontent" src="" width="100%" height="500px"></iframe>
                            </div>
                          </div>
                        </div>
                      </div>
            </div>
        </div>
    </div>
<?php include_once('footer.php'); ?>
